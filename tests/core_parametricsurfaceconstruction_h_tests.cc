// gtest
#include <gtest/gtest.h>   // googletest header file

// gmlib2
#include <parametric/polygonsurface_constructions/ppolygon.h>







namespace unittest_detail
{
}   // namespace unittest_detail










TEST(Core_ParametricSurfaceConstruction, Stack_Compile_Test)
{

  namespace gm2  = gmlib2;
  namespace gm2p = gm2::parametric;

    using EmbedSpaceInfo
    = gm2::spaces::D3R3SpaceInfo<>;
  using SO       = gm2::ProjectiveSpaceObject<EmbedSpaceInfo>;
  using PPolygon = gm2p::PPolygon<SO>;


  // Space object
  [[maybe_unused]] SO so;

  // Polygon surfaces construction
  [[maybe_unused]] PPolygon ppolygon4{
    gm2::polygonutils::generateRegularPolygon2DXZ(4)};
  [[maybe_unused]] PPolygon ppolygon5{
    gm2::polygonutils::generateRegularPolygon2DXZ(5)};
  [[maybe_unused]] PPolygon ppolygon6{
    gm2::polygonutils::generateRegularPolygon2DXZ(6)};

  EXPECT_EQ(ppolygon4.sides(), 4UL);
  EXPECT_EQ(ppolygon5.sides(), 5UL);
  EXPECT_EQ(ppolygon6.sides(), 6UL);
}
