// gtest
#include <gtest/gtest.h>   // googletest header file


// gmlib2
#include <parametric/ppoint.h>
#include <parametric/pcurve.h>
#include <parametric/psurface.h>
#include <parametric/ppolygonsurface.h>


TEST(Parametric_ParametricObject_EvaluationCtrl_Traits,
     PPoint_Default_PObjEvalCtrl_isSEvaluationCtrl)
{
  using PPoint = gmlib2::parametric::PPoint<>;
  EXPECT_TRUE(gmlib2::traits::Is_PObjEvalCtrl<PPoint::PObjEvalCtrl>::value);
}

TEST(Parametric_ParametricObject_EvaluationCtrl_Traits,
     evaluationctrl_PPointEvaluationCtrl_isSEvaluationCtrl)
{
  using PPoint = gmlib2::parametric::PPoint<
    gmlib2::ProjectiveSpaceObject<>,
    gmlib2::parametric::evaluationctrl::PCurveEvalCtrl>;
  EXPECT_TRUE(gmlib2::traits::Is_PObjEvalCtrl<PPoint::PObjEvalCtrl>::value);
}

TEST(Parametric_ParametricObject_EvaluationCtrl_Traits,
     PCurve_Default_PObjEvalCtrl_isSEvaluationCtrl)
{
  using PCurve = gmlib2::parametric::PCurve<>;
  EXPECT_TRUE(gmlib2::traits::Is_PObjEvalCtrl<PCurve::PObjEvalCtrl>::value);
}

TEST(Parametric_ParametricObject_EvaluationCtrl_Traits,
     evaluationctrl_PCurveEvaluationCtrl_isSEvaluationCtrl)
{
  using PCurve = gmlib2::parametric::PCurve<
    gmlib2::ProjectiveSpaceObject<>,
    gmlib2::parametric::evaluationctrl::PCurveEvalCtrl>;
  EXPECT_TRUE(gmlib2::traits::Is_PObjEvalCtrl<PCurve::PObjEvalCtrl>::value);
}


TEST(Parametric_ParametricObject_EvaluationCtrl_Traits,
     PSurface_Default_PObjEvalCtrl_isSEvaluationCtrl)
{
  using PSurface = gmlib2::parametric::PSurface<>;
  EXPECT_TRUE(gmlib2::traits::Is_PObjEvalCtrl<PSurface::PObjEvalCtrl>::value);
}

TEST(Parametric_ParametricObject_EvaluationCtrl_Traits,
     evaluationctrl_PSurfaceEvaluationCtrl_isSEvaluationCtrl)
{
  using PSurface = gmlib2::parametric::PSurface<
    gmlib2::ProjectiveSpaceObject<>,
    gmlib2::parametric::evaluationctrl::PSurfaceEvalCtrl>;
  EXPECT_TRUE(gmlib2::traits::Is_PObjEvalCtrl<PSurface::PObjEvalCtrl>::value);
}

TEST(Parametric_ParametricObject_EvaluationCtrl_Traits,
     PPolygonSurfaceConstruction_Default_PObjEvalCtrl_isSEvaluationCtrl)
{
  using PPolygonSurface = gmlib2::parametric::PPolygonSurface<>;
  EXPECT_TRUE(
    gmlib2::traits::Is_PObjEvalCtrl<PPolygonSurface::PObjEvalCtrl>::value);
}

TEST(Parametric_ParametricObject_EvaluationCtrl_Traits,
     evaluationctrl_PPolyConstrEvalCtrl_isSEvaluationCtrl)
{
  using PPolygonSurface = gmlib2::parametric::PPolygonSurface<
    gmlib2::ProjectiveSpaceObject<>,
    gmlib2::parametric::evaluationctrl::PPolygonSurfaceEvalCtrl>;
  EXPECT_TRUE(
    gmlib2::traits::Is_PObjEvalCtrl<PPolygonSurface::PObjEvalCtrl>::value);
}
