#ifndef GM2_GMLIB2_H
#define GM2_GMLIB2_H

#include "platform.h"

// core
#include "core/gm2_blaze.h"
#include "core/datastructures.h"
#include "core/space.h"
#include "core/spaces/vectorspace.h"
#include "core/spaces/affinespace.h"
#include "core/spaces/projectivespace.h"
#include "core/projectivespaceobject.h"
#include "core/coreutils.h"
#include "core/polygonutils.h"
#include "core/basis/bfunction.h"
#include "core/basis/bfunctions/bfbs.h"
#include "core/basis/bfunctions/fabius.h"
#include "core/basis/bfunctions/ler.h"
#include "core/basis/bfunctions/linear.h"
#include "core/basisgenerators/bernsteinbasisgenerators.h"
#include "core/basisgenerators/hermitebasisgenerators.h"
#include "core/divideddifference/finitedifference.h"
#include "core/gbc/mvc.h"
#include "core/gbc/mapping.h"
#include "core/gbc/gbcutils.h"


// parametrics
#include "parametric/ppoint.h"
#include "parametric/curve.h"
#include "parametric/surface.h"
#include "parametric/parametricobject.h"
// -- utils
#include "parametric/utils/approximation.h"
#include "parametric/utils/fitting.h"
#include "parametric/utils/bsplineutils.h"
#include "parametric/utils/blendingsplineutils.h"
// -- parametric sub object constructions
#include "parametric/subobject_constructions/subcurve.h"
#include "parametric/subobject_constructions/subsurface.h"
#include "parametric/subobject_constructions/subtrianglesurface.h"
#include "parametric/subobject_constructions/subpolygonsurface.h"
// -- classic shapes
// -- ** curves
#include "parametric/classic_objects/circle.h"
#include "parametric/classic_objects/helix.h"
#include "parametric/classic_objects/line.h"
// -- ** surfaces
#include "parametric/classic_objects/plane.h"
#include "parametric/classic_objects/sphere.h"
#include "parametric/classic_objects/torus.h"
// -- classic constructions
// -- ** curves
#include "parametric/classic_constructions/beziercurve.h"
#include "parametric/classic_constructions/bsplinecurve.h"
#include "parametric/classic_constructions/nurbscurve.h"
#include "parametric/classic_constructions/hermitecurve.h"
// -- ** surfaces
#include "parametric/classic_constructions/beziersurface.h"
#include "parametric/classic_constructions/bsplinesurface.h"
#include "parametric/classic_constructions/nurbssurface.h"
#include "parametric/classic_constructions/bicubiccoonspatch.h"
#include "parametric/classic_constructions/bilinearcoonspatch.h"
// -- triangle construction
#include "parametric/trianglesurface.h"
#include "parametric/trianglesurface_constructions/triangle.h"
#include "parametric/trianglesurface_constructions/beziertriangle.h"
// -- polygon construction
#include "parametric/polygonsurface.h"
#include "parametric/polygonsurface_constructions/polygonblendingbasissurface.h"
#include "parametric/polygonsurface_constructions/polygon.h"
#include "parametric/polygonsurface_constructions/generalizedbezierpatch.h"
#include "parametric/polygonsurface_constructions/concavegeneralizedbezierpatch.h"
#include "parametric/polygonsurface_constructions/transfiniteribbonpatch.h"
// -- volume construction
#include "parametric/volume.h"
#include "parametric/volume_constructions/cuboid.h"
#include "parametric/volume_constructions/beziervolume.h"
// -- Blendspline constructions
#include "parametric/blendingspline_constructions/blendingsplinecurve.h"
#include "parametric/blendingspline_constructions/blendingsplinesurface.h"
#include "parametric/blendingspline_constructions/blendingsplinetriangle.h"
#include "parametric/blendingspline_constructions/blendingsplinepolygonpatch.h"


#endif   // GM2_GMLIB2_H
