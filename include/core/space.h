#ifndef GM2_SPACES_H
#define GM2_SPACES_H


#include "gm2_blaze.h"
#include "traits.h"

//// blaze
//#include <blaze/math/Band.h>

// stl
#include <cmath>
#include <iostream>



namespace gmlib2::spaces
{






  //////////////////////////////////
  ///  Embeddings
  ///
  ///




  //  template <typename Unit_T = double, size_t FrameDim_T, size_t VectorDim_T>
  //  struct ArbitrarySpaceInfo {
  //    using Unit = Unit_T;
  //    static constexpr auto FrameDim  = FrameDim_T;
  //    static constexpr auto VectorDim = VectorDim_T;
  //  };


  template <typename Unit_T = double>
  struct D3R3SpaceInfo {
    using Unit                      = Unit_T;
    static constexpr auto FrameDim  = 3UL;
    static constexpr auto VectorDim = 3UL;
  };







}   // namespace gmlib2::spaces


#endif   // GM2_SPACES_H
