#ifndef GM2_CORE_TRAITS_H
#define GM2_CORE_TRAITS_H



// stl
#include <type_traits>




///////////////////////
// SFINAE HELPERS START
// Substitution failure is not an error

//#define GM2_DEFINE_MEMBER_TYPE_CHECK(id)                                       \
//  template <typename T, typename = void>                                       \
//  struct HasMemberType_##id : std::false_type {                                \
//  };                                                                           \
//                                                                               \
//  template <typename T>                                                        \
//  struct HasMemberType_##id<T, decltype(T::id(), void())> : std::true_type {   \
//  };
//#define GM2_HAS_MEMBER_TYPE(T, id) HasMemberType_##id<T>::value



#define GM2_DEFINE_MEMBER_CHECK(id)                                            \
  template <typename T, typename = void>                                       \
  struct HasMember_##id : std::false_type {                                    \
  };                                                                           \
                                                                               \
  template <typename T>                                                        \
  struct HasMember_##id<T, decltype(std::declval<T>().id, void())>             \
    : std::true_type {                                                         \
  };

#define GM2_HAS_MEMBER(T, id) HasMember_##id<T>::value



#define GM2_DEFINE_USING_CHECK(id)                                             \
  template <typename T, typename = void>                                       \
  struct HasMemberType_##id : std::false_type {                                \
  };                                                                           \
                                                                               \
  template <typename T>                                                        \
  struct HasMemberType_##id<T, decltype(typename T::id(), void())>             \
    : std::true_type {                                                         \
  };
#define GM2_HAS_USING(T, id) HasMemberType_##id<T>::value


// SFINAE HELPERS END
/////////////////////

namespace gmlib2::traits
{



  //////////////////////////////////
  // Space concepts (C++20)
  //  template <typename FrameDim_T, typename VectorDim_T>
  //  concept Space = std::is_integral<FrameDim_T>::value and
  //          std::is_integral<VectorDim_T>::value and
  //          requires(FrameDim_T FD, VectorDim_T VD){
  //    frameDimension() const -> FrameDim_T;
  //    vectorDimension() const -> VectorDim_T;
  //  };

  //  template <typename FrameDim_T>
  //  concept ParametricSpace = Space<FrameDim_T,1>;

  //  template <typename FrameDim_T,VectorDim_T>
  //  concept EmbeddedSpace = Space<FrameDim_T,VectorDim_T>;


  //////////////////////////////////
  // Space type-trait
  //  namespace traits {

  //    // Space trait
  //    template <typename Space_T>
  //    class is_space {
  //      using yes = char;
  //      using no  = long;

  //      template <typename C>
  //      static yes hasFrameDimFunc(typeof(&C::frameDim));
  //      template <typename C>
  //      static no hasFrameDimFunc(...);

  //      template <typename C>
  //      static yes hasVectorDimFunc(typeof(&C::vectorDim));
  //      template <typename C>
  //      static no hasVectorDimFunc(...);


  //      template <typename C>
  //      static constexpr bool hasRequiredMemberFunctions() {
  //        return (sizeof(hasFrameDimFunc<Space_T>(0)) == sizeof(yes))
  //                and (sizeof(hasVectorDimFunc<Space_T>(0)) == sizeof(yes));
  //      }

  //    public:
  //      enum {
  //        value = hasRequiredMemberFunctions()
  //      };
  //    };
  //  }



  template <typename SpaceInfo_T>
  struct Is_SpaceInfo {

    GM2_DEFINE_USING_CHECK(Unit)
    GM2_DEFINE_MEMBER_CHECK(VectorDim)
    GM2_DEFINE_MEMBER_CHECK(FrameDim)

    static constexpr auto value = GM2_HAS_USING(SpaceInfo_T, Unit)
                                  and GM2_HAS_MEMBER(SpaceInfo_T, VectorDim)
                                  and GM2_HAS_MEMBER(SpaceInfo_T, FrameDim);
  };



  template <typename Space_T>
  struct Is_VectorSpace {

    // SpaceInfo and aggregate types
    GM2_DEFINE_USING_CHECK(SpaceInfo)

    GM2_DEFINE_USING_CHECK(Unit)
    GM2_DEFINE_MEMBER_CHECK(VectorDim)
    GM2_DEFINE_MEMBER_CHECK(FrameDim)

    // VectorSpace types
    GM2_DEFINE_USING_CHECK(Vector)
    GM2_DEFINE_USING_CHECK(Frame)

    static constexpr auto value =

      // SpaceInfo and aggregate types checks
      GM2_HAS_USING(Space_T, SpaceInfo)
      and Is_SpaceInfo<typename Space_T::SpaceInfo>::value
      and GM2_HAS_USING(Space_T, Unit) and GM2_HAS_MEMBER(Space_T, VectorDim)
      and GM2_HAS_MEMBER(Space_T, FrameDim)

      // VectorSpace types checks
      and GM2_HAS_USING(Space_T, Vector) and GM2_HAS_USING(Space_T, Frame);
  };



  template <typename Space_T>
  struct Is_AffineSpace {

    // SpaceInfo and aggregate types
    GM2_DEFINE_USING_CHECK(SpaceInfo)

    GM2_DEFINE_USING_CHECK(Unit)
    GM2_DEFINE_MEMBER_CHECK(VectorDim)
    GM2_DEFINE_MEMBER_CHECK(FrameDim)

    // VectorSpace and aggreate types
    GM2_DEFINE_USING_CHECK(VectorSpace)

    GM2_DEFINE_USING_CHECK(Vector)
    GM2_DEFINE_USING_CHECK(Frame)

    // AffineSpace types
    GM2_DEFINE_MEMBER_CHECK(ASFrameDim)
    GM2_DEFINE_USING_CHECK(Point)
    GM2_DEFINE_USING_CHECK(ASFrame)

    static constexpr auto value =

      // SpaceInfo and aggregate types checks
      GM2_HAS_USING(Space_T, SpaceInfo)
      and Is_SpaceInfo<typename Space_T::SpaceInfo>::value
      and GM2_HAS_USING(Space_T, Unit) and GM2_HAS_MEMBER(Space_T, VectorDim)
      and GM2_HAS_MEMBER(Space_T, FrameDim)

      // VectorSpace and aggregate types checks
      and GM2_HAS_USING(Space_T, VectorSpace)
      and Is_VectorSpace<typename Space_T::VectorSpace>::value
      and GM2_HAS_USING(Space_T, Vector)
      and GM2_HAS_USING(Space_T, Frame)

      // AffineSpace types checks
      and GM2_HAS_MEMBER(Space_T, ASFrameDim) and GM2_HAS_USING(Space_T, Point)
      and GM2_HAS_USING(Space_T, ASFrame);
  };



  template <typename Space_T>
  struct Is_ProjectiveSpace {

    // SpaceInfo and aggregate types
    GM2_DEFINE_USING_CHECK(SpaceInfo)

    GM2_DEFINE_USING_CHECK(Unit)
    GM2_DEFINE_MEMBER_CHECK(VectorDim)
    GM2_DEFINE_MEMBER_CHECK(FrameDim)

    // VectorSpace and aggregate types
    GM2_DEFINE_USING_CHECK(VectorSpace)

    GM2_DEFINE_USING_CHECK(Vector)
    GM2_DEFINE_USING_CHECK(Frame)

    // AffineSpace and aggregate types
    GM2_DEFINE_USING_CHECK(AffineSpace)

    GM2_DEFINE_MEMBER_CHECK(ASFrameDim)
    GM2_DEFINE_USING_CHECK(Point)
    GM2_DEFINE_USING_CHECK(ASFrame)

    // ProjectiveSpace types
    GM2_DEFINE_MEMBER_CHECK(VectorHDim)
    GM2_DEFINE_USING_CHECK(PointH)
    GM2_DEFINE_USING_CHECK(VectorH)
    GM2_DEFINE_USING_CHECK(ASFrameH)



    static constexpr auto value =

      // SpaceInfo and aggregate types checks
      GM2_HAS_USING(Space_T, SpaceInfo)
      and Is_SpaceInfo<typename Space_T::SpaceInfo>::value
      and GM2_HAS_USING(Space_T, Unit) and GM2_HAS_MEMBER(Space_T, VectorDim)
      and GM2_HAS_MEMBER(Space_T, FrameDim)

      // VectorSpace and aggregate types checks
      and GM2_HAS_USING(Space_T, VectorSpace)
      and Is_VectorSpace<typename Space_T::VectorSpace>::value
      and GM2_HAS_USING(Space_T, Vector)
      and GM2_HAS_USING(Space_T, Frame)

      // AffineSpace types checks
      and GM2_HAS_USING(Space_T, AffineSpace)
      and Is_VectorSpace<typename Space_T::AffineSpace>::value
      and GM2_HAS_MEMBER(Space_T, ASFrameDim) and GM2_HAS_USING(Space_T, Point)
      and GM2_HAS_USING(Space_T, ASFrame)

      // ProjectiveSpace type checks
      and GM2_HAS_MEMBER(Space_T, VectorHDim) and GM2_HAS_USING(Space_T, PointH)
      and GM2_HAS_USING(Space_T, VectorH) and GM2_HAS_USING(Space_T, ASFrameH);
  };



  template <typename SpaceObject_T>
  struct Is_ProjectiveSpaceObject {

    // Embed space types : ProjectiveSpace
    GM2_DEFINE_USING_CHECK(EmbedSpaceInfo)
    GM2_DEFINE_USING_CHECK(EmbedSpace)

    // ProjectiveSpace aggregate types : Dimensions
    GM2_DEFINE_MEMBER_CHECK(FrameDim)
    GM2_DEFINE_MEMBER_CHECK(VectorDim)
    GM2_DEFINE_MEMBER_CHECK(ASFrameDim)
    GM2_DEFINE_MEMBER_CHECK(VectorHDim)

    // ProjectiveSpace aggregate types
    GM2_DEFINE_USING_CHECK(Unit)

    // ProjectiveSpace aggregate types : vector space
    GM2_DEFINE_USING_CHECK(Vector)
    GM2_DEFINE_USING_CHECK(Frame)

    // ProjectiveSpace aggregate types : affine space
    GM2_DEFINE_USING_CHECK(Point)
    GM2_DEFINE_USING_CHECK(ASFrame)

    // ProjectiveSpace aggregate types : projective space
    GM2_DEFINE_USING_CHECK(PointH)
    GM2_DEFINE_USING_CHECK(VectorH)
    GM2_DEFINE_USING_CHECK(ASFrameH)


    ////////////////
    /// CHECKING ///
    ////////////////
    static constexpr auto value
      = GM2_HAS_USING(SpaceObject_T, EmbedSpaceInfo)
        and Is_SpaceInfo<typename SpaceObject_T::EmbedSpaceInfo>::value
        and GM2_HAS_USING(SpaceObject_T, EmbedSpace)
        and Is_ProjectiveSpace<typename SpaceObject_T::EmbedSpace>::value

        // ProjectiveSpace aggregate types : Dimensions
        and GM2_HAS_MEMBER(SpaceObject_T, FrameDim)
        and GM2_HAS_MEMBER(SpaceObject_T, VectorDim)
        and GM2_HAS_MEMBER(SpaceObject_T, ASFrameDim)
        and GM2_HAS_MEMBER(SpaceObject_T, VectorHDim)

        // ProjectiveSpace aggregate types
        and GM2_HAS_USING(SpaceObject_T, Unit)

        // ProjectiveSpace aggregate types : vector space
        and GM2_HAS_USING(SpaceObject_T, Vector)
        and GM2_HAS_USING(SpaceObject_T, Frame)

        // ProjectiveSpace aggregate types : affine space
        and GM2_HAS_USING(SpaceObject_T, Point)
        and GM2_HAS_USING(SpaceObject_T, ASFrame)

        // ProjectiveSpace aggregate types : projective space
        and GM2_HAS_USING(SpaceObject_T, PointH)
        and GM2_HAS_USING(SpaceObject_T, VectorH)
        and GM2_HAS_USING(SpaceObject_T, ASFrameH);
  };



  template <typename SpaceObject_T>
  struct Is_ParametricObject {

    // Embed space types : ProjectiveSpace
    GM2_DEFINE_USING_CHECK(EmbedSpaceInfo)
    GM2_DEFINE_USING_CHECK(EmbedSpace)

    // ProjectiveSpace aggregate types : Dimensions
    GM2_DEFINE_MEMBER_CHECK(FrameDim)
    GM2_DEFINE_MEMBER_CHECK(VectorDim)
    GM2_DEFINE_MEMBER_CHECK(ASFrameDim)
    GM2_DEFINE_MEMBER_CHECK(VectorHDim)

    // ProjectiveSpace aggregate types
    GM2_DEFINE_USING_CHECK(Unit)

    // ProjectiveSpace aggregate types : vector space
    GM2_DEFINE_USING_CHECK(Vector)
    GM2_DEFINE_USING_CHECK(Frame)

    // ProjectiveSpace aggregate types : affine space
    GM2_DEFINE_USING_CHECK(Point)
    GM2_DEFINE_USING_CHECK(ASFrame)

    // ProjectiveSpace aggregate types : projective space
    GM2_DEFINE_USING_CHECK(PointH)
    GM2_DEFINE_USING_CHECK(VectorH)
    GM2_DEFINE_USING_CHECK(ASFrameH)






    // Parametric space types : ProjectiveSpace
    GM2_DEFINE_USING_CHECK(PSpaceInfo)
    GM2_DEFINE_USING_CHECK(PSpace)

    // ProjectiveSpace aggregate types : Dimensions
    GM2_DEFINE_MEMBER_CHECK(PSpaceFrameDim)
    GM2_DEFINE_MEMBER_CHECK(PSpaceVectorDim)
    GM2_DEFINE_MEMBER_CHECK(PSpaceASFrameDim)
    GM2_DEFINE_MEMBER_CHECK(PSpaceVectorHDim)

    // ProjectiveSpace aggregate types
    GM2_DEFINE_USING_CHECK(PSpaceUnit)

    // ProjectiveSpace aggregate types : vector space
    GM2_DEFINE_USING_CHECK(PSpaceVector)
    GM2_DEFINE_USING_CHECK(PSpaceFrame)

    // ProjectiveSpace aggregate types : affine space
    GM2_DEFINE_USING_CHECK(PSpacePoint)
    GM2_DEFINE_USING_CHECK(PSpaceASFrame)

    // ProjectiveSpace aggregate types : projective space
    GM2_DEFINE_USING_CHECK(PSpacePointH)
    GM2_DEFINE_USING_CHECK(PSpaceVectorH)
    GM2_DEFINE_USING_CHECK(PSpaceASFrameH)

    // Parameter space function-parameter types
    GM2_DEFINE_USING_CHECK(PSpaceSizeArray)
    GM2_DEFINE_USING_CHECK(PSpaceBoolArray)






    // evaluationctrl::PObjEvalCtrl types
    GM2_DEFINE_USING_CHECK(PObjEvalCtrl)
    GM2_DEFINE_USING_CHECK(EvaluationResult)
    GM2_DEFINE_USING_CHECK(SamplingResult)










    ////////////////
    /// CHECKING ///
    ////////////////
    static constexpr auto value =
      // Embed SPACE
      GM2_HAS_USING(SpaceObject_T, EmbedSpaceInfo)
      and Is_SpaceInfo<typename SpaceObject_T::EmbedSpaceInfo>::value
      and GM2_HAS_USING(SpaceObject_T, EmbedSpace)
      and Is_ProjectiveSpace<typename SpaceObject_T::EmbedSpace>::value

      // ProjectiveSpace aggregate types : Dimensions
      and GM2_HAS_MEMBER(SpaceObject_T, FrameDim)
      and GM2_HAS_MEMBER(SpaceObject_T, VectorDim)
      and GM2_HAS_MEMBER(SpaceObject_T, ASFrameDim)
      and GM2_HAS_MEMBER(SpaceObject_T, VectorHDim)

      // ProjectiveSpace aggregate types
      and GM2_HAS_USING(SpaceObject_T, Unit)

      // ProjectiveSpace aggregate types : vector space
      and GM2_HAS_USING(SpaceObject_T, Vector)
      and GM2_HAS_USING(SpaceObject_T, Frame)

      // ProjectiveSpace aggregate types : affine space
      and GM2_HAS_USING(SpaceObject_T, Point)
      and GM2_HAS_USING(SpaceObject_T, ASFrame)

      // ProjectiveSpace aggregate types : projective space
      and GM2_HAS_USING(SpaceObject_T, PointH)
      and GM2_HAS_USING(SpaceObject_T, VectorH)
      and GM2_HAS_USING(SpaceObject_T, ASFrameH)



      // PARAMETRIC SPACE
      and GM2_HAS_USING(SpaceObject_T, PSpaceInfo)
      and Is_SpaceInfo<typename SpaceObject_T::PSpaceInfo>::value
      and GM2_HAS_USING(SpaceObject_T, PSpace)
      and Is_ProjectiveSpace<typename SpaceObject_T::PSpace>::value

      // ProjectiveSpace aggregate types : Dimensions
      and GM2_HAS_MEMBER(SpaceObject_T, PSpaceFrameDim)
      and GM2_HAS_MEMBER(SpaceObject_T, PSpaceVectorDim)
      and GM2_HAS_MEMBER(SpaceObject_T, PSpaceASFrameDim)
      and GM2_HAS_MEMBER(SpaceObject_T, PSpaceVectorHDim)

      // ProjectiveSpace aggregate types
      and GM2_HAS_USING(SpaceObject_T, PSpaceUnit)

      // ProjectiveSpace aggregate types : vector space
      and GM2_HAS_USING(SpaceObject_T, PSpaceVector)
      and GM2_HAS_USING(SpaceObject_T, PSpaceFrame)

      // ProjectiveSpace aggregate types : affine space
      and GM2_HAS_USING(SpaceObject_T, PSpacePoint)
      and GM2_HAS_USING(SpaceObject_T, PSpaceASFrame)

      // ProjectiveSpace aggregate types : projective space
      and GM2_HAS_USING(SpaceObject_T, PSpacePointH)
      and GM2_HAS_USING(SpaceObject_T, PSpaceVectorH)
      and GM2_HAS_USING(SpaceObject_T, PSpaceASFrameH)

      // Parameter space function-parameter types
      and GM2_HAS_USING(SpaceObject_T, PSpaceSizeArray)
      and GM2_HAS_USING(SpaceObject_T, PSpaceBoolArray)

      // SAMPLER
      and GM2_HAS_USING(SpaceObject_T, PObjEvalCtrl)
      and GM2_HAS_USING(SpaceObject_T, EvaluationResult)
      and GM2_HAS_USING(SpaceObject_T, SamplingResult)

      ;
  };


  template <typename PObjEvalCtrl_T>
  struct Is_PObjEvalCtrl {

    // Space types : Dimensions
    GM2_DEFINE_MEMBER_CHECK(FrameDim)
    GM2_DEFINE_MEMBER_CHECK(VectorDim)
    GM2_DEFINE_MEMBER_CHECK(ASFrameDim)
    GM2_DEFINE_MEMBER_CHECK(VectorHDim)

    // Space types
    GM2_DEFINE_USING_CHECK(Unit)

    // Space types : vector space
    GM2_DEFINE_USING_CHECK(Vector)
    GM2_DEFINE_USING_CHECK(Frame)

    // Space types : affine space
    GM2_DEFINE_USING_CHECK(Point)
    GM2_DEFINE_USING_CHECK(ASFrame)

    // Space types : projective space
    GM2_DEFINE_USING_CHECK(PointH)
    GM2_DEFINE_USING_CHECK(VectorH)
    GM2_DEFINE_USING_CHECK(ASFrameH)



    // Parametric Space types : Dimensions
    GM2_DEFINE_MEMBER_CHECK(PSpaceFrameDim)
    GM2_DEFINE_MEMBER_CHECK(PSpaceVectorDim)
    GM2_DEFINE_MEMBER_CHECK(PSpaceASFrameDim)
    GM2_DEFINE_MEMBER_CHECK(PSpaceVectorHDim)

    // Parametric Space types
    GM2_DEFINE_USING_CHECK(PSpaceUnit)

    // Parametric Space types : vector space
    GM2_DEFINE_USING_CHECK(PSpaceVector)
    GM2_DEFINE_USING_CHECK(PSpaceFrame)

    // Parametric Space types : affine space
    GM2_DEFINE_USING_CHECK(PSpacePoint)
    GM2_DEFINE_USING_CHECK(PSpaceASFrame)

    // Parametric Space types : projective space
    GM2_DEFINE_USING_CHECK(PSpacePointH)
    GM2_DEFINE_USING_CHECK(PSpaceVectorH)
    GM2_DEFINE_USING_CHECK(PSpaceASFrameH)

    // Parametric Space function-parameter types
    GM2_DEFINE_USING_CHECK(PSpaceSizeArray)
    GM2_DEFINE_USING_CHECK(PSpaceBoolArray)


    // Evaluation Control spesific types
    GM2_DEFINE_USING_CHECK(EvaluationResult)
    GM2_DEFINE_USING_CHECK(SamplingResult)



    ////////////////
    /// CHECKING ///
    ////////////////
    static constexpr auto value =

      // Space types : Dimensions
      GM2_HAS_MEMBER(PObjEvalCtrl_T, FrameDim)
      and GM2_HAS_MEMBER(PObjEvalCtrl_T, VectorDim)
      and GM2_HAS_MEMBER(PObjEvalCtrl_T, ASFrameDim)
      and GM2_HAS_MEMBER(PObjEvalCtrl_T, VectorHDim)

      // Space types
      and GM2_HAS_USING(PObjEvalCtrl_T, Unit)

      // Space types : vector space
      and GM2_HAS_USING(PObjEvalCtrl_T, Vector)
      and GM2_HAS_USING(PObjEvalCtrl_T, Frame)

      // Space types : affine space
      and GM2_HAS_USING(PObjEvalCtrl_T, Point)
      and GM2_HAS_USING(PObjEvalCtrl_T, ASFrame)

      // Space types : projective space
      and GM2_HAS_USING(PObjEvalCtrl_T, PointH)
      and GM2_HAS_USING(PObjEvalCtrl_T, VectorH)
      and GM2_HAS_USING(PObjEvalCtrl_T, ASFrameH)



      // Parametric space types : Dimensions
      and GM2_HAS_MEMBER(PObjEvalCtrl_T, PSpaceFrameDim)
      and GM2_HAS_MEMBER(PObjEvalCtrl_T, PSpaceVectorDim)
      and GM2_HAS_MEMBER(PObjEvalCtrl_T, PSpaceASFrameDim)
      and GM2_HAS_MEMBER(PObjEvalCtrl_T, PSpaceVectorHDim)

      // Parametric space types
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpaceUnit)

      // Parametric space types : vector space
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpaceVector)
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpaceFrame)

      // Parametric space types : affine space
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpacePoint)
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpaceASFrame)

      // Parametric space types : projective space
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpacePointH)
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpaceVectorH)
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpaceASFrameH)

      // Parameter space function-parameter types
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpaceSizeArray)
      and GM2_HAS_USING(PObjEvalCtrl_T, PSpaceBoolArray)

      // Evaluation control spesific types
      and GM2_HAS_USING(PObjEvalCtrl_T, EvaluationResult)
      and GM2_HAS_USING(PObjEvalCtrl_T, SamplingResult)

      ;
  };


  template <typename PSubObjEvalCtrl_T>
  struct Is_PSubObjEvalCtrl {

    // Object types
    GM2_DEFINE_USING_CHECK(SubParametricObject)
    GM2_DEFINE_USING_CHECK(PSpaceObject)
    GM2_DEFINE_USING_CHECK(ParametricObject)

    // Evaluation controller
    GM2_DEFINE_USING_CHECK(PObjEvalCtrl)

    // Evaluation result return type
    GM2_DEFINE_USING_CHECK(EvaluationResult)

    ////////////////
    /// CHECKING ///
    ////////////////
    static constexpr auto value =

      // Object types
      //      GM2_HAS_USING(PSubObjEvalCtrl_T, SubParametricObject)
      //      and
      Is_ParametricObject<
        typename PSubObjEvalCtrl_T::SubParametricObject>::value
      and GM2_HAS_USING(PSubObjEvalCtrl_T, PSpaceObject)
      and Is_ParametricObject<typename PSubObjEvalCtrl_T::PSpaceObject>::value
      and GM2_HAS_USING(PSubObjEvalCtrl_T, ParametricObject)
      and Is_ParametricObject<
            typename PSubObjEvalCtrl_T::ParametricObject>::value

      // Evaluation controller
      and GM2_HAS_USING(PSubObjEvalCtrl_T, PObjEvalCtrl)
      and Is_PObjEvalCtrl<typename PSubObjEvalCtrl_T::PObjEvalCtrl>::value

      // Evaluation result return type
      and GM2_HAS_USING(PSubObjEvalCtrl_T, EvaluationResult)

      ;
  };




}   // namespace gmlib2::traits





#endif   // GM2_CORE_TRAITS_H
