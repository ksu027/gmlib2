#ifndef GM2_CORE_SPACES_PROJECTIVESPACE_H
#define GM2_CORE_SPACES_PROJECTIVESPACE_H


#include "affinespace.h"




namespace gmlib2::spaces::projectivespace
{



  template <typename SpaceInfo_T>
  struct ProjectiveSpace : affinespace::AffineSpace<SpaceInfo_T> {

    // Base   : Affine Space
    // "Base" : Vector space
    using Base        = affinespace::AffineSpace<SpaceInfo_T>;
    using AffineSpace = Base;
    using VectorSpace = typename Base::VectorSpace;

    // Space_T
    using SpaceInfo                 = SpaceInfo_T;
    using Unit                      = typename SpaceInfo::Unit;
    static constexpr auto FrameDim  = VectorSpace::FrameDim;
    static constexpr auto VectorDim = VectorSpace::VectorDim;

    // Vector space
    using Vector = typename VectorSpace::Vector;
    using Frame  = typename VectorSpace::Frame;

    // Affine space
    static constexpr auto ASFrameDim = Base::ASFrameDim;
    using Point                      = typename Base::Point;
    using ASFrame                    = typename Base::ASFrame;

    // Projective space
    static constexpr auto VectorHDim = VectorDim + 1;
    using PointH                     = VectorT<Unit, VectorHDim>;
    using VectorH                    = VectorT<Unit, VectorHDim>;
    using ASFrameH                   = MatrixT<Unit, VectorHDim, ASFrameDim>;
  };




  template <typename ProjectiveSpace_T>
  const typename ProjectiveSpace_T::ASFrameH identityFrame()
  {
    using Unit      = typename ProjectiveSpace_T::Unit;
    auto ASFrameDim = ProjectiveSpace_T::ASFrameDim;
    auto VectorHDim = ProjectiveSpace_T::VectorHDim;

    typename ProjectiveSpace_T::ASFrameH I(Unit(0));
    // blaze::submatrix<0UL, 0UL, VectorHDim - 1, ASFrameDim - 1>(I) // next
    // blaze
    blaze::submatrix(I, 0UL, 0UL, VectorHDim - 1, ASFrameDim - 1)
      = vectorspace::identityFrame<typename ProjectiveSpace_T::VectorSpace>();
    I(VectorHDim - 1, ASFrameDim - 1) = Unit(1);
    return I;
  }

  template <typename ProjectiveSpace_T>
  auto aSpaceFrame(const typename ProjectiveSpace_T::ASFrameH& frame)
  {
    return blaze::submatrix<0UL, 0UL, ProjectiveSpace_T::VectorDim,
                            ProjectiveSpace_T::ASFrameDim>(frame);
  }

  template <typename ProjectiveSpace_T>
  auto vSpaceFrame(const typename ProjectiveSpace_T::ASFrameH& frame)
  {
    return blaze::submatrix<0UL, 0UL, ProjectiveSpace_T::VectorDim,
                            ProjectiveSpace_T::FrameDim>(frame);
  }

  template <typename ProjectiveSpace_T>
  auto pSpaceFrameFromDup(const typename ProjectiveSpace_T::Vector& dir,
                          const typename ProjectiveSpace_T::Vector& up,
                          const typename ProjectiveSpace_T::Point&  origin)
  {

    //      if constexpr (ProjectiveSpace_T::VectorDim == 3) {

    using ReturnType = typename ProjectiveSpace_T::ASFrameH;

    auto asframeh = identityFrame<ProjectiveSpace_T>();
    auto asframe  = blaze::submatrix<0UL, 0UL, ProjectiveSpace_T::VectorDim,
                                    ProjectiveSpace_T::ASFrameDim>(asframeh);

    auto fdir  = blaze::column<0UL>(asframe);
    auto fside = blaze::column<1UL>(asframe);
    auto fup   = blaze::column<2UL>(asframe);
    auto fpos  = blaze::column<3UL>(asframe);

    fdir  = blaze::normalize(dir);
    fup   = blaze::normalize(up - (up * dir) * dir);
    fside = blaze::normalize(blaze::cross(fup, fdir));
    fpos  = blaze::evaluate(origin);

    return ReturnType(asframeh);
    //      }
  }


  namespace detail
  {
    template <typename ProjectiveSpace_T, size_t COL_T>
    auto aSpaceFrameColumn(const typename ProjectiveSpace_T::ASFrameH& frame)
    {
      return blaze::column<0UL>(
        blaze::submatrix<0UL, COL_T, ProjectiveSpace_T::VectorDim, 1UL>(frame));
    }
  }   // namespace detail

  /*!
   * Returns a frames direction-axis, given a projective frame, with respect
   * to a parent space
   */
  template <typename ProjectiveSpace_T>
  auto directionAxis(const typename ProjectiveSpace_T::ASFrameH& frame)
  {
    return detail::aSpaceFrameColumn<ProjectiveSpace_T, 0UL>(frame);
  }

  template <typename ProjectiveSpace_T>
  auto sideAxis(const typename ProjectiveSpace_T::ASFrameH& frame)
  {
    return detail::aSpaceFrameColumn<ProjectiveSpace_T, 1UL>(frame);
  }

  template <typename ProjectiveSpace_T>
  auto upAxis(const typename ProjectiveSpace_T::ASFrameH& frame)
  {
    return detail::aSpaceFrameColumn<ProjectiveSpace_T, 2UL>(frame);
  }

  template <typename ProjectiveSpace_T>
  auto frameOrigin(const typename ProjectiveSpace_T::ASFrameH& frame)
  {
    return detail::aSpaceFrameColumn<ProjectiveSpace_T, 3UL>(frame);
  }


  /*!
   *  Returns a frames direction-axis, in homogenuous coordinates, with
   * respect to a parent space
   */
  template <typename ProjectiveSpace_T>
  auto directionAxisH(const typename ProjectiveSpace_T::ASFrameH& frame)
  {
    return blaze::column<0UL>(frame);
  }

  template <typename ProjectiveSpace_T>
  auto sideAxisH(const typename ProjectiveSpace_T::ASFrameH& hframe)
  {
    return blaze::column<1UL>(hframe);
  }

  template <typename ProjectiveSpace_T>
  auto upAxisH(const typename ProjectiveSpace_T::ASFrameH& hframe)
  {
    return blaze::column<2UL>(hframe);
  }

  template <typename ProjectiveSpace_T>
  auto frameOriginH(const typename ProjectiveSpace_T::ASFrameH& hframe)
  {
    return blaze::column<3UL>(hframe);
  }



  // clang-format off
    //   -- Concept TS
    //   template <ProjectiveSpaceConcept&&> void move(...) {}
    //   -- C++20
    //   template <typename ProjectiveSpace_T> void move(...) ->
    //     ProjectiveSpaceConcept<ProjectiveSpace_T> {}
    //   -- Type traits
    //   template <typename ProjectiveSpace_T>
    //     enable_if_t<is_projectivespace<ProjectiveSpace_T>,void>
    //     move(...) {}
  // clang-format on
  template <typename ProjectiveSpace_T>
  void translateParent(typename ProjectiveSpace_T::ASFrameH& homogenous_frame,
                       const typename ProjectiveSpace_T::Vector& direction)
  {
    constexpr auto ASFrameDim = ProjectiveSpace_T::ASFrameDim;
    constexpr auto VectorDim  = ProjectiveSpace_T::VectorDim;
    blaze::column(
      blaze::submatrix(homogenous_frame, 0UL, 0UL, VectorDim, ASFrameDim), 3UL)
      += direction;
  }

  template <typename ProjectiveSpace_T>
  std::enable_if_t<ProjectiveSpace_T::VectorDim == 3, void>
  rotateParent(typename ProjectiveSpace_T::ASFrameH& frame, double ang,
               const typename ProjectiveSpace_T::Vector& axis)
  {
    const auto rot_frame = algorithms::rotationMatrix(ang, axis);

    const auto dir  = projectivespace::directionAxis<ProjectiveSpace_T>(frame);
    const auto side = projectivespace::sideAxis<ProjectiveSpace_T>(frame);
    const auto up   = projectivespace::upAxis<ProjectiveSpace_T>(frame);

    blaze::subvector<0UL, 3UL>(blaze::column<0UL>(frame)) = rot_frame * dir;
    blaze::subvector<0UL, 3UL>(blaze::column<1UL>(frame)) = rot_frame * side;
    blaze::subvector<0UL, 3UL>(blaze::column<2UL>(frame)) = rot_frame * up;
  }


}   // namespace gmlib2::spaces::projectivespace



#endif   // GM2_CORE_SPACES_PROJECTIVESPACE_H
