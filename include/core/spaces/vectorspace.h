#ifndef GM2_CORE_SPACES_VECTORSPACE_H
#define GM2_CORE_SPACES_VECTORSPACE_H


#include "../space.h"


namespace gmlib2::spaces::vectorspace
{

  template <typename SpaceInfo_T>
  struct VectorSpace {

    // Space_T
    using SpaceInfo                 = SpaceInfo_T;
    using Unit                      = typename SpaceInfo::Unit;
    static constexpr auto FrameDim  = SpaceInfo_T::FrameDim;
    static constexpr auto VectorDim = SpaceInfo_T::VectorDim;

    // VectorSpace
    using Vector = VectorT<Unit, VectorDim>;
    using Frame  = MatrixT<Unit, VectorDim, FrameDim>;
  };


  template <typename VectorSpace_T>
  const typename VectorSpace_T::Frame identityFrame()
  {
    using Unit     = typename VectorSpace_T::Unit;
    auto FrameDim  = VectorSpace_T::FrameDim;
    auto VectorDim = VectorSpace_T::VectorDim;
    auto MinDim    = std::min(VectorDim, FrameDim);

    typename VectorSpace_T::Frame I(0);
    for (size_t i = 0; i < MinDim; ++i) I(i, i) = Unit(1);
    //      blaze::diagonal(I) = VectorT<Unit, MinDim>(1); // Next blaze
    return I;
  }

}   // namespace gmlib2::spaces::vectorspace



#endif   // GM2_CORE_SPACES_VECTORSPACE_H
