#ifndef GM2_CORE_SPACES_AFFINESPACE_H
#define GM2_CORE_SPACES_AFFINESPACE_H


#include "vectorspace.h"




namespace gmlib2::spaces::affinespace
{


  template <typename SpaceInfo_T>
  struct AffineSpace {

    // "Base" : Vector space
    using VectorSpace = vectorspace::VectorSpace<SpaceInfo_T>;

    // SpaceInfo_T
    using SpaceInfo                 = SpaceInfo_T;
    using Unit                      = typename VectorSpace::Unit;
    static constexpr auto FrameDim  = VectorSpace::FrameDim;
    static constexpr auto VectorDim = VectorSpace::VectorDim;

    // Vector space
    using Vector = typename VectorSpace::Vector;
    using Frame  = typename VectorSpace::Frame;

    // Affine space
    static constexpr auto ASFrameDim = FrameDim + 1;
    using Point                      = VectorT<Unit, VectorDim>;
    using ASFrame                    = MatrixT<Unit, VectorDim, ASFrameDim>;
  };









  template <typename AffineSpace_T>
  auto vSpaceFrame(const typename AffineSpace_T::ASFrameH& frame)
  {
    return blaze::submatrix<0UL, 0UL, AffineSpace_T::VectorDim,
                            AffineSpace_T::ASFrameDim>(frame);
  }

}   // namespace gmlib2::spaces::affinespace


#endif   // GM2_CORE_SPACES_AFFINESPACE_H
