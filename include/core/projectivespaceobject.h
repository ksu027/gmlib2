#ifndef GM2_PROJECTIVESPACEOBJECT_H
#define GM2_PROJECTIVESPACEOBJECT_H

// gmlib2
#include "spaces/projectivespace.h"

// stl
#include <cassert>
#include <iostream>



namespace gmlib2
{

  template <typename EmbedSpaceInfo_T = spaces::D3R3SpaceInfo<>>
  struct ProjectiveSpaceObject {

    // SpaceObjectBase
    using BaseSpaceObjectBase = ProjectiveSpaceObject;

    // Embedded space
    using EmbedSpaceInfo = EmbedSpaceInfo_T;
    using EmbedSpace = spaces::projectivespace::ProjectiveSpace<EmbedSpaceInfo>;

    // Dimensions
    static constexpr auto FrameDim   = EmbedSpace::FrameDim;
    static constexpr auto VectorDim  = EmbedSpace::VectorDim;
    static constexpr auto ASFrameDim = EmbedSpace::ASFrameDim;
    static constexpr auto VectorHDim = EmbedSpace::VectorHDim;

    // Embedded space types
    using Unit = typename EmbedSpace::Unit;

    // Embedded space types: vector space
    using Vector = typename EmbedSpace::Vector;
    using Frame  = typename EmbedSpace::Frame;

    // Embedded space types: affine space
    using Point   = typename EmbedSpace::Point;
    using ASFrame = typename EmbedSpace::ASFrame;

    // Embedded space types: projective space
    using PointH   = typename EmbedSpace::PointH;
    using VectorH  = typename EmbedSpace::VectorH;
    using ASFrameH = typename EmbedSpace::ASFrameH;



    // Constructors
    ProjectiveSpaceObject();
    virtual ~ProjectiveSpaceObject() = default;


    //! Return the SpaceObject's projective-space frame;
    //! with respect to parent coordinates
    ASFrameH pSpaceFrameParent() const;

    //! Return the SpaceObject's affine-space frame;
    //! with respect to parent coordinates
    ASFrame aSpaceFrameParent() const;

    //! Return the SpaceObject's vector-space frame;
    //! with respect to parent coordinates
    Frame vSpaceFrameParent() const;


    //! Return the SpaceObject frame's direction-axis;
    //!  with respect to parent space
    const Vector directionAxisParent() const;

    //! Return the SpaceObject frame's side-axis;
    //! with respect to parent space
    const Vector sideAxisParent() const;

    //! Return the SpaceObject frame's up-axis;
    //! with respect to parent space
    const Vector upAxisParent() const;

    //! Return the SpaceObject frame's origin;
    //! with respect to parent space
    const Point  frameOriginParent() const;


    //! Return the SpaceObject frame's direction-axis
    //! in homogeneous coordinates;
    //! with respect to parent space
    const VectorH directionAxisParentH() const;

    //! Return the SpaceObject frame's side-axis in homogeneous coordinates;
    //! with respect to parent space
    const VectorH sideAxisParentH() const;

    //! Return the SpaceObject frame's up-axis in homogeneous coordinates;
    //! with respect to parent space
    const VectorH upAxisParentH() const;

    //! Return the SpaceObject frame's origin in homogeneous coordinates;
    //! with respect to parent space
    const PointH  frameOriginParentH() const;


    //! Apply a translation "locally" to the space object;
    //! before the current transformation stack
    virtual void translateLocal(const Vector& v);

    //! Apply a rotation "locally" to the space object;
    //! before the current transformation stack
    virtual void rotateLocal(double ang, const Vector& axis);

    //! Apply a translation to the space object; with respect to a parent frame
    virtual void translateParent(const Vector& v);

    //! Apply a rotation to the space object; with respect to a parent frame
    virtual void rotateParent(double ang, const Vector& axis);


    //! Set the objects frame; in referrece of a parent space
    //! Right-hand coordinate system
    //! Orthogonal frame: [[dir,side,up],origin]
    virtual void setFrameParent(const Vector& direction_axis,
                                const Vector& up_axis,
                                const Point&  frame_origin);


    // Members
  private:
    /*!
     * Right-hand-system: [dir,side,up,pos]
     * --
     * the projective space object's frame in the projective space
     */
    ASFrameH m_frame{spaces::projectivespace::identityFrame<EmbedSpace>()};
  };










  template <typename EmbedSpaceInfo_T>
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::ProjectiveSpaceObject()
  {
    //    setFrame(m_pos, m_dir, m_up);
  }

  /*!
   * Return the frame of the associated projective space
   */
  template <typename EmbedSpaceInfo_T>
  typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::ASFrameH
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::pSpaceFrameParent() const
  {
    return m_frame;
  }

  template <typename EmbedSpaceInfo_T>
  typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::ASFrame
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::aSpaceFrameParent() const
  {
    return spaces::projectivespace::aSpaceFrame<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Frame
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::vSpaceFrameParent() const
  {
    return spaces::projectivespace::vSpaceFrame<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Vector
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::directionAxisParent() const
  {
    return spaces::projectivespace::directionAxis<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Vector
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::sideAxisParent() const
  {
    return spaces::projectivespace::sideAxis<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Vector
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::upAxisParent() const
  {
    return spaces::projectivespace::upAxis<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::Point
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::frameOriginParent() const
  {
    return spaces::projectivespace::frameOrigin<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::VectorH
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::directionAxisParentH() const
  {
    return spaces::projectivespace::directionAxisH<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::VectorH
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::sideAxisParentH() const
  {
    return spaces::projectivespace::sideAxisH<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::VectorH
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::upAxisParentH() const
  {
    return spaces::projectivespace::upAxisH<EmbedSpace>(pSpaceFrameParent());
  }

  template <typename EmbedSpaceInfo_T>
  const typename ProjectiveSpaceObject<EmbedSpaceInfo_T>::PointH
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::frameOriginParentH() const
  {
    return spaces::projectivespace::frameOriginH<EmbedSpace>(pSpaceFrameParent());
  }



  template <typename EmbedSpaceInfo_T>
  void ProjectiveSpaceObject<EmbedSpaceInfo_T>::translateLocal(
    const Vector& v)
  {
    translateParent(vSpaceFrameParent() * v);
  }

  template <typename EmbedSpaceInfo_T>
  void ProjectiveSpaceObject<EmbedSpaceInfo_T>::rotateLocal(
    double ang, const Vector& axis)
  {
    rotateParent(ang, vSpaceFrameParent() * axis);
  }


  template <typename EmbedSpaceInfo_T>
  void
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::translateParent(const Vector& v)
  {
    spaces::projectivespace::translateParent<EmbedSpace>(m_frame, v);
  }

  template <typename EmbedSpaceInfo_T>
  void
  ProjectiveSpaceObject<EmbedSpaceInfo_T>::rotateParent(double             ang,
                                                        const Vector& axis)
  {


    if constexpr (VectorDim == 3) {
      spaces::projectivespace::rotateParent<EmbedSpace>(m_frame, ang, axis);
    }
  }

  template <typename EmbedSpaceInfo_T>
  void ProjectiveSpaceObject<EmbedSpaceInfo_T>::setFrameParent(
    const Vector& dir, const Vector& up, const Point& origin)
  {

    if constexpr (VectorDim == 3UL) {

      m_frame = spaces::projectivespace::pSpaceFrameFromDup<EmbedSpace>(dir, up,
                                                                        origin);


//      // subview(s)
//      auto asframe = blaze::submatrix<0UL, 0UL, VectorDim, ASFrameDim>(m_frame);
//      auto fdir    = blaze::column<0UL>(asframe);
//      auto fside   = blaze::column<1UL>(asframe);
//      auto fup     = blaze::column<2UL>(asframe);
//      auto fpos    = blaze::column<3UL>(asframe);

//      // values
//      fdir  = blaze::normalize(dir);
//      fup   = blaze::normalize(up - (up * dir) * dir);
//      fside = blaze::normalize(blaze::cross(fup, fdir));
//      fpos  = blaze::evaluate(origin);
    }
  }

}   // namespace gmlib2

#endif   // GM2_PROJECTIVESPACEOBJECT_H
