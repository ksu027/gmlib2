#ifndef GM2_PARAMETRIC_PARAMETRICOBJECT_H
#define GM2_PARAMETRIC_PARAMETRICOBJECT_H

// gmlib2
#include "../core/projectivespaceobject.h"
#include "../core/coreutils.h"

// stl
#include <array>
#include <mutex>


#if 1






#define GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES                             \
  /*********************************************/                              \
  /* Presumes the required definition of      **/                              \
  /* the parametric base-class-using:         **/                              \
  /*                                          **/                              \
  /* Base                                     **/                              \
  /*********************************************/                              \
                                                                               \
  /*******************/                                                        \
  /* SpaceObjectBase */                                                        \
  using BaseSpaceObjectBase = typename Base::BaseSpaceObjectBase;              \
                                                                               \
  /**************/                                                             \
  /* Embed Space*/                                                             \
  using EmbedSpaceObject = Base;                                               \
  using EmbedSpaceInfo   = typename EmbedSpaceObject::EmbedSpaceInfo;          \
  using EmbedSpace       = typename EmbedSpaceObject::EmbedSpace;              \
                                                                               \
  /* Dimensions */                                                             \
  static constexpr auto FrameDim   = EmbedSpace::FrameDim;                     \
  static constexpr auto VectorDim  = EmbedSpace::VectorDim;                    \
  static constexpr auto ASFrameDim = EmbedSpace::ASFrameDim;                   \
  static constexpr auto VectorHDim = EmbedSpace::VectorHDim;                   \
                                                                               \
  /* Embedded space types */                                                   \
  using Unit = typename EmbedSpace::Unit;                                      \
                                                                               \
  /* Embedded space types : vector space */                                    \
  using Vector = typename EmbedSpace::Vector;                                  \
  using Frame  = typename EmbedSpace::Frame;                                   \
                                                                               \
  /* Embedded space types : affine space */                                    \
  using Point   = typename EmbedSpace::Point;                                  \
  using ASFrame = typename EmbedSpace::ASFrame;                                \
                                                                               \
  /* Embedded space types : projective space */                                \
  using PointH   = typename EmbedSpace::PointH;                                \
  using VectorH  = typename EmbedSpace::VectorH;                               \
  using ASFrameH = typename EmbedSpace::ASFrameH;                              \
                                                                               \
                                                                               \
  /*******************/                                                        \
  /* Parametric Space*/                                                        \
  using PSpaceInfo = typename EmbedSpaceObject::PSpaceInfo;                    \
  using PSpace     = typename EmbedSpaceObject::PSpace;                        \
                                                                               \
  /* Parametric space dimensions */                                            \
  static constexpr auto PSpaceFrameDim   = PSpace::FrameDim;                   \
  static constexpr auto PSpaceVectorDim  = PSpace::VectorDim;                  \
  static constexpr auto PSpaceASFrameDim = PSpace::ASFrameDim;                 \
  static constexpr auto PSpaceVectorHDim = PSpace::VectorHDim;                 \
                                                                               \
  /* Parametric space types */                                                 \
  using PSpaceUnit = typename PSpace::Unit;                                    \
                                                                               \
  /* Parametric space types : vector space */                                  \
  using PSpaceVector = typename PSpace::Vector;                                \
  using PSpaceFrame  = typename PSpace::Frame;                                 \
                                                                               \
  /* Parametric space types : affine space */                                  \
  using PSpacePoint   = typename PSpace::Point;                                \
  using PSpaceASFrame = typename PSpace::ASFrame;                              \
                                                                               \
  /* Parametric space types : projective space */                              \
  using PSpacePointH   = typename PSpace::PointH;                              \
  using PSpaceVectorH  = typename PSpace::VectorH;                             \
  using PSpaceASFrameH = typename PSpace::ASFrameH;                            \
                                                                               \
                                                                               \
  /**********************************/                                         \
  /* Paramter container space types */                                         \
  using PSpaceSizeArray = typename EmbedSpaceObject::PSpaceSizeArray;          \
  using PSpaceBoolArray = typename EmbedSpaceObject::PSpaceBoolArray;          \
                                                                               \
                                                                               \
  /***********/                                                                \
  /* PObjEvalCtrl */                                                        \
  using PObjEvalCtrl   = typename EmbedSpaceObject::PObjEvalCtrl;          \
  using EvaluationResult = typename EmbedSpaceObject::EvaluationResult;        \
  using SamplingResult   = typename EmbedSpaceObject::SamplingResult;

#else
#define GM2_DEFINE_PARAMETRIC_OBJECT_DEFAULT_TYPES
#endif




#if 1
#define GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_EVALUATIONCTRL_TYPES              \
  /*********************************************/                              \
  /* Presumes the following                   **/                              \
  /* template parameter:                      **/                              \
  /*                                          **/                              \
  /* ParametricObject_T                       **/                              \
  /*********************************************/                              \
                                                                               \
  /***************/                                                            \
  /* Embed Space */                                                            \
                                                                               \
  /* Dimensions */                                                             \
  static constexpr auto FrameDim   = ParametricObject_T::FrameDim;             \
  static constexpr auto VectorDim  = ParametricObject_T::VectorDim;            \
  static constexpr auto ASFrameDim = ParametricObject_T::ASFrameDim;           \
  static constexpr auto VectorHDim = ParametricObject_T::VectorHDim;           \
                                                                               \
  /* Embedded space types */                                                   \
  using Unit = typename ParametricObject_T::Unit;                              \
                                                                               \
  /* Embedded space types : vector space */                                    \
  using Vector = typename ParametricObject_T::Vector;                          \
  using Frame  = typename ParametricObject_T::Frame;                           \
                                                                               \
  /* Embedded space types : affine space */                                    \
  using Point   = typename ParametricObject_T::Point;                          \
  using ASFrame = typename ParametricObject_T::ASFrame;                        \
                                                                               \
  /* Embedded space types : projective space */                                \
  using PointH   = typename ParametricObject_T::PointH;                        \
  using VectorH  = typename ParametricObject_T::VectorH;                       \
  using ASFrameH = typename ParametricObject_T::ASFrameH;                      \
                                                                               \
                                                                               \
  /********************/                                                       \
  /* Parametric Space */                                                       \
                                                                               \
  /* Parametric space dimensions */                                            \
  static constexpr auto PSpaceFrameDim  = ParametricObject_T::PSpaceFrameDim;  \
  static constexpr auto PSpaceVectorDim = ParametricObject_T::PSpaceVectorDim; \
  static constexpr auto PSpaceASFrameDim                                       \
    = ParametricObject_T::PSpaceASFrameDim;                                    \
  static constexpr auto PSpaceVectorHDim                                       \
    = ParametricObject_T::PSpaceVectorHDim;                                    \
                                                                               \
  /* Parametric space types */                                                 \
  using PSpaceUnit = typename ParametricObject_T::PSpaceUnit;                  \
                                                                               \
  /* Parametric space types : vector space */                                  \
  using PSpaceVector = typename ParametricObject_T::PSpaceVector;              \
  using PSpaceFrame  = typename ParametricObject_T::PSpaceFrame;               \
                                                                               \
  /* Parametric space types : affine space */                                  \
  using PSpacePoint   = typename ParametricObject_T::PSpacePoint;              \
  using PSpaceASFrame = typename ParametricObject_T::PSpaceASFrame;            \
                                                                               \
  /* Parametric space types : projective space */                              \
  using PSpacePointH   = typename ParametricObject_T::PSpacePointH;            \
  using PSpaceVectorH  = typename ParametricObject_T::PSpaceVectorH;           \
  using PSpaceASFrameH = typename ParametricObject_T::PSpaceASFrameH;          \
                                                                               \
  /**********************************/                                         \
  /* Paramter container space types */                                         \
  using PSpaceSizeArray = typename ParametricObject_T::PSpaceSizeArray;        \
  using PSpaceBoolArray = typename ParametricObject_T::PSpaceBoolArray;

#else
#define GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_EVALUATIONCTRL_TYPES
#endif









namespace gmlib2::spaces
{

  template <size_t F, size_t V, typename Unit_T = double>
  struct ParameterFVSpaceInfo {
    using Unit                      = Unit_T;
    static constexpr auto FrameDim  = F;
    static constexpr auto VectorDim = V;
  };

  template <size_t V, typename Unit_T = double>
  struct ParameterVSpaceInfo : ParameterFVSpaceInfo<V, V, Unit_T> {
  };

}   // namespace gmlib2::spaces

namespace gmlib2::parametric
{


  ////////////////////////
  /// ParametricObject ///
  ////////////////////////
  template <typename ParametricSpaceInfo_T, typename ParametricObjectType_Tag_T,
            typename SpaceObjectEmbedBase_T,
            template <typename PObjEvalCtrl_PO_T> typename PObjEvalCtrl_T>
  struct ParametricObject : SpaceObjectEmbedBase_T {
    using BaseSpaceObjectBase =
      typename SpaceObjectEmbedBase_T::BaseSpaceObjectBase;
    using Base                      = SpaceObjectEmbedBase_T;
    using ParametricObjectEmbedBase = Base;
    using ParametricObjectType_Tag  = ParametricObjectType_Tag_T;


    /////////////////
    // Embedded space
    using EmbedSpaceObject = Base;
    using EmbedSpaceInfo   = typename EmbedSpaceObject::EmbedSpaceInfo;
    using EmbedSpace       = typename EmbedSpaceObject::EmbedSpace;

    // Dimensions
    static constexpr auto FrameDim   = EmbedSpace::FrameDim;
    static constexpr auto VectorDim  = EmbedSpace::VectorDim;
    static constexpr auto ASFrameDim = EmbedSpace::ASFrameDim;
    static constexpr auto VectorHDim = EmbedSpace::VectorHDim;

    // Embedded space types
    using Unit = typename EmbedSpace::Unit;

    // Embedded space types: vector space
    using Vector = typename EmbedSpace::Vector;
    using Frame  = typename EmbedSpace::Frame;

    // Embedded space types: affine space
    using Point   = typename EmbedSpace::Point;
    using ASFrame = typename EmbedSpace::ASFrame;

    // Embedded space types: projective space
    using PointH   = typename EmbedSpace::PointH;
    using VectorH  = typename EmbedSpace::VectorH;
    using ASFrameH = typename EmbedSpace::ASFrameH;


    ///////////////////
    // Parametric space
    using PSpaceInfo = ParametricSpaceInfo_T;
    using PSpace     = spaces::projectivespace::ProjectiveSpace<PSpaceInfo>;

    // Dimensions
    static constexpr auto PSpaceFrameDim   = PSpace::FrameDim;
    static constexpr auto PSpaceVectorDim  = PSpace::VectorDim;
    static constexpr auto PSpaceASFrameDim = PSpace::ASFrameDim;
    static constexpr auto PSpaceVectorHDim = PSpace::VectorHDim;

    // Embedded space types
    using PSpaceUnit = typename PSpace::Unit;

    // Embedded space types: vector space
    using PSpaceVector = typename PSpace::Vector;
    using PSpaceFrame  = typename PSpace::Frame;

    // Embedded space types: affine space
    using PSpacePoint   = typename PSpace::Point;
    using PSpaceASFrame = typename PSpace::ASFrame;

    // Embedded space types: projective space
    using PSpacePointH   = typename PSpace::PointH;
    using PSpaceVectorH  = typename PSpace::VectorH;
    using PSpaceASFrameH = typename PSpace::ASFrameH;

    // Paramter space types
    using PSpaceSizeArray = std::array<size_t, PSpaceVectorDim>;
    using PSpaceBoolArray = std::array<bool, PSpaceVectorDim>;





    // PObjEvalCtrl
    using PObjEvalCtrl     = PObjEvalCtrl_T<ParametricObject>;
    using EvaluationResult = typename PObjEvalCtrl::EvaluationResult;
    using SamplingResult   = typename PObjEvalCtrl::SamplingResult;

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult evaluateLocal(
      const PSpacePoint&     par,
      const PSpaceSizeArray& no_der
      = utils::initStaticContainer<PSpaceSizeArray, PSpaceVectorDim>(0UL),
      const PSpaceBoolArray& from_left
      = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        true)) const;

    EvaluationResult evaluateParent(
      const PSpacePoint&     par,
      const PSpaceSizeArray& no_der
      = utils::initStaticContainer<PSpaceSizeArray, PSpaceVectorDim>(0UL),
      const PSpaceBoolArray& from_left
      = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        true)) const;


    // Interface
    virtual PSpaceBoolArray isClosed() const        = 0;
    virtual PSpacePoint     startParameters() const = 0;
    virtual PSpacePoint     endParameters() const   = 0;

  protected:
    virtual EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& from_left) const = 0;
  };





  template <typename ParametricSpaceInfo_T, typename ParametricObjectType_Tag_T,
            typename SpaceObjectEmbedBase_T,
            template <typename PObjEvalCtrl_PO_T> typename PObjEvalCtrl_T>
  typename ParametricObject<ParametricSpaceInfo_T, ParametricObjectType_Tag_T,
                            SpaceObjectEmbedBase_T,
                            PObjEvalCtrl_T>::EvaluationResult
  ParametricObject<
    ParametricSpaceInfo_T, ParametricObjectType_Tag_T, SpaceObjectEmbedBase_T,
    PObjEvalCtrl_T>::evaluateLocal(const PSpacePoint&     par,
                                   const PSpaceSizeArray& no_der,
                                   const PSpaceBoolArray& from_left) const
  {
    return evaluate(par, no_der, from_left);
  }

  template <typename ParametricSpaceInfo_T, typename ParametricObjectType_Tag_T,
            typename SpaceObjectEmbedBase_T,
            template <typename PObjEvalCtrl_PO_T> typename PObjEvalCtrl_T>
  typename ParametricObject<ParametricSpaceInfo_T, ParametricObjectType_Tag_T,
                            SpaceObjectEmbedBase_T,
                            PObjEvalCtrl_T>::EvaluationResult
  ParametricObject<
    ParametricSpaceInfo_T, ParametricObjectType_Tag_T, SpaceObjectEmbedBase_T,
    PObjEvalCtrl_T>::evaluateParent(const PSpacePoint&     par,
                                    const PSpaceSizeArray& no_derivatives,
                                    const PSpaceBoolArray& from_left) const
  {
    const auto eval_res = evaluate(par, no_derivatives, from_left);
    const auto pframe   = this->pSpaceFrameParent();

    EvaluationResult res = blaze::map(eval_res, [pframe](const auto& ele) {
      return blaze::evaluate(pframe * ele);
    });
    return res;
  }




}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_PARAMETRICOBJECT_H
