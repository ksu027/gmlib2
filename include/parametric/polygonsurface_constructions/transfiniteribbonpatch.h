#ifndef GM2_PARAMETRIC_POLYGONSURFACE_TRANSFINITERIBBONPATCH_H
#define GM2_PARAMETRIC_POLYGONSURFACE_TRANSFINITERIBBONPATCH_H

// gmlib2

#include "../polygonsurface.h"

namespace gmlib2::parametric
{
  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl>
  class TransfiniteRibbonPatch
    : public PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
    using Base = PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES


    using RibbonPoints   = std::vector<Point>;
    using RibbonTangents = std::vector<Vector>;

    using Lambda  = typename Base::Lambda;
    using Lambdas = typename Base::Lambdas;

    template <typename... Ts>
    TransfiniteRibbonPatch(const RibbonPoints& P, const RibbonTangents& T,
                           Ts&&... ts)
      : Base(P.size(), std::forward<Ts>(ts)...), m_P(P), m_T(T)
    {
      const auto N = P.size();

      assert(N >= 3);
      if (N < 3) throw std::runtime_error("Need at least 3 sides");
    }


  public:
    RibbonPoints   m_P;
    RibbonTangents m_T;

  public:
    PSpaceBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        false);
    }
    PSpacePoint startParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(0.0);
    }
    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(1.0);
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint&     par,
             const PSpaceSizeArray& no_der
             = utils::initStaticContainer<PSpaceSizeArray, PSpaceVectorDim>(1),
             const PSpaceBoolArray& /*from_left*/
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override
    {

      const auto eval_fn
        = [this](const auto& at_par) { return evaluatePosition(at_par); };

      // Results
      EvaluationResult p(no_der[0] + 1, no_der[1] + 1);

      // S
      p(0, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        eval_fn(par), Unit(1));

      // Du S
      if (no_der[0] > 0) {

        const auto par_d0 = PSpaceVector{1e-6, 0};
        const auto Ds0    = m_D(par, par_d0, eval_fn);

        p(1, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds0, Unit(1));
      }

      // Dv S
      if (no_der[1] > 0) {
        const auto par_d1 = PSpaceVector{0, 1e-6};
        const auto Ds1    = m_D(par, par_d1, eval_fn);

        p(0, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds1, Unit(1));
      }

      // Duv S
      if (no_der[0] > 0 and no_der[1] > 0)
        p(1, 1) = typename EvaluationResult::ElementType(0);

      return p;
    }


  private:
    Lambda gbc(const PSpacePoint& x) const
    {
      return gbc::mvc(this->polygon2D(), x);
    }

    std::vector<PSpaceUnit> computeSis(const Lambdas& L) const
    {

      std::vector<PSpaceUnit> Sis;
      Sis.reserve(this->sides());
      std::transform(std::begin(L), std::end(L), std::back_inserter(Sis),
                     [](const auto& Li) { return gbc::gbcToSMapping(Li); });

      return Sis;
    }

    std::vector<PSpaceUnit> computeHis(const Lambdas& L) const
    {

      std::vector<PSpaceUnit> His;
      His.reserve(this->sides());
      std::transform(std::begin(L), std::end(L), std::back_inserter(His),
                     [](const auto& Li) { return gbc::gbcToHMapping(Li); });

      return His;
    }


    Point evaluatePosition(const PSpacePoint& par) const
    {
      const auto& N = this->sides();

      // Find MVC
      const auto l = gbc(par);

      // Lambdas
      const auto L = gbc::generateLeftRotatedLambdaSet(l);

      const auto Sis = computeSis(L);
      const auto His = computeHis(L);

      const auto Bh = m_Bh(l);

      Point p;
      for (auto i = 0UL; i < N; ++i) p += Bh[i] * evalR(i, Sis[i], His[i]);
      return p;
    }

    Point evalR(size_t i, const PSpaceUnit& s, const PSpaceUnit& h) const
    {
      const auto P_eval = evalP(i, s);
      const auto T_eval = evalT(i, s);
      const auto G_eval = evalGamma(h);
      const auto R_eval = blaze::evaluate(P_eval + (G_eval * T_eval));

      return R_eval;
    }

    Vector evalT(size_t i, const PSpaceUnit& t) const
    {
      const auto N  = this->sides();
      const auto im = (N + i - 1) % N;
      return m_T[im];   // (t);
    }

    Point evalP(size_t i, const PSpaceUnit& t) const
    {

      const auto N  = this->sides();
      const auto im = (N + i - 1) % N;
      //      const auto ip  = (N + i + 1) % N;
      const auto Pis = m_P[im];
      const auto Pie = m_P[i];

      return blaze::evaluate(Pis + ((Pie - Pis) * t));
    }

    PSpaceUnit evalGamma(const PSpaceUnit& t) const
    {
      return t / (2 * t + 1);
    }

    static constexpr basis::polygonblendingbasis::
      HungarianSpecialSideBlendingBasis<basis::bfunction::BFBSBFunction>
                                                                      m_Bh{};
    static constexpr basis::bfunction::BFBSBFunction          m_BfB{};
    static constexpr divideddifference::fd::CentralDifference         m_D{};
  };




}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_POLYGONSURFACE_TRANSFINITERIBBONPATCH_H
