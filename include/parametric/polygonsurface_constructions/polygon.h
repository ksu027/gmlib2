#ifndef GM2_PARAMETRIC_POLYGONSURFACE_POLYGON_H
#define GM2_PARAMETRIC_POLYGONSURFACE_POLYGON_H


#include "../polygonsurface.h"

#include "../../core/divideddifference/finitedifference.h"
#include "../../core/coreutils.h"
#include "../../core/gbc/mvc.h"

namespace gmlib2::parametric
{
  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl>
  class Polygon
    : public PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
    using Base = PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Polygon construction types
    using Polygon2D = typename Base::Polygon2D;
    using Vertices  = DVectorT<Point>;

    // Constructor(s)
    template <typename... Ts>
    Polygon(const Vertices& vertices, Ts&&... ts)
      : Base(vertices.size(), std::forward<Ts>(ts)...), m_vertices(vertices)
    {
      assert(vertices.size() == this->polygon2D().size());
    }

    const Vertices& vertices() const { return m_vertices; }
    size_t          sides() const { return m_vertices.size(); }

    // Member(s)
//  private:
    Vertices m_vertices;

    // Polygon interface
  public:
    PSpaceBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        false);
    }

    PSpacePoint startParameters() const override
    {

      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(0.0);
    }

    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(1.0);
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/) const override
    {
      const auto eval_fn
        = [this](const auto& at_par) { return evaluatePosition(at_par); };

      // Results
      EvaluationResult p(no_der[0] + 1, no_der[1] + 1);

      // S
      p(0, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        eval_fn(par), Unit(1));

      // Du S
      if (no_der[0] > 0) {
        const auto par_d0 = PSpaceVector{1e-6, 0};
        const auto Ds0    = D(par, par_d0, eval_fn);

        p(1, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds0, Unit(1));
      }

      // Dv S
      if (no_der[1] > 0) {
        const auto par_d1 = PSpaceVector{0, 1e-6};
        const auto Ds1    = D(par, par_d1, eval_fn);

        p(0, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds1, Unit(1));
      }

      // Duv S
      if (no_der[0] > 0 and no_der[1] > 0)
        p(1, 1) = typename EvaluationResult::ElementType(0);

      return p;
    }

  private:
    Point evaluatePosition(const PSpacePoint& par) const
    {
      const auto& v     = par;
      const auto  l     = gbc::mvc(this->polygon2D(), v);
      const auto& V     = vertices();
      const auto  eval  = blaze::inner(l, V);

      return eval;
    }

    static constexpr divideddifference::fd::CentralDifference D{};
  };

}   // namespace gmlib2::parametric


#endif   // GM2_PARAMETRIC_POLYGONSURFACE_POLYGON_H
