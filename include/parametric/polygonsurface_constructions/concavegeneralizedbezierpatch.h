#ifndef GM2_PARAMETRIC_POLYGONSURFACE_CONCAVEGENERALIZEDBEZIERPATCH_H
#define GM2_PARAMETRIC_POLYGONSURFACE_CONCAVEGENERALIZEDBEZIERPATCH_H

// gmlib2

#include "../polygonsurface.h"

#include "../../core/divideddifference/finitedifference.h"
#include "../../core/basisgenerators/bernsteinbasisgenerators.h"
#include "../../core/gbc/mvc.h"
#include "../../core/gbc/mapping.h"
#include "../../core/gbc/gbcutils.h"

namespace gmlib2::parametric
{

  namespace concavegeneralizedbezierpatch
  {

    namespace detail
    {
      template <typename Unit_T>
      auto
      rcnBoundary(std::vector<DMatrixT<VectorT<Unit_T, 3>>> const& ribbonCNs)
      {
        DVectorT<VectorT<Unit_T, 3>> P3D(ribbonCNs.size());
        for (auto i = 0UL; i < ribbonCNs.size(); ++i)
          P3D[i] = ribbonCNs[i](0UL, 0UL);
        return P3D;
      }
    }   // namespace detail

    template <typename Unit_T>
    auto projectRCNsBoundaryOntoBestFitApproximatedPlane(
      std::vector<DMatrixT<VectorT<Unit_T, 3>>>const& ribbonCNs)
    {
      const auto P3D = detail::rcnBoundary(ribbonCNs);
      return polygonutils::projectPolygonOntoBestFitApproximatedPlane(P3D);
    }

    template <typename Unit_T>
    auto projectRCNsBoundaryOntoBestFitApproximatedPlaneNUV(
      std::vector<DMatrixT<VectorT<Unit_T, 3>>> const& ribbonCNs)
    {
      const auto P3D = detail::rcnBoundary(ribbonCNs);
      return polygonutils::projectPolygonOntoBestFitApproximatedPlaneNUV(P3D);
    }

    template <typename Unit_T>
    auto projectRCNsBoundaryOntoBestFitApproximatedPlaneCenteredIn(
      std::vector<DMatrixT<VectorT<Unit_T, 3>>> const& ribbonCNs,
      const VectorT<Unit_T, 3>&                        p)
    {
      const auto P3D = detail::rcnBoundary(ribbonCNs);
      return polygonutils::projectPolygonOntoBestFitApproximatedPlaneCenteredIn(
        P3D, p);
    }


    template <typename Unit_T>
    auto projectRCNsBoundaryOntoBestFitApproximatedZXPlaneNormalized(
      std::vector<DMatrixT<VectorT<Unit_T, 3>>>const & ribbonCNs)
    {
      const auto P3D = detail::rcnBoundary(ribbonCNs);
      return polygonutils::
        projectPolygonOntoBestFitApproximatedZXPlaneNormalized(P3D);
    }


    // Expects a ribbon of degree d as input
    // Outputs a control net of degree d+1
    template <typename CGBP_T>
    auto degreeElevate(typename CGBP_T::RibbonControlNets const& CNs)
    {

      using ControlNets = typename CGBP_T::RibbonControlNets;
      using ControlNet  = typename CGBP_T::RibbonControlNet;
      using T           = typename CGBP_T::PSpaceUnit;
      using Point       = typename CGBP_T::Point;

      const auto N = CNs.size();

      ControlNets eRCNs(N);

      for (size_t i = 0; i < N; ++i) {




        auto&      RCNi = CNs.at(size_t(i));
        const auto D_z   = RCNi.columns();
        const auto L_z   = RCNi.rows();
        const auto D     = D_z + 1;
        const auto L     = L_z + 1;


        auto& eRCNi = eRCNs.at(size_t(i));
        eRCNi.resize(L, D);

        // retain the corner control point(s)
        eRCNi(0, 0)         = RCNi(0, 0);
        eRCNi(0, D - 1)     = RCNi(0, RCNi.columns() - 1);
        eRCNi(L - 1, 0)     = RCNi(RCNi.rows() - 1, 0);
        eRCNi(L - 1, D - 1) = RCNi(RCNi.rows() - 1, RCNi.columns() - 1);


        // Degree elevate the boundaries
        // columns
        for (auto c_k = 1UL; c_k < D-1; ++c_k) {
          const auto vk = T(c_k) / T(D_z);
          const auto w0 = vk;
          const auto w1 = 1 - vk;

          auto C0_s = RCNi(0, c_k - 1);
          auto C1_s = RCNi(0, c_k);
          eRCNi(0, c_k)     = w0 * C0_s + w1 * C1_s;

          auto C0_e = RCNi(RCNi.rows() - 1, c_k - 1);
          auto C1_e = RCNi(RCNi.rows() - 1, c_k);
          eRCNi(L - 1, c_k) = w0 * C0_e + w1 * C1_e;
        }

        // rows
        for (auto c_j = 1UL; c_j < L-1; ++c_j) {
          const auto vj = T(c_j) / T(L_z * 2-1);
          const auto w0 = vj;
          const auto w1 = 1 - vj;

          auto C0_s    = RCNi(c_j - 1, 0);
          auto C1_s    = RCNi(c_j, 0);
          eRCNi(c_j, 0) = w0 * C0_s + w1 * C1_s;

          auto C0_e        = RCNi(c_j - 1, RCNi.rows() - 1);
          auto C1_e        = RCNi(c_j, RCNi.rows() - 1);
          eRCNi(c_j, D - 1) = w0 * C0_e + w1 * C1_e;
        }



        // degree elevate the interior
        for (auto c_j = 1UL; c_j < L-1; ++c_j) {
          for (auto c_k = 1UL; c_k < D-1; ++c_k) {

            const auto vj = T(c_j) / T(L_z*2-1);
            const auto vk = T(c_k) / T(D_z);

            // clang-format off
            std::array<T, 4> w{      vj  * vk,
                                (1 - vj) * vk,
                                     vj  * (1 - vk),
                                (1 - vj) * (1 - vk) };
            // clang-format on

            auto C0 = RCNi(c_j - 1, c_k - 1);
            auto C1 = RCNi(c_j, c_k - 1);
            auto C2 = RCNi(c_j - 1, c_k);
            auto C3 = RCNi(c_j, c_k);


            // clang-format off
            eRCNi(c_j, c_k) = w[0] * C0 +
                              w[1] * C1 +
                              w[2] * C2 +
                              w[3] * C3;
            // clang-format on
          }
        }
      }


      return eRCNs;
    }

  }   // namespace concavegeneralizedbezierpatch



  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl>
  class ConcaveGeneralizedBezierPatch
    : public PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
    using Base = PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES


    // Polygon construction types
    using RibbonControlNet  = DMatrixT<Point>;
    using RibbonControlNets = std::vector<RibbonControlNet>;

    // Basis-cover Polygon construction types
    using BasisCoverRibbonControlNet  = DMatrixT<Unit>;
    using BasisCoverRibbonControlNets = std::vector<BasisCoverRibbonControlNet>;

    using Polygon2D = typename Base::Polygon2D;

    using Lambda = typename Base::Lambda;


    // Constructor(s)
    template <typename... Ts>
    inline ConcaveGeneralizedBezierPatch(const RibbonControlNets& C, Ts&&... ts)
      : Base(concavegeneralizedbezierpatch::
               projectRCNsBoundaryOntoBestFitApproximatedPlane(C),
             std::forward<Ts>(ts)...),
        m_C(C)
    {
      assert(C.size() >= 3);
      if (C.size() < 3)
        throw std::runtime_error("Wrong dimension: Control Nets");

      DVectorT<VectorT<double, 3>> P(C.size());
      for (auto i = 0UL; i < C.size(); ++i) P[i] = C[i](0UL, 0UL);



      m_CB_no_coefs = 0;
      m_CB.resize(m_C.size());
      for (auto i = 0UL; i < m_CB.size(); ++i, ++m_CB_no_coefs)
        m_CB[i] = BasisCoverRibbonControlNet(m_C[i].rows(), m_C[i].columns(),
                                             Unit(0));
    }

  private:
    size_t di(size_t side_i) const { return m_C[side_i].columns() - 1; }
    size_t li(size_t side_i) const { return m_C[side_i].rows(); }

  public:
    RibbonControlNets m_C;

    Point m_C_central;
    bool  m_use_central_cp{false};
    Unit  m_W_central{1.0};


    mutable BasisCoverRibbonControlNets m_CB;
    mutable size_t                      m_CB_no_coefs;

    mutable std::vector<PSpaceUnit>           m_Sis;
    mutable std::vector<PSpaceUnit>           m_His;
    mutable std::vector<DMatrixT<PSpaceUnit>> m_BssT;
    mutable std::vector<DMatrixT<PSpaceUnit>> m_Bhs;
    mutable std::vector<DMatrixT<PSpaceUnit>> m_Mus;

    PSpaceSizeArray degrees(size_t side_i) const
    {
      return {di(side_i), 2 * li(side_i) - 1};
    }

    PSpaceBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        false);
    }

    PSpacePoint startParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(0.0);
    }

    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(1.0);
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint&     par,
             const PSpaceSizeArray& no_der
             = utils::initStaticContainer<PSpaceSizeArray, PSpaceVectorDim>(1),
             const PSpaceBoolArray& /*from_left*/
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override
    {
      // Evaluation function driver
      const auto eval_fn
        = [this](const auto& at_par) { return evaluatePosition(at_par); };

      // Helpers
      enum class InsidePolygonTestResult { Inside, Outside };

      const auto insidePolygonTestShVariation
        = [this](const PSpaceVector& psp) {
            const auto l = gbc(psp);
            const auto s = gbc::gbcToSMapping(l);
            const auto h = gbc::gbcToHMapping(l);
            if (s < 0 or s > 1 or h < 0 or h > 1) {
              return InsidePolygonTestResult::Outside;
            }
            return InsidePolygonTestResult::Inside;
          };

      const auto compDD = [&](const PSpacePoint& psp, const PSpaceVector& psv) {
        if (insidePolygonTestShVariation(psp - psv)
            == InsidePolygonTestResult::Outside)
          return Df(psp, psv, eval_fn);
        else if (insidePolygonTestShVariation(psp + psv)
                 == InsidePolygonTestResult::Outside)
          return Db(psp, psv, eval_fn);
        else
          return D(psp, psv, eval_fn);
      };


      // Compute step size
      const auto step = [&]() -> double { return 1e-5; }();

      // Results
      EvaluationResult p(no_der[0] + 1, no_der[1] + 1);

      // S
      p(0, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        eval_fn(par), Unit(1));

      // Du S
      if (no_der[0] > 0) {
        const auto par_d0 = PSpaceVector{step, 0};
        p(1, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          compDD(par, par_d0), Unit(1));
      }

      // Dv S
      if (no_der[1] > 0) {

        const auto par_d1 = PSpaceVector{0, step};
        p(0, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          compDD(par, par_d1), Unit(1));
      }

      // Duv S
      if (no_der[0] > 0 and no_der[1] > 0)
        p(1, 1) = typename EvaluationResult::ElementType(0);

      return p;
    }

  public:
    void preComputeBasisCoverCache(const PSpacePoint& par) const
    {
      const auto M = this->sides();

      // Reserve and clear content of cache
      m_Sis.reserve(M);
      m_His.reserve(M);
      m_BssT.reserve(M);
      m_Bhs.reserve(M);
      m_Mus.reserve(M);

      m_Sis.clear();
      m_His.clear();
      m_BssT.clear();
      m_Bhs.clear();
      m_Mus.clear();


      // Find MVC
      const auto x = gbc(par);

      // Lambdas
      const auto L = gbc::generateLeftRotatedLambdaSet(x);



      // Local coords: s/h-parameterization
      std::transform(std::begin(L), std::end(L), std::back_inserter(m_Sis),
                     [](const auto& l) { return gbc::gbcToSMapping(l); });
      std::transform(std::begin(L), std::end(L), std::back_inserter(m_His),
                     [](const auto& l) { return gbc::gbcToHMapping(l); });

      std::rotate(std::begin(m_Sis), std::begin(m_Sis) + 1, std::end(m_Sis));
      std::rotate(std::begin(m_His), std::begin(m_His) + 1, std::end(m_His));


      // Pre-computation - Basis function
      for (auto i = 0UL; i < M; ++i) {
        const auto  d_i = di(i);
        const auto  l_i = li(i);
        const auto& s_i = m_Sis[i];
        const auto& h_i = m_His[i];

        m_BssT.push_back(blaze::evaluate(
          blaze::trans(basis::generateBernsteinBasisMatrix(int(d_i), s_i))));
        m_Bhs.push_back(blaze::evaluate(blaze::submatrix(
          basis::generateBernsteinBasisMatrix(int(2 * l_i - 1), h_i), 0UL, 0UL,
          l_i, l_i)));
      }

      // Pre-compute deficiency weights
      for (auto i = 0UL; i < M; ++i) {
        const auto im = (M + i - 1) % M;
        const auto ip = (M + i + 1) % M;

        m_Mus.push_back(generateMuMatrix(i, m_His[im], m_His[i], m_His[ip]));
      }
    }



  private:
    Lambda gbc(const PSpacePoint& x) const
    {
      return gbc::mvc(this->polygon2D(), x);
    }


    Point evaluatePosition(const PSpacePoint& par) const
    {

      const auto M = this->sides();

      // Pre-compute basis cover cache
      preComputeBasisCoverCache(par);

      // Position
      Point R_sum(0);
      Unit  B_sum(0);

      // Per-side evaluation -- Ri and Bi
      for (size_t i = 0; i < M; ++i) {

        const auto& Bhs_i  = m_Bhs[i];
        const auto& BssT_i = m_BssT[i];
        const auto& Mus_i  = m_Mus[i];
        const auto& C_i    = m_C[i];

        // Edge-ribbon evaluation
        const auto Ri = blaze::evaluate(Bhs_i * ((Mus_i % C_i) * BssT_i));
        R_sum += Ri(0, 0);

        const auto Bi = blaze::evaluate(Bhs_i * (Mus_i * BssT_i));
        B_sum += Bi(0, 0);
      }

      // Center
      if (m_use_central_cp) {

        const auto h_center
          = std::accumulate(std::begin(m_His), std::end(m_His), Unit(1),
                            [](const auto& init, const auto& hi) {
                              return init * std::pow(hi, 2);
                            });

        R_sum += m_C_central * h_center * m_W_central;
        B_sum += h_center;
      }

      return (Unit(1) / B_sum) * R_sum;
    }

    Unit compute_mu(size_t j, size_t d, const PSpaceUnit& him,
                    const PSpaceUnit& hi, const PSpaceUnit& hip) const
    {
      const auto h2im = std::pow(him, 2);
      const auto h2i  = std::pow(hi, 2);
      const auto h2ip = std::pow(hip, 2);
      if (2 * j < d) {
        if (std::abs(h2im + h2i) < 1e-7)
          return Unit(0.5);
        else
          return h2im / (h2im + h2i);
      }
      else if (2 * j == d)
        return Unit(1);
      else {   // 2 * j > d
        if (std::abs(h2ip + h2i) < 1e-7)
          return Unit(0.5);
        else
          return h2ip / (h2ip + h2i);
      }
    }

    DMatrixT<Unit> generateMuMatrix(size_t i, const PSpaceUnit& him,
                                    const PSpaceUnit& hi,
                                    const PSpaceUnit& hip) const
    {
      DMatrixT<Unit> Mu(li(i), di(i) + 1);
      for (size_t k = 0; k < Mu.rows(); ++k)
        for (size_t j = 0; j < Mu.columns(); ++j)
          Mu(k, j) = compute_mu(j, di(i), him, hi, hip);
      return Mu;
    }

    static constexpr divideddifference::fd::CentralDifference  D{};
    static constexpr divideddifference::fd::ForwardDifference  Df{};
    static constexpr divideddifference::fd::BackwardDifference Db{};

  };   // class ConcaveGeneralizedBezierPatch

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_POLYGONSURFACE_CONCAVEGENERALIZEDBEZIERPATCH_H
