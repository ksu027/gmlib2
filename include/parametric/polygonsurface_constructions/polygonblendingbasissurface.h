#ifndef GM2_PARAMETRIC_POLYGONSURFACE_BLENDINGBASISSURFACE_H
#define GM2_PARAMETRIC_POLYGONSURFACE_BLENDINGBASISSURFACE_H

// gmlib2

#include "../polygonsurface.h"
#include "../../core/basis/polygonblendingbasis.h"
#include "../../core/divideddifference/finitedifference.h"

namespace gmlib2::parametric
{

  template <typename PolygonBlendBasis_T,
            typename SpaceObjectEmbedBase_T = gmlib2::ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl>
  class PolygonBlendingBasisSurface
    : public PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
    using Base = PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using Polygon2D = typename Base::Polygon2D;
    using Lambda    = typename Base::Lambda;
    using Lambdas   = typename Base::Lambdas;
    using BVector   = DVectorT<PSpaceUnit>;


    // Constructor(s)
    template <typename... Ts>
    PolygonBlendingBasisSurface(size_t N, Ts&&... ts)
      : Base(N, std::forward<Ts>(ts)...)
    {
    }

    template <typename... Ts>
    PolygonBlendingBasisSurface(size_t N, size_t basis_nr, Ts&&... ts)
      : Base(N, std::forward<Ts>(ts)...), m_basis_nr{basis_nr}
    {
    }

    // Members
    size_t m_basis_nr{0ul};
    bool   m_sum_surface{false};

  public:
    PSpaceBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        false);
    }
    PSpacePoint startParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(
        PSpaceUnit(0));
    }
    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(
        PSpaceUnit(1));
    }




  protected:
    EvaluationResult evaluate(
      const PSpacePoint&     par,
      const PSpaceSizeArray& no_der
      = utils::initStaticContainer<PSpaceSizeArray, PSpaceVectorDim>(1UL),
      const PSpaceBoolArray& /*from_left*/
      = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        true)) const override
    {
      const auto eval_fn
        = [this](const auto& at_par) { return evaluatePosition(at_par); };



      // Evaluation result (point and normal)
      EvaluationResult p(no_der[0] + 1, no_der[1] + 1);
      const auto       dd_epsilon = 1e-4;

      // S
      p(0, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        eval_fn(par), Unit(1));

      // Du S
      if (no_der[0] > 0) {
        const auto par_d0 = PSpaceVector{dd_epsilon, 0};
        const auto Ds0    = m_D(par, par_d0, eval_fn);

        p(1, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds0, Unit(1));
      }

      // Dv S
      if (no_der[1] > 0) {
        const auto par_d1 = PSpaceVector{0, dd_epsilon};
        const auto Ds1    = m_D(par, par_d1, eval_fn);

        p(0, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds1, Unit(1));
      }

      // Duv S
      if (no_der[0] > 0 and no_der[1] > 0)
        p(1, 1) = typename EvaluationResult::ElementType(0);

      return p;
    }

    Point evaluatePosition(const PSpacePoint& par) const
    {

      // Find MVC
      const auto l = gbc::mvc(this->polygon2D(), par);

      auto S    = blaze::evaluate(blaze::inner(l, this->polygon2D()));

      // Evaluate Basis at gbc
      const auto B     = m_B(l);
      const auto B_sum = std::accumulate(std::begin(B), std::end(B), Unit(0));

      return Point{S[0], m_sum_surface ? B_sum : B[m_basis_nr], S[1]};
    }



  private:
    static constexpr PolygonBlendBasis_T m_B{};
    //    static constexpr polygonblendbasis::SideBlendBasis<
    //      basis::bfunction::FabiusBFunction>
    //      m_SBB_FabiusB{};
    //    static constexpr polygonblendbasis::HungarianCornerBlendBasis<
    //      basis::bfunction::FabiusBFunction>
    //      m_HCBB_FabiusB{};
    //    static constexpr polygonblendbasis::HungarianSideBlendBasis<
    //      basis::bfunction::FabiusBFunction>
    //      m_HSBB_FabiusB{};
    //    static constexpr polygonblendbasis::HungarianSpecialSideBlendBasis<
    //      basis::bfunction::FabiusBFunction>
    //      m_HSSBB_FabiusB{};

    //    static constexpr polygonblendbasis::HungarianSpecialSideBlendBasis<
    //      basis::bfunction::BFBSBFunction>
    //      m_HSSBB_BFB{};

    static constexpr divideddifference::fd::CentralDifference m_D{};
  };


  namespace polygonblendbasissurface {

    template <typename BFunction_T,
              typename SpaceObjectEmbedBase_T = gmlib2::ProjectiveSpaceObject<>,
              template <typename> typename PObjEvalCtrl_T
              = evaluationctrl::PolygonSurfaceEvalCtrl>
    using CornerBlendingSurface = PolygonBlendingBasisSurface<
      basis::polygonblendingbasis::CornerBlendingBasis<BFunction_T>,
      SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    template <typename BFunction_T            = basis::bfunction::FabiusBFunction,
              typename SpaceObjectEmbedBase_T = gmlib2::ProjectiveSpaceObject<>,
              template <typename> typename PObjEvalCtrl_T
              = evaluationctrl::PolygonSurfaceEvalCtrl>
    using HungarianCornerBlendingBasis = PolygonBlendingBasisSurface<
      basis::polygonblendingbasis::HungarianCornerBlendingBasis<BFunction_T>,
      SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    template <typename BFunction_T,
              typename SpaceObjectEmbedBase_T = gmlib2::ProjectiveSpaceObject<>,
              template <typename> typename PObjEvalCtrl_T
              = evaluationctrl::PolygonSurfaceEvalCtrl>
    using HungarianSideBlendingBasis = PolygonBlendingBasisSurface<
      basis::polygonblendingbasis::HungarianSideBlendingBasis<BFunction_T>,
      SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    template <typename BFunction_T,
              typename SpaceObjectEmbedBase_T = gmlib2::ProjectiveSpaceObject<>,
              template <typename> typename PObjEvalCtrl_T
              = evaluationctrl::PolygonSurfaceEvalCtrl>
    using HungarianSpecialSideBlendingBasis = PolygonBlendingBasisSurface<
      basis::polygonblendingbasis::HungarianSpecialSideBlendingBasis<
        BFunction_T>,
      SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;



  }   // namespace polygonblendbasissurface

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_POLYGONSURFACE_BLENDINGBASISSURFACE_H
