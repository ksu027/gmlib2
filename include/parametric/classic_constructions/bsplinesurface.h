#ifndef GM2_PARAMETRIC_SURFACE_BSPLINESURFACE_H
#define GM2_PARAMETRIC_SURFACE_BSPLINESURFACE_H

#include "../surface.h"


#include "../utils/bsplineutils.h"

#include "../../core/basisgenerators/bernsteinbasisgenerators.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  class BSplineSurface
    : public Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base = Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES


    using KnotVector = bspline::BSplineKnotVector<PSpaceUnit>;
    using ControlNet = DMatrixT<Point>;
    using Degrees    = PSpaceSizeArray;
    using Orders     = PSpaceSizeArray;


    // Constructor(s)
    template <typename... Ts>
    BSplineSurface(const ControlNet& C, const Degrees& d, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C(C)
    {
      setDegrees(d);
    }


    // Members
  public:
    ControlNet m_C;

  private:
    Degrees m_degrees{
      utils::initStaticContainer<PSpaceSizeArray, PSpaceVectorDim>(0UL)};
    KnotVector m_u;
    KnotVector m_v;



    // Member functions
  public:
    Orders orders() const { return {m_degrees[0] + 1, m_degrees[1] + 1}; }
    const Degrees& degrees() const { return m_degrees; }
    bool           setDegrees(const Degrees& d)
    {

      if ((d[0] > m_C.rows() - 1) or (d[1] > m_C.columns() - 1)) return false;

      m_degrees = d;

      initUniformKnotVectors();

      return true;
    }
    bool isValid() const { return m_degrees[0] > 0 and m_degrees[1] > 0; }

    const KnotVector& knotVectorU() const
    {
      return m_u;
    }

    const KnotVector& knotVectorV() const
    {
      return m_v;
    }

  private:

    void initUniformKnotVectors()
    {

      const auto& [d_u, d_v] = degrees();
      m_u = bspline::generateUniformKnotVector<PSpaceUnit>(m_C.rows(), d_u);
      m_v = bspline::generateUniformKnotVector<PSpaceUnit>(m_C.columns(), d_v);
    }




    // TPSurf interface
  public:
    PSpaceBoolArray isClosed() const final { return {{false, false}}; }
    PSpacePoint     startParameters() const final
    {
      return PSpacePoint{m_u[0UL], m_v[0UL]};
    }
    PSpacePoint endParameters() const final
    {
      return PSpacePoint{m_u[m_C.rows()], m_v[m_C.columns()]};
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const final
    {

      const auto u          = par[0];
      const auto v          = par[1];
      const auto nd_u       = std::min(no_der[0], m_C.rows() - 1);
      const auto nd_v       = std::min(no_der[1], m_C.columns() - 1);
      const auto& [d_u,d_v] = degrees();
      const auto u_i        = bspline::findKnotIndex(m_u, u, d_u);
      const auto v_i        = bspline::findKnotIndex(m_v, v, d_v);




      const auto Bu = basis::generateBSplineBasisMatrix(m_u, u_i, u, d_u);
      const auto Bv = basis::generateBSplineBasisMatrix(m_v, v_i, v, d_v);
      const auto C  = blaze::submatrix(m_C, u_i - d_u, v_i - d_v,
                                      d_u + 1, d_v + 1);
      const auto res = Bu * (C * blaze::trans(Bv));


//      std::cout << "--------- BSplineSurf Eval ----------\n" << std::endl;
//      std::cout << "\n\n";
//      std::cout << "u:    " << u << '\n';
//      std::cout << "v:    " << v << '\n';
//      std::cout << "nd_u: " << nd_u << '\n';
//      std::cout << "nd_v: " << nd_v << '\n';
//      std::cout << "d_u:  " << d_u << '\n';
//      std::cout << "d_v:  " << d_v << '\n';
//      std::cout << "o_u:  " << orders()[0] << '\n';
//      std::cout << "o_v:  " << orders()[1] << '\n';
//      std::cout << "u_i:  " << u_i << '\n';
//      std::cout << "v_i:  " << v_i << '\n';
//      std::cout << "---------\n";
//      std::cout << "U:\n" << m_u << '\n';
//      std::cout << "V:\n" << m_v << '\n';
//      std::cout << "---------\n";
//      std::cout << "Bu:\n" << Bu << '\n';
//      std::cout << "Bv:\n" << Bv << '\n';
//      std::cout << "---------\n";
//      std::cout << "C[...]:\n" << C << '\n';
//      std::cout << "---------\n";
//      std::cout << "res (Bu * (C * Bv^T):\n" << res << '\n';
//      std::cout << "--------- BSplineSurf Eval [END] ----\n" << std::endl;




      EvaluationResult p(nd_u + 1, nd_v + 1);
      p(0, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        res(0, 0), 1.0);

      if (nd_u)
        p(1, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(1, 0), 0.0);

      if (nd_v)
        p(0, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(0, 1), 0.0);

      if (nd_u and nd_v)
        p(1, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(1, 1), 0.0);

      using ReturnType = const EvaluationResult;
      return ReturnType(p);
    }
  };





}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_SURFACE_BSPLINESURFACE_H
