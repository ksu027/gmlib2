#ifndef GM2_PARAMETRIC_SURFACE_BEZIERSURFACE_H
#define GM2_PARAMETRIC_SURFACE_BEZIERSURFACE_H

#include "../surface.h"

#include "../../core/coreutils.h"
#include "../../core/basisgenerators/bernsteinbasisgenerators.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename SurfaceEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  class BezierSurface
    : public Surface<SpaceObjectEmbedBase_T, SurfaceEvalCtrl_T> {

  public:
    using Base = Surface<SpaceObjectEmbedBase_T, SurfaceEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES



    using ControlNet = DMatrixT<Point>;

    // Constructor(s)

    BezierSurface(const ControlNet& C = ControlNet(2,2))
    {
        std::vector<Point> v_points;
        v_points.push_back(Point{ -10.0, 0.0, -10.0});
        v_points.push_back(Point{   0.0, 8.0, -10.0});
        v_points.push_back(Point{  10.0, 0.0, -10.0});

        v_points.push_back(Point{ -10.0, 5.0,   0.0});
        v_points.push_back(Point{   0.0, 2.0,   0.0});
        v_points.push_back(Point{  10.0, 5.0,   0.0});

        v_points.push_back(Point{ -10.0, 8.0,   10.0});
        v_points.push_back(Point{   0.0, 0.0,   10.0});
        v_points.push_back(Point{  10.0, 8.0,   10.0});


        ControlNet points(3,3);
        points(0,0) = v_points[0];
        points(0,1) = v_points[1];
        points(0,2) = v_points[2];

        points(1,0) = v_points[3];
        points(1,1) = v_points[4];
        points(1,2) = v_points[5];

        points(2,0) = v_points[6];
        points(2,1) = v_points[7];
        points(2,2) = v_points[8];

        m_C     = points;
        m_scale = PSpaceVector{1.0,1.0};
    }

    template <typename... Ts>
    explicit BezierSurface(const ControlNet& C, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_C(C), m_scale{PSpaceUnit(1), PSpaceUnit(1)}
    {
      //      std::cout << "m_C: " << m_C << std::endl;
    }


    // Members
    ControlNet   m_C;
    PSpaceVector m_scale;

    // TPSurf interface
  public:
    PSpaceBoolArray isClosed() const final { return {{false, false}}; }
    PSpacePoint     startParameters() const final { return PSpacePoint{0, 0}; }
    PSpacePoint     endParameters() const final { return PSpacePoint{1, 1}; }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const final
    {

      const auto u    = par[0];
      const auto v    = par[1];
      const auto nd_u = std::min(no_der[0], m_C.rows() - 1);
      const auto nd_v = std::min(no_der[1], m_C.columns() - 1);

      EvaluationResult p(nd_u + 1, nd_v + 1);


      const auto Bu = basis::generateBernsteinBasisMatrix(int(m_C.rows() - 1),
                                                          u, m_scale[0]);
      const auto Bv = basis::generateBernsteinBasisMatrix(
        int(m_C.columns() - 1), v, m_scale[1]);


      const auto res = Bu * (m_C * blaze::trans(Bv));

      p(0, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        res(0, 0), 1.0);

      if (nd_u)
        p(1, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(1, 0), 0.0);

      if (nd_v)
        p(0, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(0, 1), 0.0);

      if (nd_u and nd_v)
        p(1, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res(1, 1), 0.0);

      using ReturnType = const EvaluationResult;
      return ReturnType(p);
    }
  };





}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_SURFACE_BEZIERSURFACE_H
