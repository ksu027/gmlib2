#ifndef GM2_PARAMETRIC_CURVE_NURBSCURVE_H
#define GM2_PARAMETRIC_CURVE_NURBSCURVE_H

#include "../curve.h"

#include "../../core/coreutils.h"
#include "../../core/basisgenerators/bernsteinbasisgenerators.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  class NURBSCurve : public   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base =   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES




    using KnotVector     = DVectorT<Unit>;
    using ControlPoints  = DColVectorT<Point>;
    using ControlPointsH = DColVectorT<VectorH>;
    using Weights        = DColVectorT<Unit>;

    // Constructor
    template <typename... Ts>
    NURBSCurve(const ControlPoints& C, size_t d, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C(C), m_W(C.size(), Unit(1))
    {
      // set degree and generate knot vector
      setDegree(d);
    }

    template <typename... Ts>
    NURBSCurve(const ControlPoints& C, const Weights& W, size_t d, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C(C), m_W(W)
    {
      // set degree and generate knot vector
      setDegree(d);
    }

    template <typename... Ts>
    NURBSCurve(const ControlPointsH& C, size_t d, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...) /*, m_C{C}*/
    {
      // init m_C and m_W
      m_C = blaze::map(C, [](const auto& c_i) {
        return Point(blaze::subvector<0UL, VectorDim>(c_i));
      });
      m_W = blaze::map(C, [](const auto& c_i) { return Unit(c_i[VectorDim]); });

      setDegree(d);
    }

    // Members
  public:
    ControlPoints m_C;
    Weights       m_W;

  private:
    size_t     m_degree{0};
    KnotVector m_t;



    // Member function
  public:
    size_t order() const { return m_degree + 1; }
    size_t degree() const { return m_degree; }
    bool   setDegree(size_t d)
    {
      if (d > m_C.size() - 1) return false;

      m_degree = d;

      initUniformKnotVector();

      return true;
    }
    bool isValid() const { return m_degree > 0; }

    const KnotVector& knotVector() const { return m_t; }

  private:
    void initUniformKnotVector()
    {

      m_t.resize(m_C.size() + order());
      int step_knots = int(m_t.size()) - int((order() * 2));

      // implicit paramterization [0,max(knot_value)]
      Unit   knot_value = Unit(0);
      size_t i          = 0;

      // Set the start knots
      for (; i < order(); i++) m_t[i] = knot_value;

      // Set the "step"-knots
      for (int j = 0; j < step_knots; j++) m_t[i++] = ++knot_value;

      // Set the end knots
      knot_value++;
      for (; i < m_t.size(); i++) m_t[i] = knot_value;

      // implicit normalize paramterization [0,1]
      m_t *= 1 / Unit(knot_value);
    }





    //   Curve interface
  public:
    PSpaceBoolArray isClosed() const final { return {{false}}; }
    PSpacePoint startParameters() const final { return PSpacePoint{m_t[0UL]}; }
    PSpacePoint endParameters() const final
    {
      return PSpacePoint{m_t[m_C.size()]};
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const final
    {
      const auto& t              = par[0];
      const auto& no_derivatives = no_der[0];

      // degree
      const auto& d = degree();

      // Knot index
      auto t_i = d;
      while (t > m_t[t_i + 1]) t_i++;

      EvaluationResult p(no_derivatives + 1);

      const auto B = basis::generateBSplineBasisMatrix(m_t, t_i, t, d);

      const auto C = blaze::subvector(m_C, t_i - d, d + 1);
      const auto W = blaze::subvector(m_W, t_i - d, d + 1);


      const auto B_0 = blaze::row(B, 0);
      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        (B_0 * (C * W)) / (B_0 * W), Unit(1));

      if (no_derivatives) {
        const auto B_1   = blaze::row(B, 1);
        const auto cmp_C = B_1 * (C * W);
        const auto cmp_W = B_1 * W;

        if (cmp_W < 1e-5) {
          p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
            cmp_C, Unit(0));
        }
        else {
          p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
            cmp_C / cmp_W, Unit(0));
        }
      }

      return p;
    }
  };



}   // namespace gmlib2::parametric




#endif   // GM2_PARAMETRIC_CURVE_NURBSCURVE_H
