#ifndef GM2_PARAMETRIC_SURFACE_NURBSSURFACE_H
#define GM2_PARAMETRIC_SURFACE_NURBSSURFACE_H

#include "../surface.h"

#include "../../core/basisgenerators/bernsteinbasisgenerators.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  class NURBSSurface
    : public Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base = Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES



    using KnotVector  = DVectorT<Unit>;
    using ControlNet  = DMatrixT<Point>;
    using ControlNetH = DMatrixT<VectorH>;
    using Weights     = DMatrixT<Unit>;

    // Constructor(s)
    template <typename... Ts>
    NURBSSurface(const ControlNet& C, const PSpaceSizeArray& d, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C(C),
        m_W(C.rows(), C.columns(), Unit(1))
    {
      setDegree(d);
    }

    template <typename... Ts>
    NURBSSurface(const ControlNet& C, const Weights& W,
                  const PSpaceSizeArray& d, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C(C), m_W(W)
    {
      setDegree(d);
    }

    template <typename... Ts>
    NURBSSurface(const ControlNetH& C, const PSpaceSizeArray& d, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...)
    {
      // init m_C and m_W
      m_C = blaze::map(C, [](const auto& c_i) {
        return Point(blaze::subvector<0UL, VectorDim>(c_i));
      });
      m_W = blaze::map(C, [](const auto& c_i) { return Unit(c_i[VectorDim]); });

      setDegree(d);
    }



    // Members
  public:
    ControlNet m_C;
    Weights    m_W;


  private:
    PSpaceSizeArray m_degree{
      utils::initStaticContainer<PSpaceSizeArray, PSpaceVectorDim>(0UL)};
    KnotVector m_u;
    KnotVector m_v;



    // Member functions
  public:
    PSpaceSizeArray order() const
    {
      PSpaceSizeArray o;
      std::transform(std::begin(m_degree), std::end(m_degree), std::begin(o),
                     [](const auto& ele) { return ele + 1; });
      return o;
    }
    const PSpaceSizeArray& degree() const { return m_degree; }
    bool                   setDegree(const PSpaceSizeArray& d)
    {

      if ((d[0] > m_C.rows() - 1) or (d[1] > m_C.columns() - 1)) return false;

      m_degree = d;

      initUniformKnotVectors();

      return true;
    }
    bool isValid() const { return m_degree[0] > 0 and m_degree[1] > 0; }

    std::pair<const KnotVector&, const KnotVector&> knotVectors() const
    {
      return std::pair(&m_u, &m_u);
    }

  private:
    KnotVector generateUniformKnotVector(size_t no_cps, size_t order)
    {

      KnotVector kv;
      kv.resize(no_cps + order);
      int step_knots = int(kv.size()) - int((order * 2));

      // implicit paramterization [0,max(knot_value)]
      Unit   knot_value = Unit(0);
      size_t i          = 0;

      // Set the start knots
      for (; i < order; i++) kv[i] = knot_value;

      // Set the "step"-knots
      for (int j = 0; j < step_knots; j++) kv[i++] = ++knot_value;

      // Set the end knots
      knot_value++;
      for (; i < kv.size(); i++) kv[i] = knot_value;

      // implicit normalize paramterization [0,1]
      kv *= 1 / Unit(knot_value);

      return kv;
    }

    void initUniformKnotVectors()
    {

      const auto o = order();
      m_u          = generateUniformKnotVector(m_C.rows(), o[0]);
      m_v          = generateUniformKnotVector(m_C.columns(), o[1]);
    }




    // TPSurf interface
  public:
    PSpaceBoolArray isClosed() const final { return {{false, false}}; }
    PSpacePoint     startParameters() const final
    {
      return PSpacePoint{m_u[0UL], m_v[0UL]};
    }
    PSpacePoint endParameters() const final
    {
      return PSpacePoint{m_u[m_C.rows()], m_v[m_C.columns()]};
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const final
    {

      const auto u    = par[0];
      const auto v    = par[1];
      const auto nd_u = std::min(no_der[0], m_C.rows() - 1);
      const auto nd_v = std::min(no_der[1], m_C.columns() - 1);


      // degree
      const auto& d = degree();

      // Knot index
      PSpaceSizeArray t_i = d;
      while (u > m_u[t_i[0] + 1]) t_i[0]++;
      while (v > m_v[t_i[1] + 1]) t_i[1]++;



      EvaluationResult p(nd_u + 1, nd_v + 1);


      const auto Bu = basis::generateBSplineBasisMatrix(m_u, t_i[0], u, d[0]);
      const auto Bv = basis::generateBSplineBasisMatrix(m_v, t_i[1], v, d[1]);

      const auto C = blaze::submatrix(m_C, t_i[0] - d[0], t_i[1] - d[1],
                                      d[0] + 1, d[1] + 1);
      const auto W = blaze::submatrix(m_W, t_i[0] - d[0], t_i[1] - d[1],
                                      d[0] + 1, d[1] + 1);

      const auto res_C = (Bu * ((C % W) * blaze::trans(Bv)));
      const auto res_W = Bu * (W * blaze::trans(Bv));


      p(0, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        res_C(0, 0) / res_W(0, 0), Unit(1));

      if (nd_u)
        p(1, 0) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res_C(1, 0), Unit(0));

      if (nd_v)
        p(0, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res_C(0, 1), Unit(0));

      if (nd_u and nd_v)
        p(1, 1) = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res_C(1, 1), Unit(0));

      using ReturnType = const EvaluationResult;
      return ReturnType(p);
    }
  };






}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_SURFACE_NURBSSURFACE_H
