#ifndef GM2_PARAMETRIC_CURVE_BEZIERCURVE_H
#define GM2_PARAMETRIC_CURVE_BEZIERCURVE_H

#include "../curve.h"

#include "../../core/coreutils.h"
#include "../../core/basisgenerators/bernsteinbasisgenerators.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename CurveEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  class BezierCurve : public   Curve<SpaceObjectEmbedBase_T, CurveEvalCtrl_T> {

  public:
    using Base =   Curve<SpaceObjectEmbedBase_T, CurveEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES


    using ControlPoints = DColVectorT<Point>;

    // Constructor
    template <typename... Ts>
    BezierCurve(const ControlPoints& C, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C(C), m_scale{PSpaceUnit(1)}
    {
    }

    // Members
    ControlPoints m_C;
    PSpaceVector  m_scale;


    //   Curve interface
  public:
    PSpaceBoolArray isClosed() const override { return {{false}}; }
    PSpacePoint     startParameters() const override { return PSpacePoint{0}; }
    PSpacePoint     endParameters() const override { return PSpacePoint{1}; }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override
    {
      const auto& t              = par[0];
      const auto& no_derivatives = no_der[0];

      EvaluationResult p(no_derivatives + 1);

      const auto B = basis::generateBernsteinBasisMatrix(int(m_C.size() - 1), t,
                                                         m_scale[0]);

      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        blaze::row(B, 0) * m_C, 1.0);
      if (no_derivatives)
        p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          blaze::row(B, 1) * m_C, 0.0);
      if (no_derivatives > 1)
        p[2] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          blaze::row(B, 2) * m_C, 0.0);
      if (no_derivatives > 2)
        p[3] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          blaze::row(B, 3) * m_C, 0.0);

      return p;
    }
  };



}   // namespace gmlib2::parametric


#endif   // GM2_PARAMETRIC_CURVE_BEZIERCURVE_H
