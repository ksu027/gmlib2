#ifndef GM_PARAMETRIC_CURVE_HERMITECURVE_H
#define GM_PARAMETRIC_CURVE_HERMITECURVE_H


#include "../helper_constructions/matrixbasisvectorcoefscurve.h"
#include "../../core/basisgenerators/hermitebasisgenerators.h"


namespace gmlib2::parametric
{


  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  using HermiteCurveP2V2
    = MatrixBasisVectorCoefsCurve<basis::HermiteBasisVectorF2D2Kernel<>,
                                   SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  using HermiteCurveP3V2
    = MatrixBasisVectorCoefsCurve<basis::HermiteBasisVectorF3D2Kernel<>,
                                   SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  using HermiteCurveP3V3
    = MatrixBasisVectorCoefsCurve<basis::HermiteBasisVectorF3D3Kernel<>,
                                   SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;



}   // namespace gmlib2::parametric



#endif   // GM_PARAMETRIC_CURVE_HERMITECURVE_H
