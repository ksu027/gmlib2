#ifndef GM2_PARAMETRIC_CURVE_BSPLINECURVE_H
#define GM2_PARAMETRIC_CURVE_BSPLINECURVE_H

#include "../curve.h"

#include "../utils/bsplineutils.h"

#include "../../core/coreutils.h"
#include "../../core/basisgenerators/bernsteinbasisgenerators.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  class BSplineCurve : public   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base =   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES


    using KnotVector    = bspline::BSplineKnotVector<Unit>;
    using ControlPoints = DColVectorT<Point>;
    using Degree        = size_t;
    using Order         = size_t;


    // Constructor
    template <typename... Ts>
    BSplineCurve(const ControlPoints& C, Degree d, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C(C)
    {
      setDegree(d);
    }

    // Members
  public:
    ControlPoints m_C;

  private:
    Degree     m_degree{0};
    KnotVector m_t;



    // Member function
  public:
    Order order() const { return m_degree + 1; }
    Degree degree() const { return m_degree; }
    bool   setDegree(Degree d)
    {
      if (d > m_C.size() - 1) return false;

      m_degree = d;

      initUniformKnotVector();

      return true;
    }
    bool isValid() const { return m_degree > 0; }

    const KnotVector& knotVector() const { return m_t; }

  private:
    void initUniformKnotVector()
    {
      m_t
        = bspline::generateUniformKnotVector<PSpaceUnit>(m_C.size(), m_degree);
    }



    //   Curve interface
  public:
    PSpaceBoolArray isClosed() const final { return {{false}}; }
    PSpacePoint startParameters() const final { return PSpacePoint{m_t[0UL]}; }
    PSpacePoint endParameters() const final
    {
      return PSpacePoint{m_t[m_C.size()]};
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const final
    {
      const auto& t              = par[0];
      const auto& no_derivatives = no_der[0];
      const auto& d              = degree();
      const auto t_i             = bspline::findKnotIndex(m_t, t, d);


      const auto B = basis::generateBSplineBasisMatrix(m_t, t_i, t, d);
      const auto C = blaze::subvector(m_C, t_i - d, d + 1);
      const auto res = B * C;

      EvaluationResult p(no_derivatives + 1);
      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        res[0], 1.0);
      if (no_derivatives)
        p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res[1], 0.0);
      if (no_derivatives > 1)
        p[2] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res[2], 0.0);
      if (no_derivatives > 2)
        p[3] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          res[3], 0.0);

//      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
//        blaze::row(B, 0) * C, 1.0);
//      if (no_derivatives)
//        p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
//          blaze::row(B, 1) * C, 0.0);
//      if (no_derivatives > 1)
//        p[2] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
//          blaze::row(B, 2) * C, 0.0);
//      if (no_derivatives > 2)
//        p[3] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
//          blaze::row(B, 3) * C, 0.0);

      return p;
    }
  };



}   // namespace gmlib2::parametric




#endif   // GM2_PARAMETRIC_CURVE_BSPLINECURVE_H
