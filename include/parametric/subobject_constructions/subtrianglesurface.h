#ifndef GM2_PARAMETRIC_SUBOBJECT_SUBTRIANGLESURFACE_H
#define GM2_PARAMETRIC_SUBOBJECT_SUBTRIANGLESURFACE_H

#include "../surface.h"
#include "../trianglesurface.h"

#include "../parametricsubobject.h"
#include "../utils/approximation.h"


namespace gmlib2::parametric
{

  namespace evaluationctrl
  {

    ////////////////////
    /// evaluate(...)
    /// SubSurface in Surface
    ///
    template <typename SubParametricObject_T>
    struct SubTriangleSurfaceInSurfaceEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpaceTriangleSurface = PSpaceObject;
      using ParametricSurface     = PObject;


      static EvaluationResult evaluate(
        const PSpaceTriangleSurface& pspace_trianglesurface,
        const ParametricSurface*     psurface,
        const typename PSpaceTriangleSurface::PSpacePoint&     par,
        const typename PSpaceTriangleSurface::PSpaceSizeArray& no_der,
        const typename PSpaceTriangleSurface::PSpaceBoolArray& /*from_left*/)
      {
        // Evaluate pspace curve -- result in homogeneous coords ^^,
        const auto pspace_eval = pspace_trianglesurface.evaluateParent(
          par, {{0, 0}}, {{true, true}});

        // Make pspace curve result not homeogeneous coords
        const auto psurface_par
          = blaze::subvector<0UL, ParametricSurface::PSpaceVectorDim>(
            pspace_eval[0UL]);

        // Evaluate surface along pspace curve
        const auto psurface_eval
          = psurface->evaluateLocal(psurface_par, {{0, 0}}, {{true, true}});


        // Results
        EvaluationResult p(4UL);

        // S(S0)
        p[0] = PObjEvalCtrl::toPositionH(psurface_eval);

//        // D_DuS0 S(S0)
//        if (no_der[0] > 0) {
//          const auto pspace_d0 = typename PSpaceSurface::PSpaceVector{1e-6, 0};
//          const auto [Ds0_psurface, Ds0_psurface_scale]
//            = D(pspace_surface, par, pspace_d0, *psurface);

//          p(1, 0) = Ds0_psurface * Ds0_psurface_scale;
//        }

//        // D_DvS0 S(S0)
//        if (no_der[1] > 0) {
//          const auto pspace_d1 = typename PSpaceSurface::PSpaceVector{0, 1e-6};
//          const auto [Ds1_psurface, Ds1_psurface_scale]
//            = D(pspace_surface, par, pspace_d1, *psurface);

//          p(0, 1) = Ds1_psurface * Ds1_psurface_scale;
//        }

//        // D_DuvS0 S(S0)
//        if (no_der[0] > 0 and no_der[1] > 0)
//          p(1, 1) = typename EvaluationResult::ElementType(0);

        return p;
      }

    private:
      // Differntial operator
//      static constexpr gmlib2::parametric::approximation::
//        SubObjectDirectionalDerivativeLocal<>
//              D{};
    };

  }   // namespace evaluationctrl




  ////////////////////////////////////
  /// Parametric SubSurface in Surface

  template <
    template <typename, template <typename> typename> typename PSpaceSurface_T,
    typename PObject_T,
    typename BaseSpaceObjectBase_T = typename PObject_T::BaseSpaceObjectBase,
    template <typename> typename PSubObjEvalCtrl_T
    = evaluationctrl::SubTriangleSurfaceInSurfaceEvalCtrl,
    template <typename> typename PSpaceObject_PObjEvalCtrl_T
    = evaluationctrl::TriangleSurfaceEvalCtrl,
    template <typename> typename PSubObj_PObjEvalCtrl_T
    = evaluationctrl::TriangleSurfaceEvalCtrl>
  using SubTriangleSurfaceInSurface
    = ParametricSubObject<PSpaceSurface_T, PObject_T, PSubObjEvalCtrl_T,
                          PSpaceObject_PObjEvalCtrl_T, PSubObj_PObjEvalCtrl_T,
                          BaseSpaceObjectBase_T>;


}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_SUBOBJECT_SUBTRIANGLESURFACE_H
