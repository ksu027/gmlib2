#ifndef GM2_PARAMETRIC_SUBOBJECT_SUBPOLYGONSURFACE_H
#define GM2_PARAMETRIC_SUBOBJECT_SUBPOLYGONSURFACE_H

#include "../surface.h"
#include "../trianglesurface.h"
#include "../polygonsurface.h"

#include "../parametricsubobject.h"
#include "../utils/approximation.h"


namespace gmlib2::parametric
{

  namespace evaluationctrl
  {

    template <typename SubParametricObject_T>
    struct SubPointInPolygonSurfaceEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpacePoint              = PSpaceObject;
      using ParametricPolygonSurface = PObject;

      static EvaluationResult
      evaluate(const PSpacePoint&                       pspace_point,
               const ParametricPolygonSurface*          ppolysurf,
               const typename PSpacePoint::PSpacePoint& par,
               const typename PSpacePoint::PSpaceSizeArray& /*no_der*/,
               const typename PSpacePoint::PSpaceBoolArray& /*from_left*/)
      {

        // Compute position
        const auto pspace_eval = pspace_point.evaluateParent(par, {0});
        const auto ppolysurf_par
          = blaze::subvector<0UL, ParametricPolygonSurface::PSpaceVectorDim>(
            pspace_eval);

        const auto polysurf_eval
          = ppolysurf->evaluateLocal(ppolysurf_par, {0, 0});

        // Results
        EvaluationResult p = PObjEvalCtrl::toPositionH(polysurf_eval);
        return p;
      }
    };




    template <typename SubParametricObject_T>
    struct SubCurveInPolygonSurfaceEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpaceCurve              = PSpaceObject;
      using ParametricPolygonSurface = PObject;




      static EvaluationResult
      evaluate(const PSpaceCurve&                       pspace_curve,
               const ParametricPolygonSurface*          ppolysurf,
               const typename PSpaceCurve::PSpacePoint& par,
               const typename PSpaceCurve::PSpaceSizeArray& /*no_der*/,
               const typename PSpaceCurve::PSpaceBoolArray& /*from_left*/)
      {

        // Compute position
        const auto pspace_eval = pspace_curve.evaluateParent(par, {0});
        const auto ppolysurf_par
          = blaze::subvector<0UL, ParametricPolygonSurface::PSpaceVectorDim>(
            pspace_eval[0]);

        const auto polysurf_eval
          = ppolysurf->evaluateLocal(ppolysurf_par, {0, 0});

        // Compute differentials
        const auto h        = 1e-6;
        const auto pspace_d = typename PSpaceCurve::PSpaceVector{h};

        const auto [Dc_ppolysurf, Dc_ppolysurf_scale]
          = D(pspace_curve, par, pspace_d, *ppolysurf);


        // Results
        EvaluationResult p(2);
        p[0] = PObjEvalCtrl::toPositionH(polysurf_eval);
        p[1] = Dc_ppolysurf * Dc_ppolysurf_scale;
        return p;
      }

    private:
      // Differntial operator
      static constexpr approximation::
        SubObjectDirectionalDerivativeLocal<>
          D{};
    };


    template <typename SubParametricObject_T>
    struct SubSurfaceInPolygonSurfaceEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpaceSurface            = PSpaceObject;
      using ParametricPolygonSurface = PObject;


      static EvaluationResult
      evaluate(const PSpaceSurface&                           pspace_polysurf,
               const ParametricPolygonSurface*                ppolysurf,
               const typename PSpaceSurface::PSpacePoint&     par,
               const typename PSpaceSurface::PSpaceSizeArray& no_der,
               const typename PSpaceSurface::PSpaceBoolArray& /*from_left*/)
      {


        // function val
        //        std::cout << "-- polysub (pspace):" << std::endl;
        const auto pspace_eval = pspace_polysurf.evaluateParent(par, {0, 0});
        const auto pobj_par
          = blaze::subvector<0UL, ParametricPolygonSurface::PSpaceVectorDim>(
            pspace_eval(0, 0));
        //        std::cout << "-- polysub (pobj):" << std::endl;
        const auto pobj_eval = ppolysurf->evaluateLocal(pobj_par, {0, 0});



        EvaluationResult p(no_der[0] + 1, no_der[1] + 1);

        // f
        p(0, 0) = pobj_eval(0, 0);

        // fu
        if (no_der[0] > 0) {
          const auto pspace_d0 = typename PSpaceSurface::PSpaceVector{1e-6, 0};
          const auto [Dp0_ppolysurf, Dp0_ppolysurf_scale]
            = D(pspace_polysurf, par, pspace_d0, *ppolysurf);

          p(1, 0) = Dp0_ppolysurf * Dp0_ppolysurf_scale;
        }

        // fv
        if (no_der[1] > 0) {
          const auto pspace_d1 = typename PSpaceSurface::PSpaceVector{0, 1e-6};
          const auto [Dp1_ppolysurf, Dp1_ppolysurf_scale]
            = D(pspace_polysurf, par, pspace_d1, *ppolysurf);

          p(0, 1) = Dp1_ppolysurf * Dp1_ppolysurf_scale;
        }

        // Fuv
        if (no_der[0] > 0 and no_der[1] > 0)
          p(1, 1) = typename EvaluationResult::ElementType(0);

        //                std::cout << "===============================\n"
        //                          << p << '\n'
        //                          << "=== END =======================" <<
        //                          std::endl;

        return p;
      }

    private:
      // Differntial operator
      static constexpr approximation::
        SubObjectDirectionalDerivativeLocal<>
          D{};
    };





    template <typename SubParametricObject_T>
    struct SubPolygonSurfaceInPolygonSurfaceEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpacePolygonSurface     = PSpaceObject;
      using ParametricPolygonSurface = PObject;


      static EvaluationResult evaluate(
        const PSpacePolygonSurface&                           pspace_polysurf,
        const ParametricPolygonSurface*                       ppolysurf,
        const typename PSpacePolygonSurface::PSpacePoint&     par,
        const typename PSpacePolygonSurface::PSpaceSizeArray& no_der,
        const typename PSpacePolygonSurface::PSpaceBoolArray& /*from_left*/)
      {


        // function val
        //        std::cout << "-- polysub (pspace):" << std::endl;
        const auto pspace_eval = pspace_polysurf.evaluateParent(par, {0, 0});
        const auto pobj_par
          = blaze::subvector<0UL, ParametricPolygonSurface::PSpaceVectorDim>(
            pspace_eval(0, 0));
        //        std::cout << "-- polysub (pobj):" << std::endl;
        const auto pobj_eval = ppolysurf->evaluateLocal(pobj_par, {0, 0});

        constexpr auto step = 1e-5;


        EvaluationResult p(no_der[0] + 1, no_der[1] + 1);

        // f
        p(0, 0) = pobj_eval(0, 0);

        // fu
        if (no_der[0] > 0) {
          const auto pspace_d0 =
            typename PSpacePolygonSurface::PSpaceVector{step, 0};
          const auto [Dp0_ppolysurf, Dp0_ppolysurf_scale]
            = D(pspace_polysurf, par, pspace_d0, *ppolysurf);

          p(1, 0) = Dp0_ppolysurf * Dp0_ppolysurf_scale;
        }

        // fv
        if (no_der[1] > 0) {
          const auto pspace_d1 =
            typename PSpacePolygonSurface::PSpaceVector{0, step};
          const auto [Dp1_ppolysurf, Dp1_ppolysurf_scale]
            = D(pspace_polysurf, par, pspace_d1, *ppolysurf);

          p(0, 1) = Dp1_ppolysurf * Dp1_ppolysurf_scale;
        }

        // Fuv
        if (no_der[0] > 0 and no_der[1] > 0)
          p(1, 1) = typename EvaluationResult::ElementType(0);

        //                std::cout << "===============================\n"
        //                          << p << '\n'
        //                          << "=== END =======================" <<
        //                          std::endl;

        return p;
      }

    private:
      // Differntial operator
      static constexpr approximation::
        SubObjectDirectionalDerivativeLocal<>
          D{};
    };





    template <typename SubParametricObject_T>
    struct SubPolygonSurfaceInSurfaceEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpacePolygonSurface = PSpaceObject;
      using ParametricSurface    = PObject;


      static EvaluationResult evaluate(
        const PSpacePolygonSurface&                           pspace_polysurf,
        const ParametricSurface*                              psurface,
        const typename PSpacePolygonSurface::PSpacePoint&     par,
        const typename PSpacePolygonSurface::PSpaceSizeArray& no_der,
        const typename PSpacePolygonSurface::PSpaceBoolArray& /*from_left*/)
      {


        // function val
        //        std::cout << "-- polysub (pspace):" << std::endl;
        const auto pspace_eval = pspace_polysurf.evaluateParent(par, {0, 0});
        const auto pobj_par
          = blaze::subvector<0UL, ParametricSurface::PSpaceVectorDim>(
            pspace_eval(0, 0));
        //        std::cout << "-- polysub (pobj):" << std::endl;
        const auto pobj_eval = psurface->evaluateLocal(pobj_par, {0, 0});



        EvaluationResult p(no_der[0] + 1, no_der[1] + 1);

        // f
        p(0, 0) = pobj_eval(0, 0);

        // fu
        if (no_der[0] > 0) {
          const auto pspace_d0 =
            typename PSpacePolygonSurface::PSpaceVector{1e-6, 0};
          const auto [Dp0_ppolysurf, Dp0_ppolysurf_scale]
            = D(pspace_polysurf, par, pspace_d0, *psurface);

          p(1, 0) = Dp0_ppolysurf * Dp0_ppolysurf_scale;
        }

        // fv
        if (no_der[1] > 0) {
          const auto pspace_d1 =
            typename PSpacePolygonSurface::PSpaceVector{0, 1e-6};
          const auto [Dp1_ppolysurf, Dp1_ppolysurf_scale]
            = D(pspace_polysurf, par, pspace_d1, *psurface);

          p(0, 1) = Dp1_ppolysurf * Dp1_ppolysurf_scale;
        }

        // Fuv
        if (no_der[0] > 0 and no_der[1] > 0)
          p(1, 1) = typename EvaluationResult::ElementType(0);

        //                std::cout << "===============================\n"
        //                          << p << '\n'
        //                          << "=== END =======================" <<
        //                          std::endl;

        return p;
      }

    private:
      // Differntial operator
      static constexpr approximation::SubObjectDirectionalDerivativeLocal<> D{};
    };

  }   // namespace evaluationctrl



  //////////////////////////////////
  /// Parametric SubPoint In PolygonSurface
  ///
  template <template <typename, template <typename> typename>
            typename PSpacePoint_T,
            typename ParametricObject_T,
            typename BaseSpaceObjectBase_T =
              typename ParametricObject_T::BaseSpaceObjectBase,
            template <typename> typename PSubObjEvalCtrl_T
            = evaluationctrl::SubPointInPolygonSurfaceEvalCtrl,
            template <typename> typename PSpaceObject_PObjEvalCtrl_T
            = evaluationctrl::PPointEvalCtrl,
            template <typename> typename PSubObj_PObjEvalCtrl_T
            = evaluationctrl::PPointEvalCtrl>
  using SubPointInPolygonSurface
    = ParametricSubObject<PSpacePoint_T, ParametricObject_T, PSubObjEvalCtrl_T,
                          PSpaceObject_PObjEvalCtrl_T, PSubObj_PObjEvalCtrl_T,
                          BaseSpaceObjectBase_T>;





  //////////////////////////////////
  /// Parametric SubCurve In PolygonSurface
  ///
  template <template <typename, template <typename> typename>
            typename PSpaceCurve_T,
            typename ParametricObject_T,
            typename BaseSpaceObjectBase_T =
              typename ParametricObject_T::BaseSpaceObjectBase,
            template <typename> typename PSubObjEvalCtrl_T
            = evaluationctrl::SubCurveInPolygonSurfaceEvalCtrl,
            template <typename> typename PSpaceObject_PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl,
            template <typename> typename PSubObj_PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  using SubCurveInPolygonSurface
    = ParametricSubObject<PSpaceCurve_T, ParametricObject_T, PSubObjEvalCtrl_T,
                          PSpaceObject_PObjEvalCtrl_T, PSubObj_PObjEvalCtrl_T,
                          BaseSpaceObjectBase_T>;




  //////////////////////////////////
  /// SubSurface on PolygonSurfaceConstruction
  ///
  template <template <typename, template <typename> typename>
            typename PSpaceSurface_T,
            typename ParametricObject_T,
            typename BaseSpaceObjectBase_T =
              typename ParametricObject_T::BaseSpaceObjectBase,
            template <typename> typename PSubObjEvalCtrl_T
            = evaluationctrl::SubSurfaceInPolygonSurfaceEvalCtrl,
            template <typename> typename PSpaceObject_PObjEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl,
            template <typename> typename PSubObj_PObjEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  using SubSurfaceInPolygonSurface
    = ParametricSubObject<PSpaceSurface_T, ParametricObject_T,
                          PSubObjEvalCtrl_T, PSpaceObject_PObjEvalCtrl_T,
                          PSubObj_PObjEvalCtrl_T, BaseSpaceObjectBase_T>;




  ///////////////////////////////////////////////////
  /// SubPolygonSurface on PolygonSurfaceConstruction
  template <template <typename, template <typename> typename>
            typename PolygonSurfaceConstruction_T,
            typename ParametricObject_T,
            typename BaseSpaceObjectBase_T =
              typename ParametricObject_T::BaseSpaceObjectBase,
            template <typename> typename PSubObjEvalCtrl_T
            = evaluationctrl::SubPolygonSurfaceInPolygonSurfaceEvalCtrl,
            template <typename> typename PSpaceObject_PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl,
            template <typename> typename PSubObj_PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl>
  struct SubPolygonSurfaceInPolygonSurface
    : ParametricSubObject<PolygonSurfaceConstruction_T, ParametricObject_T,
                          PSubObjEvalCtrl_T, PSpaceObject_PObjEvalCtrl_T,
                          PSubObj_PObjEvalCtrl_T, BaseSpaceObjectBase_T> {

    using Base = ParametricSubObject<
      PolygonSurfaceConstruction_T, ParametricObject_T, PSubObjEvalCtrl_T,
      PSpaceObject_PObjEvalCtrl_T, PSubObj_PObjEvalCtrl_T,
      BaseSpaceObjectBase_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using Base::Base;
    auto polygon2D() const { return this->m_pspace_object.polygon2D(); }
    auto sides() const { return this->m_pspace_object.sides(); }
  };


  ////////////////////////////////
  /// SubPolygonSurface on Surface
  template <template <typename, template <typename> typename>
            typename PolygonSurfaceConstruction_T,
            typename ParametricObject_T,
            typename BaseSpaceObjectBase_T =
              typename ParametricObject_T::BaseSpaceObjectBase,
            template <typename> typename PSubObjEvalCtrl_T
            = evaluationctrl::SubPolygonSurfaceInSurfaceEvalCtrl,
            template <typename> typename PSpaceObject_PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl,
            template <typename> typename PSubObj_PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl>
  struct SubPolygonSurfaceInSurface
    : ParametricSubObject<PolygonSurfaceConstruction_T, ParametricObject_T,
                          PSubObjEvalCtrl_T, PSpaceObject_PObjEvalCtrl_T,
                          PSubObj_PObjEvalCtrl_T, BaseSpaceObjectBase_T> {

    using Base
      = ParametricSubObject<PolygonSurfaceConstruction_T, ParametricObject_T,
                            PSubObjEvalCtrl_T, PSpaceObject_PObjEvalCtrl_T,
                            PSubObj_PObjEvalCtrl_T, BaseSpaceObjectBase_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using Base::Base;
    auto polygon2D() const { return this->m_pspace_object.polygon2D(); }
    auto sides() const { return this->m_pspace_object.sides(); }
  };

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_SUBOBJECT_SUBTRIANGLESURFACE_H
