#ifndef GM2_PARAMETRIC_SUBOBJECT_SUBCURVE_H
#define GM2_PARAMETRIC_SUBOBJECT_SUBCURVE_H



#include "../curve.h"
#include "../surface.h"
#include "../volume.h"

#include "../parametricsubobject.h"
#include "../utils/approximation.h"

// stl
#include <type_traits>

namespace gmlib2::parametric
{

  namespace evaluationctrl
  {

    //////////SubCurveEvalCtrl////////
    /// evaluate(...)
    /// SubCurve in Curve
    ///
    template <typename SubParametricObject_T>
    struct SubCurveInCurveEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpaceCurve     = PSpaceObject;
      using ParametricCurve = PObject;

      static EvaluationResult
      evaluate(const PSpaceCurve& pspace_curve, const ParametricCurve* pcurve,
               const typename PSpaceCurve::PSpacePoint&     par,
               const typename PSpaceCurve::PSpaceSizeArray& no_der,
               const typename PSpaceCurve::PSpaceBoolArray& /*from_left*/)
      {
        // Evaluate pspace curve -- result in homogeneous coords ^^,
        const auto pspace_eval
          = pspace_curve.evaluateParent(par, no_der, {{true}});

        // Make pspace curve result not homeogeneous coords
        const auto pcurve_par
          = blaze::subvector<0UL, ParametricCurve::PSpaceVectorDim>(
            pspace_eval[0UL]);

        // Evaluate surface along pspace curve
        const auto pcurve_eval
          = pcurve->evaluateLocal(pcurve_par, no_der, {{true}});

        // Copy the result and scale the derivatives.
        // The scaling is not (yet) sane for no_der > 1.
        EvaluationResult p(pcurve_eval);


        auto d_scale{0.0};
        for (size_t i{1}; i <= no_der[0]; ++i) {
          d_scale
            += blaze::length(
                 blaze::subvector<0UL, PSpaceCurve::VectorDim>(pspace_eval[i]))
               / (pcurve->endParameters()[0] - pcurve->startParameters()[0]);
          p[i] *= d_scale;
        }

        return p;
      }
    };




    ////////////////////
    /// evaluate(...)
    /// SubCurve in Surface
    ///
    template <typename SubParametricObject_T>
    struct SubCurveInSurfaceEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpaceCurve       = PSpaceObject;
      using ParametricSurface = PObject;

      static EvaluationResult
      evaluate(const PSpaceCurve&                           pspace_curve,
               const ParametricSurface*                     psurface,
               const typename PSpaceCurve::PSpacePoint&     par,
               const typename PSpaceCurve::PSpaceSizeArray& no_der,
               const typename PSpaceCurve::PSpaceBoolArray& /*from_left*/)
      {

        // Evaluate pspace curve -- result in homogeneous coords ^^,
        const auto pspace_eval
          = pspace_curve.evaluateParent(par, {{0}}, {{true}});

        // Make pspace curve result not homeogeneous coords
        const auto psurface_par
          = blaze::subvector<0UL, ParametricSurface::PSpaceVectorDim>(
            pspace_eval[0UL]);

        // Evaluate surface along pspace curve
        const auto psurface_eval
          = psurface->evaluateLocal(psurface_par, {{0, 0}}, {{true, true}});


        // Results
        EvaluationResult p(no_der[0] + 1);

        // S(C)
        p[0] = PObjEvalCtrl::toPositionH(psurface_eval);

        // D_DtC S(C)
        if (no_der[0] > 0) {
          const auto pspace_d = typename PSpaceCurve::PSpaceVector{1e-6};
          const auto [Dc_psurface, Dc_psurface_scale]
            = D(pspace_curve, par, pspace_d, *psurface);

          p[1] = Dc_psurface * Dc_psurface_scale;
        }

        return p;
      }

    private:
      // Differntial operator
      static constexpr gmlib2::parametric::approximation::
        SubObjectDirectionalDerivativeLocal<>
          D{};
    };



    ////////////////////
    /// evaluate(...)
    /// SubCurve in Volume
    ///
    template <typename SubParametricObject_T>
    struct SubCurveInVolumeEvalCtrl {

      GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES


      using PSpaceCurve      = PSpaceObject;
      using ParametricVolume = PObject;

      static EvaluationResult
      evaluate(const PSpaceCurve& pspace_curve, const ParametricVolume* pvolume,
               const typename PSpaceCurve::PSpacePoint&     par,
               const typename PSpaceCurve::PSpaceSizeArray& no_der,
               const typename PSpaceCurve::PSpaceBoolArray& /*from_left*/)
      {

        // Evaluate pspace curve -- result in homogeneous coords ^^,
        const auto pspace_eval
          = pspace_curve.evaluateParent(par, {{0}}, {{true}});

        // Make pspace curve result not homeogeneous coords
        const auto pvolume_par
          = blaze::subvector<0UL, ParametricVolume::PSpaceVectorDim>(
            pspace_eval[0UL]);

        // Evaluate volume along pspace curve
        const auto pvolume_eval
          = pvolume->evaluateLocal(pvolume_par, {{0, 0}}, {{true, true}});


        // Results
        EvaluationResult p(no_der[0] + 1);

        // V(C)
        p[0] = PObjEvalCtrl::toPositionH(pvolume_eval);

        // D_DtC V(C)
        if (no_der[0] > 0) {
          const auto pspace_d = typename PSpaceCurve::PSpaceVector{1e-6};
          const auto [Dc_pvolume, Dc_pvolume_scale]
            = D(pspace_curve, par, pspace_d, *pvolume);

          p[1] = Dc_pvolume * Dc_pvolume_scale;
        }
        return p;
      }

    private:
      // Differntial operator
      static constexpr gmlib2::parametric::approximation::
        SubObjectDirectionalDerivativeLocal<>
          D{};
    };

  }   // namespace evaluationctrl




  ////////////////////////////////
  /// Parametric SubCurve In Curve

  template <
    template <typename, template <typename> typename> typename PSpaceCurve_T,
    typename PObject_T,
    typename BaseSpaceObjectBase_T = typename PObject_T::BaseSpaceObjectBase,
    template <typename> typename PSubObjEvalCtrl_T
    = evaluationctrl::SubCurveInCurveEvalCtrl,
    template <typename> typename PSpaceObject_PObjEvalCtrl_T
    = evaluationctrl::CurveEvalCtrl,
    template <typename> typename PSubObj_PObjEvalCtrl_T
    = evaluationctrl::CurveEvalCtrl>
  using SubCurveInCurve
    = ParametricSubObject<PSpaceCurve_T, PObject_T, PSubObjEvalCtrl_T,
                          PSpaceObject_PObjEvalCtrl_T, PSubObj_PObjEvalCtrl_T,
                          BaseSpaceObjectBase_T>;

  //////////////////////////////////
  /// Parametric SubCurve In Surface
  ///
  template <
    template <typename, template <typename> typename> typename PSpaceCurve_T,
    typename PObject_T,
    typename BaseSpaceObjectBase_T = typename PObject_T::BaseSpaceObjectBase,
    template <typename> typename PSubObjEvalCtrl_T
    = evaluationctrl::SubCurveInSurfaceEvalCtrl,
    template <typename> typename PSpaceObject_PObjEvalCtrl_T
    = evaluationctrl::CurveEvalCtrl,
    template <typename> typename PSubObj_PObjEvalCtrl_T
    = evaluationctrl::CurveEvalCtrl>
  using SubCurveInSurface
    = ParametricSubObject<PSpaceCurve_T, PObject_T, PSubObjEvalCtrl_T,
                          PSpaceObject_PObjEvalCtrl_T, PSubObj_PObjEvalCtrl_T,
                          BaseSpaceObjectBase_T>;

  //////////////////////////////////
  /// Parametric SubCurve In Volume
  ///
  template <
    template <typename, template <typename> typename> typename PSpaceCurve_T,
    typename PObject_T,
    typename BaseSpaceObjectBase_T = typename PObject_T::BaseSpaceObjectBase,
    template <typename> typename PSubObjEvalCtrl_T
    = evaluationctrl::SubCurveInVolumeEvalCtrl,
    template <typename> typename PSpaceObject_PObjEvalCtrl_T
    = evaluationctrl::CurveEvalCtrl,
    template <typename> typename PSubObj_PObjEvalCtrl_T
    = evaluationctrl::CurveEvalCtrl>
  using SubCurveInVolume
    = ParametricSubObject<PSpaceCurve_T, PObject_T, PSubObjEvalCtrl_T,
                          PSpaceObject_PObjEvalCtrl_T, PSubObj_PObjEvalCtrl_T,
                          BaseSpaceObjectBase_T>;


}   // namespace gmlib2::parametric





#endif   // GM2_PARAMETRIC_SUBOBJECT_SUBCURVE_H
