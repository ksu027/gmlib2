#ifndef GM2_PARAMETRIC_CURVE_BLENDINGSPLINECURVE_H
#define GM2_PARAMETRIC_CURVE_BLENDINGSPLINECURVE_H


#include "../curve.h"
#include "../../core/basis/bfunction.h"
#include "../../core/basis/bfunctions/fabius.h"

// stl
#include <iterator>

namespace gmlib2::parametric
{


  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            typename BFunction_T            = basis::bfunction::FabiusBFunction,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  class BlendingSplineCurve
    : public   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {

  public:
    using Base =   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using BFunction   = BFunction_T;
    using LocalCurve  = Curve<SpaceObjectEmbedBase_T>;
    using LocalCurves = std::vector<LocalCurve*>;
    using KnotVector  = std::vector<PSpaceUnit>;


    // Constructor(s)
    template <typename... Ts>
    BlendingSplineCurve(std::vector<LocalCurve*> c, std::vector<PSpaceUnit> t,
                         bool closed, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_c(c), m_t(t), m_closed(closed)
    {
    }

    // Members
    LocalCurves m_c;
    KnotVector  m_t;
    bool        m_closed{false};


    // ParametricObject interface
  public:
    PSpaceBoolArray isClosed() const override { return {m_closed}; }
    PSpacePoint     startParameters() const override
    {
      return PSpacePoint{*std::next(std::begin(m_t))};
    }
    PSpacePoint endParameters() const override
    {
      return PSpacePoint{*std::next(std::rbegin(m_t))};
    }

  protected:
    EvaluationResult evaluate(const PSpacePoint&     par,
                              const PSpaceSizeArray& no_der,
                              const PSpaceBoolArray& from_left) const override;

  private:
    Point evaluatePosition(const PSpacePoint&     par,
                           const PSpaceBoolArray& from_left) const;

  private:
    static constexpr BFunction                                        m_B{};
    static constexpr gmlib2::divideddifference::fd::ForwardDifference m_D{};
  };





  template <typename SpaceObjectEmbedBase_T, typename BFunction_T,
            template <typename> typename PObjEvalCtrl_T>
  typename BlendingSplineCurve<SpaceObjectEmbedBase_T, BFunction_T,
                             PObjEvalCtrl_T>::EvaluationResult
  BlendingSplineCurve<SpaceObjectEmbedBase_T, BFunction_T,
                    PObjEvalCtrl_T>::evaluate(const PSpacePoint&     par,
                                              const PSpaceSizeArray& no_der,
                                              const PSpaceBoolArray& from_left)
    const
  {
    const auto eval_fn = [this, from_left](const auto& at_par) {
      return evaluatePosition(at_par, from_left);
    };

    // Results
    EvaluationResult p(no_der[0] + 1);

    // C
    p[0] = gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
      eval_fn(par), Unit(1));

    // Du S
    if (no_der[0] > 0) {
      const auto par_d0 = PSpaceVector{1e-6};
      const auto Ds0    = m_D(par, par_d0, eval_fn);

      p[1]
        = gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds0, Unit(1));
    }

    //      std::cout << "BSCurve p: " << p << std::endl;

    return p;
  }


  template <typename SpaceObjectEmbedBase_T, typename BFunction_T,
            template <typename> typename PObjEvalCtrl_T>
  typename BlendingSplineCurve<SpaceObjectEmbedBase_T, BFunction_T,
                             PObjEvalCtrl_T>::Point
  BlendingSplineCurve<SpaceObjectEmbedBase_T, BFunction_T,
                    PObjEvalCtrl_T>::evaluatePosition(const PSpacePoint& par,
                                                      const PSpaceBoolArray&
                                                        from_left) const
  {

    const auto& t = par[0];

    size_t k;
    for (k = 1UL; k < m_t.size() - 2; ++k)
      if (t < m_t[k + 1]) break;

    // If right-evaluation, find first knot
    if (not from_left[0])
      while (std::abs(m_t[k] - m_t[k - 1]) < 1e-5) --k;

    const auto mapToLocal = [this](const auto& tm, size_t tk) {
      const auto* c  = m_c[tk - 1];
      const auto  cs = c->startParameters();

      const auto cd = c->endParameters() - cs;

      const auto mtl = blaze::evaluate(
        cs + (tm - m_t[tk - 1]) / (m_t[tk + 1] - m_t[tk - 1]) * cd);


      return mtl;
    };


    const auto c0_mtl = mapToLocal(t, k);
    const auto c0     = m_c[k - 1]->evaluateParent(c0_mtl, PSpaceSizeArray{0});

    if (std::abs(t - m_t[k]) < 1e-5) {
      return PObjEvalCtrl::toPosition(c0);
    }

    const auto c1
      = m_c[k]->evaluateParent(mapToLocal(t, k + 1), PSpaceSizeArray{0});

    const auto loc_t = (t - m_t[k]) / (m_t[k + 1] - m_t[k]);
    const auto B     = m_B(loc_t);

    return blaze::evaluate(
      blaze::subvector<0UL, VectorDim>(c0[0] + (c1[0] - c0[0]) * B));
  }


}   // namespace gmlib2::parametric



#endif   // GM2_PARAMETRIC_CURVE_BLENDINGSPLINECURVE_H
