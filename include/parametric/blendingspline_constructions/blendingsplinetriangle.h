#ifndef GM2_PARAMETRIC_TRIANGLESURFACE_BLENDINGSPLINETRIANGLE_H
#define GM2_PARAMETRIC_TRIANGLESURFACE_BLENDINGSPLINETRIANGLE_H



#include "../trianglesurface.h"
#include "../../core/basis/triangleblendingbasis.h"
#include "../../core/basis/bfunction.h"
#include "../../core/basis/bfunctions/fabius.h"

// stl
#include <variant>

namespace gmlib2::parametric
{


  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            typename BFunction_T            = basis::bfunction::FabiusBFunction,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::TriangleSurfaceEvalCtrl>
  class BlendingSplineTriangle
    : public TriangleSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
    using Base = TriangleSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES



    using LocalTriangle  = TriangleSurface<SpaceObjectEmbedBase_T>;
    using LocalTriangles = std::array<LocalTriangle*, 3ul>;

    using BFunction              = BFunction_T;
    using BFunctionBasisTriangle
    = basis::triangleblendingbasis::RationalTriangleBlendingBasis<BFunction>;
//      = trianglebendingba::RationalBasisTriangle<BFunction>;


    // Constructor(s)
    template <typename... Ts>
    BlendingSplineTriangle(const LocalTriangles& c, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_c{c}
    {
    }
    // Member(s)
    LocalTriangles m_c;

    // Polygon interface
  public:
    PSpaceBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        false);
    }

    PSpacePoint startParameters() const override
    {

      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(0.0);
    }

    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(1.0);
    }

  protected:
    using EvalTuple = std::tuple<Point, Point, Point, Point>;

    EvaluationResult evaluate(const PSpacePoint&                      par,
                              [[maybe_unused]] const PSpaceSizeArray& no_der
                              = {},
                              [[maybe_unused]] const PSpaceBoolArray& from_left
                              = {}) const override
    {
      // Contextualize input paramters
      const auto u  = par[0];
      const auto v  = par[1];
      const auto w  = Unit(1) - u - v;
      const auto bc = VectorT<Unit, 3ul>{u, v, w};

      const auto* c0 = m_c[0];
      const auto* c1 = m_c[1];
      const auto* c2 = m_c[2];


      // Evaluate
      const auto B  = m_B.template evaluate<PSpaceUnit, 0>(u, v, w);
      const auto B0 = B[0];
      const auto B1 = B[1];
      const auto B2 = B[2];

      const auto DB   = m_B.template evaluate<PSpaceUnit, 1>(u, v, w);
      const auto DuB0 = DB(0, 0);
      const auto DuB1 = DB(0,1);
      const auto DuB2 = DB(0,2);
      const auto DvB0 = DB(1,0);
      const auto DvB1 = DB(1,1);
      const auto DvB2 = DB(1,2);
      const auto DwB0 = DB(2,0);
      const auto DwB1 = DB(2,1);
      const auto DwB2 = DB(2,2);


      const auto c0eval = c0->evaluateParent(PSpacePoint{u, v}, no_der);
      const auto c1eval = c1->evaluateParent(PSpacePoint{v, w}, no_der);
      const auto c2eval = c2->evaluateParent(PSpacePoint{w, u}, no_der);

      const auto C0  = blaze::subvector<0UL, VectorDim>(c0eval[0]);
      const auto C0u = blaze::subvector<0UL, VectorDim>(c0eval[1]);
      const auto C0v = blaze::subvector<0UL, VectorDim>(c0eval[2]);
      const auto C0w = blaze::subvector<0UL, VectorDim>(c0eval[3]);

      const auto C1  = blaze::subvector<0UL, VectorDim>(c1eval[0]);
      const auto C1u = blaze::subvector<0UL, VectorDim>(c1eval[1]);
      const auto C1v = blaze::subvector<0UL, VectorDim>(c1eval[2]);
      const auto C1w = blaze::subvector<0UL, VectorDim>(c1eval[3]);

      const auto C2  = blaze::subvector<0UL, VectorDim>(c2eval[0]);
      const auto C2u = blaze::subvector<0UL, VectorDim>(c2eval[1]);
      const auto C2v = blaze::subvector<0UL, VectorDim>(c2eval[2]);
      const auto C2w = blaze::subvector<0UL, VectorDim>(c2eval[3]);


      // clang-format off
      const auto S = C0 * B0 + C1 * B1 + C2 * B2;
      const auto Su =
          C0 * DuB0 + C0u * B0 +
          C1 * DuB1 + C1w * B1 +
          C2 * DuB2 + C2w * B2;

      const auto Sv =
          C0 * DvB0 + C0v * B0 +
          C1 * DvB1 + C1u * B1 +
          C2 * DvB2 + C2w * B2;

      const auto Sw =
          C0 * DwB0 + C0w * B0 +
          C1 * DwB1 + C1v * B1 +
          C2 * DwB2 + C2u * B2;
      // clang-format on


      EvaluationResult p(4UL);

      // Fill
      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        S, Unit(1));
      p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        Su, Unit(1));
      p[2] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        Sv, Unit(1));
      p[3] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        Sw, Unit(1));

      return p;
    }

  private:
    static constexpr BFunctionBasisTriangle m_B{};
  };

}   // namespace gmlib2::parametric



#endif   // GM2_PARAMETRIC_TRIANGLESURFACE_BLENDINGSPLINETRIANGLE_H
