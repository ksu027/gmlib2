#ifndef GM2_PARAMETRIC_TRIANGLESURFACE_BLENDINGSPLINEPOLYGONPATCH_H
#define GM2_PARAMETRIC_TRIANGLESURFACE_BLENDINGSPLINEPOLYGONPATCH_H

#include "../polygonsurface.h"
#include "../subobject_constructions/subpolygonsurface.h"

#include "../../core/basis/polygonblendingbasis.h"
#include "../../core/basis/bfunction.h"
#include "../../core/basis/bfunctions/fabius.h"

// stl
#include <variant>

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            typename BFunction_T            = basis::bfunction::FabiusBFunction,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl>
  class BlendingSplinePolygonPatch
    : public PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
    using Base = PolygonSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using BFunction = BFunction_T;
    using Polygon2D = typename Base::Polygon2D;
    using Lambda    = typename Base::Lambda;
    using Lambdas   = typename Base::Lambdas;
    using BVector   = gmlib2::DVectorT<PSpaceUnit>;
    using Lbar      = PolygonSurface<SpaceObjectEmbedBase_T>;
    using Lbars     = std::vector<Lbar*>;

    template <typename LhatPSpace_SpaceObjectEmbedBase_T,
              template <typename> typename LhatPSpace_PObjEvalCtrl_T>
    using LhatPSpaceObject
      = TransfiniteRibbonPatch<LhatPSpace_SpaceObjectEmbedBase_T,
                               LhatPSpace_PObjEvalCtrl_T>;

    using LhatObject
      = SubPolygonSurfaceInPolygonSurface<LhatPSpaceObject, Lbar,
                                          SpaceObjectEmbedBase_T>;
    using LhatObjects = std::vector<LhatObject*>;

    using QhatPoint = typename LhatObject::PSpaceObject_Point;
    using Qhat      = std::vector<QhatPoint>;
    using Qhats     = std::vector<Qhat>;


    // Constructor(s)
    template <typename... Ts>
    BlendingSplinePolygonPatch(const Lbars& L_bars, const Qhats& Q_hats,
                               Ts&&... ts)
      : Base(L_bars.size(), std::forward<Ts>(ts)...), m_L_bars(L_bars)
    {

      assert(std::distance(std::cbegin(L_bars), std::cend(L_bars))
             == std::distance(std::cbegin(Q_hats), std::cend(Q_hats)));


      // Construct sub-surfaces (L_hat)
      for (auto i = 0UL; i < m_L_bars.size(); ++i) {

        using RP = typename LhatObject::PSpaceObject::RibbonPoints;
        using RT = typename LhatObject::PSpaceObject::RibbonTangents;

        const auto& Q_hat = Q_hats[i];

        RP rp;
        rp.reserve(Q_hat.size());
        std::transform(std::begin(Q_hat), std::end(Q_hat),
                       std::back_inserter(rp),
                       [](const auto& v_hat) { return v_hat; });

        RT rt;
        rt.reserve(rp.size());
        for (auto j = 0UL; j < rp.size(); ++j) {

          const auto jp = (j + rp.size() + 1) % rp.size();

          const auto rp_j  = rp[j];
          const auto rp_jp = rp[jp];
          const auto rp_d  = rp_jp - rp_j;
          const auto rp_D  = Vector{rp_d[0], rp_d[1], 0.0};
          const auto rp_N  = Vector{0.0, 0.0, 1.0};
          const auto rp_T  = blaze::evaluate(blaze::cross(rp_N, rp_D));
          const auto rp_t =
            typename LhatObject::PSpaceObject_Vector{rp_T[0], rp_T[1]};

          const auto rp_t_scaled = rp_t * 0.33;
          rt.push_back(rp_t_scaled);
        }

        auto* lhat_obj = new LhatObject(m_L_bars[i], rp, rt);
        lhat_obj->m_pspace_object.m_polygon = this->m_polygon;

        m_L_hats.push_back(lhat_obj);
      }
    }


    // Members
    Lbars       m_L_bars;
    LhatObjects m_L_hats;


  public:
    PSpaceBoolArray isClosed() const override
    {
      return gmlib2::utils::initStaticContainer<PSpaceBoolArray,
                                                PSpaceVectorDim>(false);
    }
    PSpacePoint startParameters() const override
    {
      return gmlib2::utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(
        PSpaceUnit(0));
    }
    PSpacePoint endParameters() const override
    {
      return gmlib2::utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(
        PSpaceUnit(1));
    }




    void computePolygon2D()
    {

      gmlib2::DVectorT<gmlib2::VectorT<Unit, 3UL>> polygon3D(this->sides());
      for (auto i = 0UL; i < this->sides(); ++i) {

        const auto par_interp   = m_L_hats[i]->m_pspace_object.m_P[i];
        const auto l_bar_interp = Lbar::PObjEvalCtrl::toPosition(
          m_L_bars[i]->evaluateParent(par_interp, {0, 0}));

        polygon3D[i] = l_bar_interp;
      }

      const auto polygon2D
        = polygonutils::projectPolygonOntoBestFitApproximatedPlane(polygon3D);
      this->m_polygon = polygon2D;


      for (auto* lhat : m_L_hats)
        lhat->m_pspace_object.m_polygon = this->m_polygon;
    }

  protected:
    EvaluationResult evaluate(
      const PSpacePoint&     par,
      const PSpaceSizeArray& no_der
      = gmlib2::utils::initStaticContainer<PSpaceSizeArray, PSpaceVectorDim>(
        0UL),
      const PSpaceBoolArray& from_left
      = gmlib2::utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        true)) const override

    {
      // Evaluation function driver
      const auto eval_fn
        = [this](const auto& at_par) { return evaluatePosition(at_par); };

      // Helpers
      enum class InsidePolygonTestResult { Inside, Outside };

      [[maybe_unused]] const auto insidePolygonTestShVariation
        = [this](const PSpaceVector& psp) {
            const auto l = gmlib2::gbc::mvc(this->polygon2D(), psp);
            const auto s = gmlib2::gbc::gbcToSMapping(l);
            const auto h = gmlib2::gbc::gbcToHMapping(l);
            if (s < 0 or s > 1 or h < 0 or h > 1) {
              return InsidePolygonTestResult::Outside;
            }
            return InsidePolygonTestResult::Inside;
          };

      const auto compDD = [&](const PSpacePoint& psp, const PSpaceVector& psv) {
        return D(psp, psv, eval_fn);
      };


      enum class InKnot { True, False };

      const auto inKnot = [this](const PSpaceVector& psp) {
        constexpr auto ep  = 1e-2;
        const auto&    p2d = this->polygon2D();
        for (auto i = 0UL; i < p2d.size(); ++i) {
          const auto v = p2d[i];
          if (blaze::length(psp - v) < ep) return std::pair(InKnot::True, i);
        }
        return std::pair(InKnot::False, 0UL);
      };


      // Compute step size
      const auto step = 1e-5;

      // Results
      EvaluationResult p(no_der[0] + 1, no_der[1] + 1);

      // S
      p(0, 0)
        = gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          eval_fn(par), Unit(1));

      // Du S
      if (no_der[0] > 0) {

        const auto par_d0 = PSpaceVector{step, 0};

        auto [in_knot, knot_id] = inKnot(par);
        if (in_knot == InKnot::True) {
          const auto* L_hat          = m_L_hats[knot_id];
          const auto* L_bar          = m_L_bars[knot_id];
          const auto  L_hat_eval_res = L_hat->evaluateParent(par, {1, 0});
          const auto  L_bar_pframe   = L_bar->pSpaceFrameParent();
          p(1, 0)                    = L_bar_pframe * L_hat_eval_res(1, 0);
        }
        else
          p(1, 0) = gmlib2::utils::extendStaticContainer<Point, VectorH,
                                                         VectorDim, 1UL>(
            compDD(par, par_d0), Unit(1));
      }

      // Dv S
      if (no_der[1] > 0) {

        const auto par_d1 = PSpaceVector{0, step};

        auto [in_knot, knot_id] = inKnot(par);
        if (in_knot == InKnot::True) {
          const auto* L_hat          = m_L_hats[knot_id];
          const auto* L_bar          = m_L_bars[knot_id];
          const auto  L_hat_eval_res = L_hat->evaluateParent(par, {0, 1});
          const auto  L_bar_pframe   = L_bar->pSpaceFrameParent();
          p(0, 1)                    = L_bar_pframe * L_hat_eval_res(0, 1);
        }
        else
          p(0, 1) = gmlib2::utils::extendStaticContainer<Point, VectorH,
                                                         VectorDim, 1UL>(
            compDD(par, par_d1), Unit(1));
      }

      // Duv S
      if (no_der[0] > 0 and no_der[1] > 0)
        p(1, 1) = typename EvaluationResult::ElementType(0);

      return p;
    }

  public:
  private:
    Point evaluatePosition(const PSpacePoint& par) const
    {

      const auto  M = this->sides();
      const auto& V = this->polygon2D();
      const auto& v = par;

      // Find MVC
      const auto l = gbc::mvc(V, v);

      // Compute 's'' from l's
      std::vector<PSpaceUnit> Sis(M);
      const auto              L = gbc::generateLeftRotatedLambdaSet(l);
      std::transform(std::begin(L), std::end(L), std::begin(Sis),
                     [](const auto& Li) { return gbc::gbcToSMapping(Li); });

      // Evaluate locals
      std::vector<EvaluationResult> L_hat_samps(M);
      std::transform(std::begin(m_L_hats), std::end(m_L_hats),
                     std::begin(m_L_bars), std::begin(L_hat_samps),
                     [par](const auto& L_hat, const auto* L_bar) {
                       const auto eval_res = L_hat->evaluateParent(par, {0, 0});
                       const auto pframe   = L_bar->pSpaceFrameParent();

                       EvaluationResult res
                         = blaze::map(eval_res, [&pframe](const auto& ele) {
                             return blaze::evaluate(pframe * ele);
                           });
                       return res;
                     });

      // Blend locals over 's'
      const auto cmpSiBlend = [&](const auto& i) {
        const auto side_im = (i + M - 1) % M;
        const auto side_i  = i;

        const auto si = Sis[side_i];

        const auto bsi  = m_Bs(si);
        const auto mbsi = 1 - bsi;

        const Point Si_blend
          = PObjEvalCtrl::toPosition(L_hat_samps[side_im]) * mbsi
            + PObjEvalCtrl::toPosition(L_hat_samps[side_i]) * bsi;

        return Si_blend;
      };

      std::vector<Point> Si_blends(M);
      for (auto i = 0UL; i < M; ++i) {
        Si_blends[i] = cmpSiBlend(i);
      }

      // Evaluate blend basis over 'h'
      const auto Bh = m_Bh(l);

      // Blend 's'-blends over 'h'
      const Point res = std::inner_product(
        std::begin(Si_blends), std::end(Si_blends), std::begin(Bh), Point(0),
        std::plus<>(),
        [](const auto& side_samp, const auto& b) { return side_samp * b; });

      return res;
    }

    static constexpr basis::polygonblendingbasis::
      HungarianSpecialSideBlendingBasis<BFunction>
                                                     m_Bh{};
    static constexpr BFunction                       m_Bs{};

    static constexpr divideddifference::fd::CentralDifference  D{};
    static constexpr divideddifference::fd::ForwardDifference  Df{};
    static constexpr divideddifference::fd::BackwardDifference Db{};
  };


}   // namespace gmlib2::parametric



#endif   // GM2_PARAMETRIC_TRIANGLESURFACE_BLENDINGSPLINEPOLYGONPATCH_H
