#ifndef GM2_PARAMETRIC_SURFACE_BLENDINGSPLINESURFACE_H
#define GM2_PARAMETRIC_SURFACE_BLENDINGSPLINESURFACE_H

#include "../surface.h"
#include "../../core/basis/bfunction.h"
#include "../../core/basis/bfunctions/fabius.h"

// stl
#include <iterator>

namespace gmlib2::parametric
{



  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            typename BFunctionU_T           = basis::bfunction::FabiusBFunction,
            typename BFunctionV_T           = BFunctionU_T,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  class BlendingSplineSurface
    : public Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {

  public:
    using Base = Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using BFunctionU      = BFunctionU_T;
    using BFunctionV      = BFunctionV_T;
    using LocalSurface    = Surface<SpaceObjectEmbedBase_T>;
    using LocalSurfaceNet = std::vector<std::vector<LocalSurface*>>;
    using KnotVector      = std::vector<PSpaceUnit>;


    // Constructor(s)
    template <typename... Ts>
    BlendingSplineSurface(const LocalSurfaceNet& c, const KnotVector& u,
                        const KnotVector& v, const PSpaceBoolArray& closed,
                        Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_c(c), m_u(u), m_v(v), m_closed(closed)
    {
    }


    // Members
    LocalSurfaceNet m_c;
    KnotVector      m_u;
    KnotVector      m_v;
    PSpaceBoolArray m_closed{false, false};



    // ParametricObject interface
  public:
    PSpaceBoolArray isClosed() const override { return m_closed; }
    PSpacePoint     startParameters() const override
    {
      return PSpacePoint{*std::next(std::begin(m_u)),
                         *std::next(std::begin(m_v))};
    }
    PSpacePoint endParameters() const override
    {
      return PSpacePoint{*std::next(std::rbegin(m_u)),
                         *std::next(std::rbegin(m_v))};
    }

  protected:
    EvaluationResult evaluate(const PSpacePoint&     par,
                              const PSpaceSizeArray& no_der,
                              const PSpaceBoolArray& from_left) const override;

  private:
    Point evaluatePosition(const PSpacePoint&     par,
                           const PSpaceBoolArray& from_left) const;

  private:
    static constexpr BFunctionU                                       m_Bu{};
    static constexpr BFunctionV                                       m_Bv{};
    static constexpr gmlib2::divideddifference::fd::ForwardDifference m_D{};
  };






  template <typename SpaceObjectEmbedBase_T, typename BFunctionU_T,
            typename BFunctionV_T, template <typename> typename PObjEvalCtrl_T>
  typename BlendingSplineSurface<SpaceObjectEmbedBase_T, BFunctionU_T,
                                 BFunctionV_T, PObjEvalCtrl_T>::EvaluationResult
  BlendingSplineSurface<SpaceObjectEmbedBase_T, BFunctionU_T, BFunctionV_T,
                        PObjEvalCtrl_T>::evaluate(const PSpacePoint&     par,
                                                  const PSpaceSizeArray& no_der,
                                                  const PSpaceBoolArray&
                                                    from_left) const
  {
    const auto eval_fn = [this, from_left](const auto& at_par) {
      return evaluatePosition(at_par, from_left);
    };

    // Results
    EvaluationResult p(no_der[0] + 1, no_der[1] + 1);

    // C
    p(0, 0)
      = gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        eval_fn(par), Unit(1));

    // Du S
    if (no_der[0] > 0) {
      const auto par_d0 = PSpaceVector{1e-6, PSpaceUnit(0)};
      const auto Ds0    = m_D(par, par_d0, eval_fn);

      p(1, 0)
        = gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds0, Unit(1));
    }

    // Du S
    if (no_der[1] > 0) {
      const auto par_d1 = PSpaceVector{PSpaceUnit(0), 1e-6};
      const auto Ds1    = m_D(par, par_d1, eval_fn);

      p(0, 1)
        = gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          Ds1, Unit(1));
    }

    // Duv S
    if (no_der[0] > 0 and no_der[1] > 0) {
      p(1, 1) = typename EvaluationResult::ElementType(0);
    }


    //      std::cout << "BSCurve p: " << p << std::endl;

    return p;
  }


  template <typename SpaceObjectEmbedBase_T, typename BFunctionU_T, typename BFunctionV_T,
            template <typename> typename PObjEvalCtrl_T>
  typename BlendingSplineSurface<SpaceObjectEmbedBase_T, BFunctionU_T,BFunctionV_T,
                               PObjEvalCtrl_T>::Point
  BlendingSplineSurface<SpaceObjectEmbedBase_T, BFunctionU_T,BFunctionV_T,
                      PObjEvalCtrl_T>::evaluatePosition(const PSpacePoint& par,
                                                        const PSpaceBoolArray&
                                                          from_left) const
  {
    const auto findKnot
      = [](const PSpaceUnit& t, const KnotVector& T, const bool& t_from_left) {
          size_t k;
          for (k = 1UL; k < T.size() - 2; ++k)
            if (t < T[k + 1]) break;

          // If right-evaluation, find first knot
          if (not t_from_left)
            while (std::abs(T[k] - T[k - 1]) < 1e-5) --k;

          return k;
        };

    const auto mapToLocal = [](const PSpaceUnit& t, size_t tk,
                               const KnotVector& T, const PSpaceUnit& s,
                               const PSpaceUnit& e) {
      const auto d = e - s;
      const auto mtl
        = blaze::evaluate(s + (t - T[tk - 1]) / (T[tk + 1] - T[tk - 1]) * d);
      return mtl;
    };

    const auto getC = [this, par, mapToLocal](const PSpaceSizeArray& tk) {
      const auto& u = par[0];
      const auto& v = par[1];

      const auto& uk  = tk[0];
      const auto& vk  = tk[1];
      const auto  cuk = uk - 1;
      const auto  cvk = vk - 1;


      const auto* l0   = m_c[cuk][cvk];
      const auto  l0_s = l0->startParameters();
      const auto  l0_e = l0->endParameters();

      using L0        = typename std::decay_t<decltype(*l0)>;
      const auto l0_p = L0::PObjEvalCtrl::toPosition(l0->evaluateParent(
        PSpacePoint{mapToLocal(u, uk, m_u, l0_s[0], l0_e[0]),
                    mapToLocal(v, vk, m_v, l0_s[1], l0_e[1])},
        PSpaceSizeArray{0, 0}));

      if (std::abs(u - m_u[uk]) < 1e-5) return l0_p;

      const auto* l1   = m_c[cuk + 1][cvk];
      const auto  l1_s = l1->startParameters();
      const auto  l1_e = l1->endParameters();

      using L1        = typename std::decay_t<decltype(*l1)>;
      const auto l1_p = L1::PObjEvalCtrl::toPosition(l1->evaluateParent(
        PSpacePoint{mapToLocal(u, uk + 1, m_u, l1_s[0], l1_e[0]),
                    mapToLocal(v, vk, m_v, l1_s[1], l1_e[1])},
        PSpaceSizeArray{0, 0}));

      const auto loc_u = (u - m_u[uk]) / (m_u[uk + 1] - m_u[uk]);
      const auto Bu    = m_Bu(loc_u);

      return blaze::evaluate(l0_p + (l1_p - l0_p) * Bu);
    };




    const auto& u = par[0];
    const auto& v = par[1];

    const auto uk = findKnot(u, m_u, from_left[0]);
    const auto vk = findKnot(v, m_v, from_left[1]);

    const auto s0 = getC({uk, vk});

    if (std::abs(v - m_v[vk]) < 1e-5) return s0;

    const auto s1 = getC({uk, vk + 1});

    const auto loc_v = (v - m_v[vk]) / (m_v[vk + 1] - m_v[vk]);
    const auto Bv    = m_Bv(loc_v);

    return blaze::evaluate(s0 + (s1 - s0) * Bv);
  }


}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_SURFACE_BLENDINGSPLINESURFACE_H
