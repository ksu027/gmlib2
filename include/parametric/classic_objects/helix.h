#ifndef GM2_PARAMETRIC_CURVE_HELIX_H
#define GM2_PARAMETRIC_CURVE_HELIX_H


#include "../curve.h"

#include "../../core/coreutils.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  class Helix : public   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base =   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES


    // Constructor
    explicit Helix(Unit radius = Unit{1.0}, Unit slope = 1.0,
                    Unit revolutions = 2.0)
      : m_radius{radius}, m_slope{slope}, m_revolutions{revolutions}
    {
    }

    // Members
    Unit m_radius{1.0};
    Unit m_slope{1.0};
    Unit m_revolutions{2.0};

    //   Curve interface
  public:
    PSpaceBoolArray isClosed() const override;
    PSpacePoint     startParameters() const override;
    PSpacePoint     endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& from_left
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override;
  };




  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Helix<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpaceBoolArray
  Helix<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::isClosed() const
  {
    return {{true}};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Helix<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Helix<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::startParameters() const
  {
    return PSpacePoint{0};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Helix<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Helix<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::endParameters() const
  {
    return PSpacePoint{2 * M_PI * m_revolutions};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Helix<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::EvaluationResult
  Helix<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::evaluate(
    const PSpacePoint& par, const PSpaceSizeArray& no_der,
    const PSpaceBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    const Unit ct = m_radius * std::cos(t);
    const Unit st = m_radius * std::sin(t);

    p[0] = {ct, st, m_slope * t, 1};
    if (no_derivatives) p[1] = {-st, ct, m_slope, 0};

    return p;
  }

}   // namespace gmlib2::parametric


#endif   // GM2_PARAMETRIC_CURVE_HELIX_H
