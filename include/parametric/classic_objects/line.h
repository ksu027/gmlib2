#ifndef GM_PARAMETRIC_CURVE_LINE_H
#define GM_PARAMETRIC_CURVE_LINE_H



#include "../curve.h"

#include "../../core/coreutils.h"

#include <iostream>

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  class Line : public   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {

  public:
    using Base =   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES



    // Constructor
    Line(const Point&  origin    = Point(0.0),    // Point{0.0, 0.0, 0.0},
          const Vector& direction = Vector(1.0))   // Vector{1.0, 0.0, 0.0})
      : m_pt(utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, Unit(1))),
        m_v(utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
          direction, Unit(0)))
    {
    }

    template <typename... Ts>
    Line(const Point& origin, const Vector& direction, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_pt(utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, Unit(1))),
        m_v(utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
          direction, Unit(0)))
    {
    }

    // Members
    VectorH m_pt;
    VectorH m_v;

    //   Curve interface
  public:
    PSpaceBoolArray isClosed() const override;
    PSpacePoint     startParameters() const override;
    PSpacePoint     endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& from_left
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override;
  };




  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Line<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpaceBoolArray
  Line<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::isClosed() const
  {
    return {{false}};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Line<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Line<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::startParameters() const
  {
    return PSpacePoint{0};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Line<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Line<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::endParameters() const
  {
    return PSpacePoint{1};
  }


  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Line<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::EvaluationResult
  Line<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::evaluate(
    const PSpacePoint& par, const PSpaceSizeArray& no_der,
    const PSpaceBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    p[0] = m_pt + t * m_v;
    if (no_derivatives) p[1] = m_v;
    for (size_t i = 2; i < no_derivatives + 1; ++i) p[i] = {0.0, 0.0, 0.0, 0.0};

    return p;
  }

}   // namespace gmlib2::parametric



#endif   // GM_PARAMETRIC_CURVE_LINE_H
