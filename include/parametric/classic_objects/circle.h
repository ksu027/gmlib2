#ifndef GM2_PARAMETRIC_CURVE_CIRCLE_H
#define GM2_PARAMETRIC_CURVE_CIRCLE_H


#include "../curve.h"

#include "../../core/coreutils.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  class Circle : public   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base =   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES



    // Constructor
    Circle(Unit radius = Unit{3.0}) : m_radius{radius} {}

    template <typename... Ts>
    Circle(Unit radius, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_radius{radius}
    {
    }

    // Members
    Unit m_radius;

    //   Curve interface
  public:
    PSpaceBoolArray isClosed() const override;
    PSpacePoint     startParameters() const override;
    PSpacePoint     endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& from_left
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override;
  };




  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Circle<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpaceBoolArray
  Circle<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::isClosed() const
  {
    return {{true}};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Circle<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Circle<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::startParameters() const
  {
    return PSpacePoint{0};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Circle<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Circle<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::endParameters() const
  {
    return PSpacePoint{2 * M_PI};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Circle<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::EvaluationResult
  Circle<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::evaluate(
    const PSpacePoint& par, const PSpaceSizeArray& no_der,
    const PSpaceBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    const Unit ct = m_radius * std::cos(t);
    const Unit st = m_radius * std::sin(t);

    p[0] = {ct, st, 0, 1};
    if (no_derivatives) p[1] = {-st, ct, 0, 0};
    if (no_derivatives > 1) p[2] = -p[0];
    if (no_derivatives > 2) p[3] = -p[1];

    return p;
  }

}   // namespace gmlib2::parametric


#endif   // GM2_PARAMETRIC_CURVE_CIRCLE_H
