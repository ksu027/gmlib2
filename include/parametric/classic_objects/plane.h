#ifndef GM2_PARAMETRIC_CURVE_PLANE_H
#define GM2_PARAMETRIC_CURVE_PLANE_H


#include "../surface.h"

#include "../../core/coreutils.h"

namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  class Plane : public Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {

  public:
    using Base = Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructor(s)
    template <typename... Ts>
    Plane(const Point&  origin   = Point{0.0, 0.0, 0.0},
           const Vector& u_vector = Vector{1.0, 0.0, 0.0},
           const Vector& v_vector = Vector{0.0, 1.0, 0.0})
      : m_pt{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, Unit(1))},
        m_u{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          u_vector, Unit(0))},
        m_v{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          v_vector, Unit(0))}
    {
    }

    template <typename... Ts>
    Plane(const Point& origin, const Vector& u_vector, const Vector& v_vector,
           Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_pt{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, 1.0)},
        m_u{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          u_vector, 0.0)},
        m_v{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          v_vector, 0.0)}
    {
    }
    ~Plane() override = default;

    // Members
    VectorH m_pt;
    VectorH m_u;
    VectorH m_v;


    // TPSurf interface
  public:
    PSpaceBoolArray isClosed() const override;
    PSpacePoint     startParameters() const override;
    PSpacePoint     endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& from_left
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override;
  };





  template <typename SpaceObjectBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Plane<SpaceObjectBase_T, PObjEvalCtrl_T>::PSpaceBoolArray
  Plane<SpaceObjectBase_T, PObjEvalCtrl_T>::isClosed() const
  {
    return {{false, false}};
  }

  template <typename SpaceObjectBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Plane<SpaceObjectBase_T, PObjEvalCtrl_T>::PSpacePoint
  Plane<SpaceObjectBase_T, PObjEvalCtrl_T>::startParameters() const
  {
    return PSpacePoint{0, 0};
  }

  template <typename SpaceObjectBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Plane<SpaceObjectBase_T, PObjEvalCtrl_T>::PSpacePoint
  Plane<SpaceObjectBase_T, PObjEvalCtrl_T>::endParameters() const
  {
    return PSpacePoint{1, 1};
  }

  template <typename SpaceObjectBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Plane<SpaceObjectBase_T, PObjEvalCtrl_T>::EvaluationResult
  Plane<SpaceObjectBase_T, PObjEvalCtrl_T>::evaluate(
    const PSpacePoint& par, const PSpaceSizeArray& no_der,
    const PSpaceBoolArray& /*from_left*/) const
  {
    const auto& u        = par[0];
    const auto& v        = par[1];
    const auto& no_der_u = no_der[0];
    const auto& no_der_v = no_der[1];

    EvaluationResult p(no_der_u + 1, no_der_v + 1);

    // S
    p(0, 0) = m_pt + u * m_u + v * m_v;

    if (no_der_u)   // Su
      p(1, 0) = m_u;
    if (no_der_v)   // Sv
      p(0, 1) = m_v;
    if (no_der_u and no_der_v)   // Suv
      p(1, 1) = {0.0, 0.0, 0.0, 0.0};

    return p;
  }





}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_CURVE_PLANE_H
