#ifndef GM2_PARAMETRIC_CURVE_SPHERE_H
#define GM2_PARAMETRIC_CURVE_SPHERE_H


#include "../surface.h"

#include "../../core/coreutils.h"

namespace gmlib2::parametric
{


  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  class Sphere : public Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {

  public:
    using Base = Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructor(s)
    Sphere(Unit radius = Unit{1}) : m_radius{radius} {}

    template <typename... Ts>
    Sphere(Unit radius, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_radius{radius}
    {
    }

    // Members
    Unit m_radius;

    // TPSurf interface
  public:
    PSpaceBoolArray isClosed() const override;
    PSpacePoint     startParameters() const override;
    PSpacePoint     endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& from_left
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override;
  };





  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Sphere<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpaceBoolArray
  Sphere<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::isClosed() const
  {
    return {{true, false}};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Sphere<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Sphere<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::startParameters() const
  {
    return PSpacePoint{0, -M_PI_2};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Sphere<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Sphere<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::endParameters() const
  {
    return PSpacePoint{2 * M_PI, M_PI_2};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Sphere<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::EvaluationResult
  Sphere<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::evaluate(
    const PSpacePoint& par, const PSpaceSizeArray& no_der,
    const PSpaceBoolArray& /*from_left*/) const
  {
    const auto& u        = par[0];
    const auto& v        = par[1];
    const auto& no_der_u = no_der[0];
    const auto& no_der_v = no_der[1];

    EvaluationResult p(no_der_u + 1, no_der_v + 1);

    const Unit r    = m_radius;
    const Unit cu   = std::cos(u);
    const Unit cv   = r * std::cos(v);
    const Unit su   = std::sin(u);
    const Unit sv   = r * std::sin(v);
    const Unit cucv = cu * cv;
    const Unit cusv = cu * sv;
    const Unit sucv = su * cv;
    const Unit susv = su * sv;

    p(0, 0) = {cucv, sucv, sv, 1};                              // S
    if (no_der_u) p(1, 0) = {-sucv, cucv, 0, 0};                // Su
    if (no_der_v) p(0, 1) = {-cusv, -susv, cv, 0};              // Sv
    if (no_der_u and no_der_v) p(1, 1) = {susv, -cusv, 0, 0};   // Suv

    return p;
  }





}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_CURVE_SPHERE_H
