#ifndef GM2_PARAMETRIC_SURFACE_TORUS_H
#define GM2_PARAMETRIC_SURFACE_TORUS_H



#include "../surface.h"

#include "../../core/coreutils.h"


namespace gmlib2::parametric
{

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  class Torus : public Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base = Surface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES




    // Constructor(s)
    Torus(Unit wheelradius = Unit{3.0}, Unit tuberadius1 = Unit{1.0},
           Unit tuberadius2 = Unit{1.0})
      : m_wheelradius{wheelradius}, m_tuberadius1{tuberadius1}, m_tuberadius2{
                                                                  tuberadius2}
    {
    }

    template <typename... Ts>
    Torus(Unit wheelradius, Unit tuberadius1, Unit tuberadius2, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_wheelradius{wheelradius},
        m_tuberadius1{tuberadius1}, m_tuberadius2{tuberadius2}
    {
    }

    // Members
    Unit m_wheelradius;
    Unit m_tuberadius1;
    Unit m_tuberadius2;

    // PSurf interface
  public:
    PSpaceBoolArray isClosed() const override;
    PSpacePoint     startParameters() const override;
    PSpacePoint     endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& from_left
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override;
  };




  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Torus<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::Base::PSpaceBoolArray
  Torus<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::isClosed() const
  {
    return {{true, true}};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Torus<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Torus<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::startParameters() const
  {
    return PSpacePoint{0, 0};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Torus<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  Torus<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::endParameters() const
  {
    return PSpacePoint{2 * M_PI, 2 * M_PI};
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename Torus<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::EvaluationResult
  Torus<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::evaluate(
    const PSpacePoint& par, const PSpaceSizeArray& no_der,
    const PSpaceBoolArray& /*from_left*/) const
  {
    const auto& u        = par[0];
    const auto& v        = par[1];
    const auto& no_der_u = no_der[0];
    const auto& no_der_v = no_der[1];

    EvaluationResult p(no_der_u + 1, no_der_v + 1);

    Unit su   = std::sin(u);
    Unit sv   = std::sin(v);
    Unit cu   = std::cos(u);
    Unit cv   = std::cos(v);
    Unit bcva = m_tuberadius1 * cv + m_wheelradius;
    Unit cucv = m_tuberadius1 * cu * cv;
    Unit cusv = m_tuberadius1 * cu * sv;
    Unit sucv = m_tuberadius1 * su * cv;
    Unit susv = m_tuberadius1 * su * sv;
    sv *= m_tuberadius2;
    cv *= m_tuberadius2;
    cu *= bcva;
    su *= bcva;

    // clang-format off
    p(0, 0) = {cu, su, sv, 1};                                          // S

    if (no_der_u > 0) p(1, 0) = {-su, cu, 0, 0};                        // Su
    if (no_der_v > 0) p(0, 1) = {-cusv, -susv, cv, 0};                  // Sv

    if (no_der_u > 1) p(2, 0) = {-cu, -su, 0, 0};                       // Suu
    if (no_der_v > 1) p(0, 2) = {-cucv, -sucv, -sv, 0};                 // Svv
    if (no_der_u > 0 and no_der_v > 0) p(1, 1) = {susv, -cusv, 0, 0};   // Suv

    if (no_der_u > 2) p(3, 0) = {su, -cu, 0, 0};                        // Suuu
    if (no_der_u > 1 and no_der_v > 0) p(2, 1) = {cusv, susv, 0, 0};    // Suuv
    if (no_der_u > 0 and no_der_v > 1) p(1, 2) = {sucv, cucv, 0, 0};    // Suvv
    if (no_der_v > 2) p(0, 3) = {cusv, susv, -cv, 0};                   // Svvv

    if (no_der_u > 2 and no_der_v > 0) p(3, 1) = {-susv, cusv, 0, 0};   // Suuuv
    if (no_der_u > 1 and no_der_v > 1) p(2, 2) = {cucv, sucv, 0, 0};    // Suuvv
    if (no_der_u > 0 and no_der_v > 2) p(1, 3) = {-susv, cusv, 0, 0};   // Suvvv

    if (no_der_u > 2 and no_der_v > 1) p(3, 2) = {-sucv, cucv, 0, 0};  // Suuuvv
    if (no_der_u > 1 and no_der_v > 2) p(2, 3) = {-cusv, -susv, 0, 0}; // Suuvvv

    if (no_der_u > 2 and no_der_v > 2) p(3, 3) = { susv, cusv, 0, 0}; // Suuuvvv
    // clang-format on

    return p;
  }


}   // namespace gmlib2::parametric


#endif   // GM2_PARAMETRIC_SURFACE_TORUS_H
