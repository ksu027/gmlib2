#ifndef GM2_PARAMETRIC_TRIANGLESURFACE_BEZIERTRIANGLE_H
#define GM2_PARAMETRIC_TRIANGLESURFACE_BEZIERTRIANGLE_H



#include "../trianglesurface.h"
#include "../../core/coreutils.h"

// stl
#include <variant>

namespace gmlib2::parametric
{

  namespace beziertriangle
  {
    template <typename Point_T, typename Unit_T>
    Point_T evaluateLinear(const VectorT<Point_T, 3ul>& c,
                           const VectorT<Unit_T, 3ul>&  bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[0] * u + c[1] * v + c[2] * w;
    }

    template <typename Point_T>
    Point_T evaluateLinearDu(const VectorT<Point_T, 3ul>& c)
    {
      return c[0];
    }

    template <typename Point_T>
    Point_T evaluateLinearDv(const VectorT<Point_T, 3ul>& c)
    {
      return c[1];
    }

    template <typename Point_T>
    Point_T evaluateLinearDw(const VectorT<Point_T, 3ul>& c)
    {
      return c[2];
    }

    template <typename Point_T, typename Unit_T>
    std::tuple<Point_T, Point_T, Point_T, Point_T>
    evaluateLinearFD3(const VectorT<Point_T, 3ul>& c,
                      const VectorT<Unit_T, 3ul>&  bc)
    {
      return {evaluateLinear(c, bc), evaluateLinearDu(c), evaluateLinearDv(c),
              evaluateLinearDw(c)};
    }




    template <typename Point_T, typename Unit_T>
    Point_T evaluateQuadratic(const VectorT<Point_T, 6ul>& c,
                              const VectorT<Unit_T, 3ul>&  bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[0] * (u * u) + c[1] * (2 * u * v) + c[2] * (2 * u * w)
             + c[3] * (v * v) + c[4] * (2 * v * w) + c[5] * (w * w);
    }


    template <typename Point_T, typename Unit_T>
    Point_T evaluateQuadraticDu(const VectorT<Point_T, 6ul>& c,
                                const VectorT<Unit_T, 3ul>&  bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[0] * (2 * u) + c[1] * (2 * v) + c[2] * (2 * w);
    }
    template <typename Point_T, typename Unit_T>
    Point_T evaluateQuadraticDv(const VectorT<Point_T, 6ul>& c,
                                const VectorT<Unit_T, 3ul>&  bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[1] * (2 * u) + c[3] * (2 * v) + c[4] * (2 * w);
    }

    template <typename Point_T, typename Unit_T>
    Point_T evaluateQuadraticDw(const VectorT<Point_T, 6ul>& c,
                                const VectorT<Unit_T, 3ul>&  bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[2] * (2 * u) + c[4] * (2 * v) + c[5] * (2 * w);
    }

    template <typename Point_T, typename Unit_T>
    std::tuple<Point_T, Point_T, Point_T, Point_T>
    evaluateQuadraticFD3(const VectorT<Point_T, 6ul>& c,
                         const VectorT<Unit_T, 3ul>&  bc)
    {
      return {evaluateQuadratic(c, bc), evaluateQuadraticDu(c, bc),
              evaluateQuadraticDv(c, bc), evaluateQuadraticDw(c, bc)};
    }




    template <typename Point_T, typename Unit_T>
    Point_T evaluateCubic(const VectorT<Point_T, 10ul>& c,
                          const VectorT<Unit_T, 3ul>&   bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[0] * (u * u * u) + c[1] * (3 * u * u * v)
             + c[2] * (3 * u * u * w) + c[3] * (3 * u * v * v)
             + c[4] * (6 * u * v * w) + c[5] * (3 * u * w * w)
             + c[6] * (v * v * v) + c[7] * (3 * v * v * w)
             + c[8] * (3 * v * w * w) + c[9] * (w * w * w);
    }

    template <typename Point_T, typename Unit_T>
    Point_T evaluateCubicDu(const VectorT<Point_T, 10ul>& c,
                            const VectorT<Unit_T, 3ul>&   bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[0] * (3 * u * u) + c[1] * (6 * u * v) + c[2] * (6 * u * w)
             + c[3] * (3 * v * v) + c[4] * (6 * v * w) + c[5] * (3 * w * w);
    }

    template <typename Point_T, typename Unit_T>
    Point_T evaluateCubicDv(const VectorT<Point_T, 10ul>& c,
                            const VectorT<Unit_T, 3ul>&   bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[1] * (3 * u * u) + c[3] * (6 * u * v) + c[4] * (6 * u * w)
             + c[6] * (3 * v * v) + c[7] * (6 * v * w) + c[8] * (3 * w * w);
    }

    template <typename Point_T, typename Unit_T>
    Point_T evaluateCubicDw(const VectorT<Point_T, 10ul>& c,
                            const VectorT<Unit_T, 3ul>&   bc)
    {
      const auto u = bc[0];
      const auto v = bc[1];
      const auto w = bc[2];
      return c[2] * (3 * u * u) + c[4] * (6 * u * v) + c[5] * (6 * u * w)
             + c[7] * (3 * v * v) + c[8] * (6 * v * w) + c[9] * (3 * w * w);
    }

    template <typename Point_T, typename Unit_T>
    std::tuple<Point_T, Point_T, Point_T, Point_T>
    evaluateCubicFD3(const VectorT<Point_T, 10ul>& c,
                     const VectorT<Unit_T, 3ul>&   bc)
    {
      return {evaluateCubic(c, bc), evaluateCubicDu(c, bc),
              evaluateCubicDv(c, bc), evaluateCubicDw(c, bc)};
    }


  }   // namespace beziertriangle

  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::TriangleSurfaceEvalCtrl>
  class BezierTriangle
    : public TriangleSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
    using Base = TriangleSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    //    using Vertices   = DVectorT<Point>;
    using Vertices3  = VectorT<Point, 3UL>;
    using Vertices6  = VectorT<Point, 6UL>;
    using Vertices10 = VectorT<Point, 10UL>;
    using Vertices   = std::variant<Vertices3, Vertices6, Vertices10>;

    // Constructor(s)
    template <typename... Ts>
    BezierTriangle(const Vertices& vertices, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_vertices{vertices}
    {
    }


    // Member functions
    const Vertices& vertices() const { return m_vertices; }

    // Member(s)
    Vertices m_vertices;

    // Polygon interface
  public:
    PSpaceBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        false);
    }

    PSpacePoint startParameters() const override
    {

      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(0.0);
    }

    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(1.0);
    }

  protected:
    using EvalTuple = std::tuple<Point, Point, Point, Point>;

    EvaluationResult evaluate(const PSpacePoint&                      par,
                              [[maybe_unused]] const PSpaceSizeArray& no_der
                              = {},
                              [[maybe_unused]] const PSpaceBoolArray& from_left
                              = {}) const override
    {
      // Contextualize input paramters
      const auto u  = par[0];
      const auto v  = par[1];
      const auto w  = Unit(1) - u - v;
      const auto bc = VectorT<Unit, 3ul>{u, v, w};


      // Evaluate
      EvaluationResult p(4UL);
      const auto [S, Su, Sv, Sw]
        = std::visit(utils::overloaded{
                       [bc](Vertices3 c) {
                         return beziertriangle::evaluateLinearFD3(c, bc);
                       },
                       [bc](Vertices6 c) {
                         return beziertriangle::evaluateQuadraticFD3(c, bc);
                       },
                       [bc](Vertices10 c) {
                         return beziertriangle::evaluateCubicFD3(c, bc);
                       },
                     },
                     m_vertices);


      // Fill
      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        S, Unit(1));
      p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        Su, Unit(1));
      p[2] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        Sv, Unit(1));
      p[3] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        Sw, Unit(1));

      return p;
    }
  };

}   // namespace gmlib2::parametric



#endif   // GM2_PARAMETRIC_TRIANGLESURFACE_BEZIERTRIANGLE_H
