#ifndef GM2_PARAMETRIC_TRIANGLESURFACE_TRIANGLE_H
#define GM2_PARAMETRIC_TRIANGLESURFACE_TRIANGLE_H


#include "../trianglesurface.h"
#include "../../core/coreutils.h"

namespace gmlib2::parametric
{
  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::TriangleSurfaceEvalCtrl>
  class Triangle
    : public TriangleSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
    using Base = TriangleSurface<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using Vertices = VectorT<Point, 3UL>;

    // Constructor(s)
    template <typename... Ts>
    Triangle(const Vertices& vertices, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_vertices(vertices)
    {
    }

    const Vertices& vertices() const { return m_vertices; }

    // Member(s)
    Vertices m_vertices;

    // Polygon interface
  public:
    PSpaceBoolArray isClosed() const override
    {
      return utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
        false);
    }

    PSpacePoint startParameters() const override
    {

      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(0.0);
    }

    PSpacePoint endParameters() const override
    {
      return utils::initStaticContainer<PSpacePoint, PSpaceVectorDim>(1.0);
    }

  protected:
    EvaluationResult evaluate(const PSpacePoint&                      par,
                              [[maybe_unused]] const PSpaceSizeArray& no_der
                              = {},
                              [[maybe_unused]] const PSpaceBoolArray& from_left
                              = {}) const override
    {
      const auto u = par[0];
      const auto v = par[1];
      const auto w = Unit(1) - u - v;
//      std::cout << "u: " << u << std::endl;
//      std::cout << "v: " << v << std::endl;
//      std::cout << "w: " << w << std::endl;
//      assert(u <= 1);
//      assert(v <= 1);
//      assert(w <= 1);
//      assert(std::abs(1 - u - v - w) < 1e-5);

      const VectorT<Unit, 3UL> par3{u, v, w};

      // Results
      EvaluationResult p(4UL);

      // S
      p[0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        blaze::inner(m_vertices, par3), Unit(1));

      // Su
      p[1] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        m_vertices[0], Unit(1));

      // Sv
      p[2] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        m_vertices[1], Unit(1));

      // Sw
      p[3] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        m_vertices[2], Unit(1));

      return p;
    }
  };

}   // namespace gmlib2::parametric


#endif   // GM2_PARAMETRIC_TRIANGLESURFACE_TRIANGLE_H
