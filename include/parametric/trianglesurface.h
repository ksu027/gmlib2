#ifndef GM2_PARAMETRIC_TRIANGLESURFACE_H
#define GM2_PARAMETRIC_TRIANGLESURFACE_H


#include "parametricobject.h"

#include "../core/datastructures.h"

namespace gmlib2::parametric
{

  namespace trianglesurface
  {
    namespace algorithms
    {

      inline size_t countTriSamplingPSpacePositions(size_t level)
      {
        size_t r = 0ul;
        for (auto i = level + 1; i > 0; i--) r += i;
        return r;
      }

      inline size_t countTriSamplingFaces(size_t level)
      {
        return size_t(std::pow(level-1, 2));
      }



      inline auto generateTriSamplingFaceIndices(size_t level)
      {

        using TriIdxArray = std::array<size_t,3ul>;
        using TriIndices = std::vector<TriIdxArray>;

        const auto no_tris = countTriSamplingFaces(level);
        TriIndices tris;
        tris.reserve(no_tris);

        for (size_t i = 0; i < level - 1; i++) {

          // Index row i and row i+1
          const size_t o1 = (i * (i + 1)) / 2;
          const size_t o2 = ((i + 1) * (i + 2)) / 2;

          // Upper triangles (pointing down)
          for (size_t j = 1; j <= i; j++)
            tris.push_back({o1 + j, o1 + j - 1, o2 + j});

          // Lower triangles (pointing up)
          for (size_t j = 1; j < i + 2; j++)
            tris.push_back({o2 + j - 1, o2 + j, o1 + j - 1});
        }

        return tris;
      }

    }   // namespace algorithms


    template <typename Object_T>
    auto sample(const Object_T* pobj, size_t no_samples, size_t no_derivatives)
    {
      using SamplingResult = typename Object_T::SamplingResult;
      using Unit           = typename Object_T::Unit;
      using PSpacePoint    = typename Object_T::PSpacePoint;

      Unit u, v;
      Unit du = 1 / Unit(no_samples - 1);

      const auto sum = algorithms::countTriSamplingPSpacePositions(no_samples);

      SamplingResult p;
      p.reserve(sum);

      int i, j, k;
      for (k = 0, i = 0; i < no_samples; i++) {
        for (j = 0; j <= i; j++) {
          v = j * du;
          u = (i - j) * du;
          p.emplace_back(
            pobj->evaluateLocal(PSpacePoint{u, v}, {1ul, 1ul}, {false, false}));
        }
      }

      return p;
    }


  }   // namespace ptrianglesurface

  namespace evaluationctrl
  {

    struct trianglesurface_tag {
    };

    template <typename ParametricObject_T>
    struct TriangleSurfaceEvalCtrl {
      GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_EVALUATIONCTRL_TYPES

      using EvaluationResult
        = datastructures::parametrics::trianglesurface::EvaluationResult<
          VectorH>;
      using SamplingResult
        = datastructures::parametrics::trianglesurface::SamplingResult<VectorH>;

      static auto toPositionH(const EvaluationResult& result)
      {
        return result[0UL];
      }

      static auto toPosition(const EvaluationResult& result)
      {
        return blaze::evaluate(
          blaze::subvector<0UL, ParametricObject_T::VectorDim>(
            toPositionH(result)));
      }
    };

  }   // namespace evaluationctrl


  template <typename SpaceObjectBase_T = ProjectiveSpaceObject<>,
            template <typename> typename TriangleSurfaceEvalCtrl_T
            = evaluationctrl::TriangleSurfaceEvalCtrl>
  using TriangleSurface
    = ParametricObject<spaces::ParameterVSpaceInfo<2>,
                       evaluationctrl::trianglesurface_tag, SpaceObjectBase_T,
                       TriangleSurfaceEvalCtrl_T>;

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_TRIANGLESURFACE_H
