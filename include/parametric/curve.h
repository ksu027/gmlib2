#ifndef GM2_PARAMETRIC_CURVE_H
#define GM2_PARAMETRIC_CURVE_H

#include "parametricobject.h"

#include "../core/datastructures.h"

namespace gmlib2::parametric
{

  namespace curve
  {
    template <typename Object_T>
    auto sample(const Object_T*                           pobj,
                typename Object_T::PSpacePoint const&     par_start,
                typename Object_T::PSpacePoint const&     par_end,
                typename Object_T::PSpaceSizeArray const& no_samples,
                typename Object_T::PSpaceSizeArray const& no_derivatives)
    {

      using SamplingResult = typename Object_T::SamplingResult;
      using Unit           = typename Object_T::Unit;
      using PSpacePoint    = typename Object_T::PSpacePoint;

      const auto m = no_samples[0];
      const auto s = par_start[0];
      const auto e = par_end[0];

      Unit dt = (e - s) / (m - 1);

      SamplingResult p(m);

      for (size_t i = 0; i < m - 1; i++)
        p[i] = pobj->evaluateLocal(PSpacePoint{s + i * dt}, no_derivatives,
                                   {{true}});
      p[m - 1] = pobj->evaluateLocal(par_end, no_derivatives, {{false}});

      return p;
    }

  }   // namespace curve

  namespace evaluationctrl
  {

    struct curve_tag {
    };


    ////////////////////////////////////
    /// Default Parametric Curve sampler
    ///
    template <typename ParametricObject_T>
    struct CurveEvalCtrl {
      GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_EVALUATIONCTRL_TYPES

      using EvaluationResult
        = datastructures::parametrics::curve::EvaluationResult<VectorH>;
      using SamplingResult
        = datastructures::parametrics::curve::SamplingResult<VectorH>;

      static auto toPositionH(const EvaluationResult& result)
      {
        return result[0UL];
      }

      static auto toPosition(const EvaluationResult& result)
      {
        return blaze::evaluate(
          blaze::subvector<0UL, ParametricObject_T::VectorDim>(
            toPositionH(result)));
      }
    };

  }   // namespace evaluationctrl


  template <typename SpaceObjectBase_T = ProjectiveSpaceObject<>,
            template <typename> typename CurveEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  using Curve = ParametricObject<spaces::ParameterVSpaceInfo<1>,
                                  evaluationctrl::curve_tag, SpaceObjectBase_T,
                                  CurveEvalCtrl_T>;

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_CURVE_H
