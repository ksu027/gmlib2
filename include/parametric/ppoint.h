#ifndef GM2_PARAMETRIC_PPOINT_H
#define GM2_PARAMETRIC_PPOINT_H

#include "parametricobject.h"

#include "../core/datastructures.h"

namespace gmlib2::parametric
{

  namespace ppoint
  {
    template <typename Object_T>
    auto sample(const Object_T* obj)
    {
      using SamplingResult = typename Object_T::SamplingResult;
      return SamplingResult(obj->evaluateLocal());
    }

  }   // namespace ppoint

  namespace evaluationctrl
  {

    struct ppoint_tag {
    };


    template <typename ParametricObject_T>
    struct PPointEvalCtrl {
      GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_EVALUATIONCTRL_TYPES

      using EvaluationResult
        = datastructures::parametrics::ppoint::EvaluationResult<VectorH>;
      using SamplingResult
        = datastructures::parametrics::ppoint::SamplingResult<VectorH>;

      static auto toPositionH(const EvaluationResult& result ) {
        return result;
      }

      static auto toPosition(const EvaluationResult& result ) {
        return blaze::evaluate(
          blaze::subvector<0UL, ParametricObject_T::VectorDim>(
            toPositionH(result)));
      }

    };

  }   // namespace evaluationctrl



  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PPointEvalCtrl_T
            = evaluationctrl::PPointEvalCtrl>
  class PPoint
    : public ParametricObject<spaces::ParameterVSpaceInfo<0>,
                              evaluationctrl::ppoint_tag,
                              SpaceObjectEmbedBase_T, PPointEvalCtrl_T> {
    using Base = ParametricObject<spaces::ParameterVSpaceInfo<0>,
                                  evaluationctrl::ppoint_tag,
                                  SpaceObjectEmbedBase_T, PPointEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructors
    using Base::Base;

    // Members
    const VectorH m_pt{0.0, 0.0, 0.0, 1.0};

    // ParameterSpaceObject interface
  public:
    PSpaceBoolArray  isClosed() const final;
    PSpacePoint      startParameters() const final;
    PSpacePoint      endParameters() const final;
    EvaluationResult evaluate(const PSpacePoint&     par    = PSpacePoint(),
                              const PSpaceSizeArray& no_der = PSpaceSizeArray(),
                              const PSpaceBoolArray& from_left
                              = PSpaceBoolArray()) const final;
  };




  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PPointEvalCtrl_T>
  typename PPoint<SpaceObjectEmbedBase_T, PPointEvalCtrl_T>::PSpaceBoolArray
  PPoint<SpaceObjectEmbedBase_T, PPointEvalCtrl_T>::isClosed() const
  {
    return PSpaceBoolArray();
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PPointEvalCtrl_T>
  typename PPoint<SpaceObjectEmbedBase_T, PPointEvalCtrl_T>::PSpacePoint
  PPoint<SpaceObjectEmbedBase_T, PPointEvalCtrl_T>::startParameters() const
  {

    return PSpacePoint();
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PPointEvalCtrl_T>
  typename PPoint<SpaceObjectEmbedBase_T, PPointEvalCtrl_T>::PSpacePoint
  PPoint<SpaceObjectEmbedBase_T, PPointEvalCtrl_T>::endParameters() const
  {
    return PSpacePoint();
  }

  template <typename SpaceObjectEmbedBase_T,
            template <typename> typename PPointEvalCtrl_T>
  typename PPoint<SpaceObjectEmbedBase_T, PPointEvalCtrl_T>::EvaluationResult
  PPoint<SpaceObjectEmbedBase_T, PPointEvalCtrl_T>::evaluate(
    const PSpacePoint&, const PSpaceSizeArray&, const PSpaceBoolArray&) const
  {
    return m_pt;
  }

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_PPOINT_H
