#ifndef GM2_PARAMETRIC_POLYGONSURFACE_H
#define GM2_PARAMETRIC_POLYGONSURFACE_H

#include "parametricobject.h"

#include "../core/polygonutils.h"
#include "../core/datastructures.h"

namespace gmlib2::parametric
{

  namespace polygonsurface
  {

    namespace convex_algorithms
    {

      template <typename Unit_T>
      auto
      countTriSamplingPSpacePositions(size_t                       rings,
                                      DVectorT<VectorT<Unit_T, 2>> polygon_2d)
      {
        const auto N = int(polygon_2d.size());
        const auto M = int(rings);

        size_t no_samples = 0;   // M == 0;
        if (M == 1)
          no_samples = 1;
        else {
          no_samples = 1;
          for (int k = 2; k <= M; ++k) no_samples += N * (k - 1);
        }

        return no_samples;
      }

      template <typename Unit_T>
      auto generateTriSamplingPSpacePositions(
        size_t rings, DVectorT<VectorT<Unit_T, 2>> polygon_2d)
      {
        using PSpaceSamplePoints = std::vector<VectorT<Unit_T, 2>>;

        const auto N = int(polygon_2d.size());
        const auto M = int(rings);

        auto no_samples = countTriSamplingPSpacePositions(rings, polygon_2d);

        PSpaceSamplePoints pspace_p(no_samples);

        auto par
          = DVectorT<DVectorT<Unit_T>>(N, DVectorT<Unit_T>(N, Unit_T(0)));
        for (size_t i = 0; i < N; ++i) {
          par[i][i] = 1.0;
        }

        const auto center          = DVectorT<Unit_T>(N, 1 / Unit_T(N));
        const auto center_eval_par = blaze::inner(polygon_2d, center);

        pspace_p[0] = center_eval_par;

        for (int m = 0; m < M; ++m) {

          const int T = m - 1;

          int r_o;
          if (m == 0)
            r_o = 0;
          else if (m == 1)
            r_o = 1;
          else {
            r_o = 1;
            for (int k = 2; k <= m; ++k) {
              r_o += N * (k - 1);
            }
          }

          for (int n = 0; n < (m == 0 ? 1 : N); ++n) {

            const int t_o = r_o + n * T + n;



            for (int t_i = 0; t_i <= T; ++t_i) {

              const int idx = t_o + t_i;

              const double u = 1.0 - ((1.0 / double(M - 1)) * m);
              const double w = (1.0 / double(M - 1)) * t_i;
              const double v = 1.0 - (u + w);

              const auto poly_par = blaze::evaluate(
                center * u + par[n] * v + par[n == N - 1 ? 0 : n + 1] * w);

              pspace_p[idx] = blaze::inner(polygon_2d, poly_par);
            }
          }
        }

        return pspace_p;
      }


      template <typename Unit_T>
      auto countTriSamplingFaceIndices(size_t                       rings,
                                       DVectorT<VectorT<Unit_T, 2>> polygon_2d)
      {
        const auto N = int(polygon_2d.size());
        const auto M = int(rings);

        if (M == 2UL) return N;

        int no_faces = 1;
        for (int k = 3; k <= M; ++k) no_faces += 1 + (k - 2) * 2;
        no_faces *= N;

        return no_faces;
      }

      template <typename Unit_T>
      auto
      generateTriSamplingFaceIndices(size_t                       rings,
                                     DVectorT<VectorT<Unit_T, 2>> polygon_2d)
      {

        const auto no_tris = countTriSamplingFaceIndices(rings, polygon_2d);

        std::vector<std::array<size_t, 3>> tris(no_tris);

        const auto N = int(polygon_2d.size());
        const auto M = int(rings);

        for (int m = 0; m <= M - 1; ++m) {

          int T_prev = m - 2;
          int T      = m - 1;

          int r_o_prev;
          if (m - 1 == 0)
            r_o_prev = 0;
          else if (m - 1 == 1)
            r_o_prev = 1;
          else {
            r_o_prev = 1;
            for (int k = 2; k <= m - 1; ++k) {
              r_o_prev += N * (k - 1);
            }
          }

          int r_o;
          if (m == 0)
            r_o = 0;
          else if (m == 1)
            r_o = 1;
          else {
            r_o = 1;
            for (int k = 2; k <= m; ++k) {
              r_o += N * (k - 1);
            }
          }

          int f_T = 2 * (m - 1);

          int f_o = 0;
          if (m > 0) {
            for (int k = 0; k <= m - 2; ++k) f_o += N * (2 * (k) + 1);
          }


          for (int n = 0; n < (m == 0 ? 1 : N); ++n) {

            int t_o_prev = r_o_prev + n * T_prev + n;
            int t_o      = r_o + n * T + n;

            int f_t_o = f_o + n * f_T + n;


            for (int t_i = 0; t_i <= T; ++t_i) {

              //          const int idx = t_o + t_i;
              const int f_t_i = f_t_o + t_i * 2;


              if (t_i == T and n == N - 1) {

                const int i0_0 = r_o_prev;
                const int i0_1 = t_o + t_i;
                const int i0_2 = r_o;
                tris[size_t(f_t_i)]
                  = {size_t(i0_0), size_t(i0_1), size_t(i0_2)};
              }
              else if (t_i == T) {

                const int i0_0 = t_o_prev + t_i;
                const int i0_1 = t_o + t_i;
                const int i0_2 = t_o + t_i + 1;
                tris[size_t(f_t_i)]
                  = {size_t(i0_0), size_t(i0_1), size_t(i0_2)};
              }
              else {

                const int i0_0 = t_o_prev + t_i;
                const int i0_1 = t_o + t_i;
                const int i0_2 = t_o + t_i + 1;

                const int i1_0 = t_o_prev + t_i;
                const int i1_1 = t_o + t_i + 1;
                const int i1_2 = t_o_prev + t_i + 1;

                tris[size_t(f_t_i)]
                  = {size_t(i0_0), size_t(i0_1), size_t(i0_2)};
                if (n == N - 1 and t_i == T - 1) {
                  tris[size_t(f_t_i + 1)]
                    = {size_t(i1_0), size_t(i1_1), size_t(r_o_prev)};
                }
                else {

                  tris[size_t(f_t_i + 1)]
                    = {size_t(i1_0), size_t(i1_1), size_t(i1_2)};
                }
              }
            }
          }
        }

        return tris;
      }

    }   // namespace convex_algorithms




    template <typename ParametricObject_T, typename SamplingPoints_T>
    auto
    sample(const ParametricObject_T*                           pobj,
           SamplingPoints_T const&                             sample_positions,
           typename ParametricObject_T::PSpaceSizeArray const& no_derivatives)
    {
      using SamplingResult = typename ParametricObject_T::SamplingResult;

      SamplingResult p;
      p.resize(sample_positions.size());

      std::transform(std::begin(sample_positions), std::end(sample_positions),
                     std::begin(p),
                     [pobj, no_derivatives](const auto& sample_pspace_pos) {
                       return pobj->evaluateLocal(sample_pspace_pos,
                                                  no_derivatives, {true, true});
                     });

      return p;
    }


    template <typename ParametricObject_T>
    auto debugSampleLambdas(
      const ParametricObject_T*                          pobj,
      const typename ParametricObject_T::SamplingPoints& sample_positions)
    {
      using DebugLambdas = typename ParametricObject_T::DebugLambdas;

      DebugLambdas p;
      p.resize(sample_positions.size());

      std::transform(std::begin(sample_positions), std::end(sample_positions),
                     std::begin(p), [pobj](const auto& sample_pspace_pos) {
                       return pobj->debugEvaluateLambda(sample_pspace_pos);
                     });
      return p;
    }

    template <typename ParametricObject_T>
    auto debugSampleSs(
      const ParametricObject_T*                          pobj,
      const typename ParametricObject_T::SamplingPoints& sample_positions)
    {
      using DebugSs = typename ParametricObject_T::DebugSs;

      DebugSs p;
      p.resize(sample_positions.size());

      std::transform(std::begin(sample_positions), std::end(sample_positions),
                     std::begin(p), [pobj](const auto& sample_pspace_pos) {
                       return pobj->debugEvaluateS(sample_pspace_pos);
                     });
      return p;
    }

    template <typename ParametricObject_T>
    auto debugSampleHs(
      const ParametricObject_T*                          pobj,
      const typename ParametricObject_T::SamplingPoints& sample_positions)
    {
      using DebugHs = typename ParametricObject_T::DebugHs;

      DebugHs p;
      p.resize(sample_positions.size());

      std::transform(std::begin(sample_positions), std::end(sample_positions),
                     std::begin(p), [pobj](const auto& sample_pspace_pos) {
                       return pobj->debugEvaluateH(sample_pspace_pos);
                     });
      return p;
    }




  }   // namespace ppolygonsurface

  namespace evaluationctrl
  {

    struct polygonsurface_tag {
    };

    template <typename ParametricObject_T>
    struct PolygonSurfaceEvalCtrl {
      GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_EVALUATIONCTRL_TYPES


      using EvaluationResult
        = datastructures::parametrics::polygonsurface::EvaluationResult<
          VectorH>;

      using SamplingResult
        = datastructures::parametrics::polygonsurface::SamplingResult<VectorH>;

      using SamplingPoints
        = datastructures::parametrics::polygonsurface::SamplingPoints<
          PSpacePoint>;

      static auto toPositionH(const EvaluationResult& result)
      {
        return result(0UL, 0UL);
      }

      static auto toPosition(const EvaluationResult& result)
      {
        return blaze::evaluate(
          blaze::subvector<0UL, ParametricObject_T::VectorDim>(
            toPositionH(result)));
      }
    };

  }   // namespace evaluationctrl




  template <typename SpaceObjectBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PolygonSurfaceEvalCtrl_T
            = evaluationctrl::PolygonSurfaceEvalCtrl>
  class PolygonSurface
    : public ParametricObject<spaces::ParameterVSpaceInfo<2>,
                              evaluationctrl::polygonsurface_tag,
                              SpaceObjectBase_T, PolygonSurfaceEvalCtrl_T> {

    using Base = ParametricObject<spaces::ParameterVSpaceInfo<2>,
                                  evaluationctrl::polygonsurface_tag,
                                  SpaceObjectBase_T, PolygonSurfaceEvalCtrl_T>;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Polygon construction types
    using SamplingPoints = typename PObjEvalCtrl::SamplingPoints;

  protected:
    template <typename Element_T>
    using PolygonContainer = DVectorT<Element_T>;

  public:
    using Lambda    = PolygonContainer<PSpaceUnit>;
    using Lambdas   = PolygonContainer<Lambda>;
    using Polygon2D = PolygonContainer<PSpacePoint>;


    const Polygon2D& polygon2D() const { return m_polygon; }
    size_t           sides() const { return m_polygon.size(); }

    template <typename... Ts>
    PolygonSurface(Polygon2D&& polygon, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_polygon(std::forward<Polygon2D>(polygon))
    {
    }

    template <typename... Ts>
    PolygonSurface(size_t N, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_polygon(polygonutils::generateRegularPolygon2D<>(N))
    {
    }

    // Member(s)
  public:
    Polygon2D m_polygon;


    using DebugS = std::vector<PSpaceUnit>;
    using DebugH = std::vector<PSpaceUnit>;

    using DebugLambdas = std::vector<Lambda>;
    using DebugSs      = std::vector<DebugS>;
    using DebugHs      = std::vector<DebugH>;


    virtual Lambda debugEvaluateLambda(const PSpacePoint&) const
    {
      return Lambda();
    }
    virtual DebugS debugEvaluateS(const PSpacePoint&) const { return DebugS(); }
    virtual DebugH debugEvaluateH(const PSpacePoint&) const { return DebugH(); }

    // Allocator utilities
  protected:
    template <typename Element_T>
    auto allocPolygonContainer(const Element_T& default_value
                               = Element_T()) const
    {
      return PolygonContainer<Element_T>(sides(), default_value);
    }

    Polygon2D allocPolygon2D(const PSpacePoint& default_value
                             = PSpacePoint()) const
    {
      return allocPolygonContainer<PSpacePoint>(default_value);
    }
    Lambda allocLambda(const PSpaceUnit& default_value = PSpaceUnit()) const
    {
      return allocPolygonContainer<PSpaceUnit>(default_value);
    }
    Lambdas allocLambdas(const Lambda& default_value) const
    {
      return allocPolygonContainer<Lambda>(default_value);
    }
  };

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_POLYGONSURFACE_H
