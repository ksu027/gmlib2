#ifndef GM2_PARAMETRIC_VOLUME_BEZIERVOLUME_H
#define GM2_PARAMETRIC_VOLUME_BEZIERVOLUME_H


#include "../volume.h"

#include "../../core/coreutils.h"
#include "../../core/basisgenerators/bernsteinbasisgenerators.h"

// stl
#include <vector>

namespace gmlib2::parametric
{
  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::VolumeEvalCtrl>
  class BezierVolume : public Volume<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base = Volume<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using ControlNet = std::vector<std::vector<std::vector<Point>>>;

    // Constructor(s)
    template <typename... Ts>
    BezierVolume(const ControlNet& C, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...), m_C{C}
    {
    }

    // Members
    ControlNet m_C;

    PSpaceBoolArray isClosed() const override
    {
      return {{false, false, false}};
    }
    PSpacePoint startParameters() const override
    {
      return PSpacePoint{0, 0, 0};
    }
    PSpacePoint endParameters() const override { return PSpacePoint{1, 1, 1}; }

    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/) const override
    {
      const auto& u        = par[0];
      const auto& v        = par[1];
      const auto& w        = par[2];
      const auto& no_der_u = no_der[0];
      const auto& no_der_v = no_der[1];
      const auto& no_der_w = no_der[2];

      EvaluationResult p;
      p.resize(no_der_u + 1);
      for (auto& pi : p) {
        pi.resize(no_der_v + 1);
        for (auto& pii : pi) pii.resize(no_der_w + 1);
      }

      const auto Bu
        = basis::generateBernsteinBasisMatrix(int(m_C.size() - 1), u);
      const auto Bv
        = basis::generateBernsteinBasisMatrix(int(m_C[0].size() - 1), v);
      const auto Bw
        = basis::generateBernsteinBasisMatrix(int(m_C[0][0].size() - 1), w);

      Point res(0);

      for (auto i = 0UL; i < m_C.size(); ++i) {

        const auto& Ci = m_C[i];
        for (auto j = 0UL; j < Ci.size(); ++j) {

          const auto& Cii = Ci[j];
          for (auto k = 0UL; k < Cii.size(); ++k)
            res += m_C[i][j][k] * blaze::row(Bw, 0)[k] * blaze::row(Bv, 0)[j]
                   * blaze::row(Bu, 0)[i];
        }
      }


      // S
      p[0][0][0] = utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
        res, 1.0);

      return p;
    }
  };

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_VOLUME_BEZIERVOLUME_H
