#ifndef GM2_PARAMETRIC_VOLUME_CUBOID_H
#define GM2_PARAMETRIC_VOLUME_CUBOID_H


#include "../volume.h"

#include "../../core/coreutils.h"

namespace gmlib2::parametric
{
  template <typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::VolumeEvalCtrl>
  class Cuboid : public Volume<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base = Volume<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES


    // Constructor(s)
  public:
    Cuboid(const Point&  origin   = Point{0.0, 0.0, 0.0},
            const Vector& u_vector = Vector{1.0, 0.0, 0.0},
            const Vector& v_vector = Vector{0.0, 1.0, 0.0},
            const Vector& w_vector = Vector{0.0, 0.0, 1.0})
      : m_pt{gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim,
                                                  1UL>(origin, 1.0)},
        m_u{
          gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
            u_vector, 0.0)},
        m_v{
          gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
            v_vector, 0.0)},
        m_w{
          gmlib2::utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
            w_vector, 0.0)}
    {
    }

    template <typename... Ts>
    Cuboid(const Point& origin, const Vector& u_vector, const Vector& v_vector,
            const Vector& w_vector, Ts&&... ts)
      : Base(std::forward<Ts>(ts)...),
        m_pt{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          origin, 1.0)},
        m_u{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          u_vector, 0.0)},
        m_v{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          v_vector, 0.0)},
        m_w{utils::extendStaticContainer<Point, VectorH, VectorDim, 1UL>(
          w_vector, 0.0)}
    {
    }

    // Members
    VectorH m_pt;
    VectorH m_u;
    VectorH m_v;
    VectorH m_w;

    PSpaceBoolArray isClosed() const override
    {
      return {{false, false, false}};
    }
    PSpacePoint startParameters() const override
    {
      return PSpacePoint{0, 0, 0};
    }
    PSpacePoint endParameters() const override { return PSpacePoint{1, 1, 1}; }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& /*from_left*/) const override
    {
      const auto& u        = par[0];
      const auto& v        = par[1];
      const auto& w        = par[2];
      const auto& no_der_u = no_der[0];
      const auto& no_der_v = no_der[1];
      const auto& no_der_w = no_der[2];

      EvaluationResult p;
      p.resize(no_der_u + 1);
      for (auto& pi : p) {
        pi.resize(no_der_v + 1);
        for (auto& pii : pi) pii.resize(no_der_w + 1);
      }

      // S
      p[0][0][0] = m_pt + u * m_u + v * m_v + w * m_w;

      // Vu
      if (no_der_u) p[1][0][0] = m_u;
      // Vv
      if (no_der_v) p[0][1][0] = m_v;
      // Vw
      if (no_der_v) p[0][0][1] = m_w;

      return p;
    }
  };

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_VOLUME_CUBOID_H
