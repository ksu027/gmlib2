#ifndef GM2_PARAMETRIC_VOLUME_H
#define GM2_PARAMETRIC_VOLUME_H

#include "parametricobject.h"

namespace gmlib2::parametric
{

  namespace volume
  {

    template <typename Volume_T>
    auto sample(const Volume_T*                           pobj,
                typename Volume_T::PSpacePoint const&     par_start,
                typename Volume_T::PSpacePoint const&     par_end,
                typename Volume_T::PSpaceSizeArray const& no_samples,
                typename Volume_T::PSpaceSizeArray const& no_derivatives)
    {
      using SamplingResult   = typename Volume_T::SamplingResult;
      using EvaluationResult = typename Volume_T::EvaluationResult;
      using PObjEvalCtrl     = typename Volume_T::PObjEvalCtrl;
      using Unit             = typename Volume_T::Unit;
      using DVector1DT =
        typename PObjEvalCtrl::template DVector1DT<EvaluationResult>;
      using DVector2DT =
        typename PObjEvalCtrl::template DVector2DT<EvaluationResult>;

      const auto m1  = no_samples[0];
      const auto m2  = no_samples[1];
      const auto m3  = no_samples[2];
      const auto s_u = par_start[0];
      const auto s_v = par_start[1];
      const auto s_w = par_start[2];
      const auto e_u = par_end[0];
      const auto e_v = par_end[1];
      const auto e_w = par_end[2];

      Unit du = (e_u - s_u) / (m1 - 1);
      Unit dv = (e_v - s_v) / (m2 - 1);
      Unit dw = (e_w - s_w) / (m3 - 1);

      SamplingResult p(m1, DVector2DT(m2, DVector1DT(m3)));

      return p;
    }
  }   // namespace volume

  namespace evaluationctrl
  {

    struct volume_tag {
    };

    template <typename ParametricObject_T>
    struct VolumeEvalCtrl {
      GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_EVALUATIONCTRL_TYPES

      template <typename Ele_T>
      using DVector1DT = gmlib2::DVectorT<Ele_T>;

      template <typename Ele_T>
      using DVector2DT = gmlib2::DVectorT<DVector1DT<Ele_T>>;

      template <typename Ele_T>
      using DVector3DT = gmlib2::DVectorT<DVector2DT<Ele_T>>;

      using EvaluationResult = DVector3DT<VectorH>;
      using SamplingResult   = DVector3DT<EvaluationResult>;


      static auto toPositionH(const EvaluationResult& result)
      {
        return result[0UL][0UL][0UL];
      }

      static auto toPosition(const EvaluationResult& result)
      {
        return blaze::evaluate(
          blaze::subvector<0UL, ParametricObject_T::VectorDim>(
            toPositionH(result)));
      }
    };

  }   // namespace evaluationctrl

  template <typename SpaceObjectBase_T = ProjectiveSpaceObject<>,
            template <typename> typename VolumeEvalCtrl_T
            = evaluationctrl::VolumeEvalCtrl>
  using Volume = ParametricObject<spaces::ParameterVSpaceInfo<3>,
                                   evaluationctrl::volume_tag,
                                   SpaceObjectBase_T, VolumeEvalCtrl_T>;

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_VOLUME_H
