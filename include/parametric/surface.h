#ifndef GM2_PARAMETRIC_SURFACE_H
#define GM2_PARAMETRIC_SURFACE_H

#include "parametricobject.h"

#include "../core/datastructures.h"

namespace gmlib2::parametric
{

  namespace surface
  {
    template <typename Object_T>
    auto sample(const Object_T*                           pobj,
                typename Object_T::PSpacePoint const&     par_start,
                typename Object_T::PSpacePoint const&     par_end,
                typename Object_T::PSpaceSizeArray const& no_samples,
                typename Object_T::PSpaceSizeArray const& no_derivatives)
    {
      using SamplingResult = typename Object_T::SamplingResult;
      using Unit           = typename Object_T::Unit;
      using PSpacePoint    = typename Object_T::PSpacePoint;

      const auto m1  = no_samples[0];
      const auto m2  = no_samples[1];
      const auto s_u = par_start[0];
      const auto s_v = par_start[1];
      const auto e_u = par_end[0];
      const auto e_v = par_end[1];

      Unit du = (e_u - s_u) / (m1 - 1);
      Unit dv = (e_v - s_v) / (m2 - 1);

      SamplingResult p(m1, m2);

      for (size_t i = 0; i < m1 - 1; i++) {
        Unit u = s_u + i * du;
        for (size_t j = 0; j < m2 - 1; j++) {
          p(i, j) = pobj->evaluateLocal(PSpacePoint{u, s_v + j * dv},
                                        no_derivatives, {{true, true}});
        }
        p(i, m2 - 1) = pobj->evaluateLocal(PSpacePoint{u, e_v}, no_derivatives,
                                           {{true, false}});
      }

      for (size_t j = 0; j < m2 - 1; j++) {
        p(m1 - 1, j) = pobj->evaluateLocal(PSpacePoint{e_u, s_v + j * dv},
                                           no_derivatives, {{false, true}});
      }

      p(m1 - 1, m2 - 1) = pobj->evaluateLocal(PSpacePoint{e_u, e_v},
                                              no_derivatives, {{false, false}});

      return p;
    }

    template <typename Object_T>
    auto samplePart(const Object_T*                           pobj,
                    typename Object_T::PSpaceSizeArray const& sample_nr,
                    typename Object_T::PSpacePoint const&     par_start,
                    typename Object_T::PSpacePoint const&     par_end,
                    typename Object_T::PSpaceSizeArray const& no_samples,
                    typename Object_T::PSpaceSizeArray const& no_derivatives)
    {
      using EvaluationResult = typename Object_T::EvaluationResult;
      using Unit             = typename Object_T::Unit;
      using PSpacePoint      = typename Object_T::PSpacePoint;

      const auto [m1, m2]         = no_samples;
      const auto s_u = par_start[0];
      const auto s_v = par_start[1];
      const auto e_u = par_end[0];
      const auto e_v = par_end[1];
      const auto [s_nr_u, s_nr_v] = sample_nr;

      const auto du = (e_u - s_u) / (m1 - 1);
      const auto dv = (e_v - s_v) / (m2 - 1);

      const auto left_u = s_nr_u == m1 - 1 ? true : false;
      const auto left_v = s_nr_v == m2 - 1 ? true : false;

      const Unit u = du * s_nr_u;
      const Unit v = dv * s_nr_v;

      EvaluationResult p = pobj->evaluateLocal(
        PSpacePoint{u, v}, no_derivatives, {{left_u, left_v}});
      return p;
    }

  }   // namespace surface

  namespace evaluationctrl
  {

    struct surface_tag {
    };

    template <typename ParametricObject_T>
    struct SurfaceEvalCtrl {
      GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_EVALUATIONCTRL_TYPES

      using EvaluationResult
        = datastructures::parametrics::surface::EvaluationResult<VectorH>;
      using SamplingResult
        = datastructures::parametrics::surface::SamplingResult<VectorH>;

      static auto toPositionH(const EvaluationResult& result)
      {
        return result(0UL, 0UL);
      }

      static auto toPosition(const EvaluationResult& result)
      {
        return blaze::evaluate(
          blaze::subvector<0UL, ParametricObject_T::VectorDim>(
            toPositionH(result)));
      }
    };

  }   // namespace evaluationctrl


  template <typename SpaceObjectBase_T = ProjectiveSpaceObject<>,
            template <typename> typename SurfaceEvalCtrl_T
            = evaluationctrl::SurfaceEvalCtrl>
  using Surface = ParametricObject<spaces::ParameterVSpaceInfo<2>,
                                    evaluationctrl::surface_tag,
                                    SpaceObjectBase_T, SurfaceEvalCtrl_T>;

}   // namespace gmlib2::parametric

#endif   // GM2_PARAMETRIC_SURFACE_H
