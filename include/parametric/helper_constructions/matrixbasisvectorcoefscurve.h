#ifndef GM2_PARAMETRIC_HELPER_CONSTRUCTIONS_MATRIXBASISVECTORCOEFSCURVE_H
#define GM2_PARAMETRIC_HELPER_CONSTRUCTIONS_MATRIXBASISVECTORCOEFSCURVE_H


#include "../curve.h"
#include "../../core/coreutils.h"


namespace gmlib2::parametric
{


  template <typename BasisMatrixGenerator_T,
            typename SpaceObjectEmbedBase_T = ProjectiveSpaceObject<>,
            template <typename> typename PObjEvalCtrl_T
            = evaluationctrl::CurveEvalCtrl>
  class MatrixBasisVectorCoefsCurve
    : public   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T> {
  public:
    using Base =   Curve<SpaceObjectEmbedBase_T, PObjEvalCtrl_T>;

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructor
    template <typename... Coef_Ts>
    MatrixBasisVectorCoefsCurve(const Coef_Ts&... coefs)
    {
      static_assert(sizeof...(coefs) == BasisMatrixGenerator_T::Degree);
      m_c = {coefs...};
    }

    // Members
    ColVectorT<Vector, BasisMatrixGenerator_T::Degree> m_c;
    BasisMatrixGenerator_T                             m_M{};

    //   Curve interface
  public:
    PSpaceBoolArray isClosed() const override;
    PSpacePoint     startParameters() const override;
    PSpacePoint     endParameters() const override;

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_der,
             const PSpaceBoolArray& from_left
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override;
  };

  template <typename BasisMatrixGenerator_T, typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename MatrixBasisVectorCoefsCurve<BasisMatrixGenerator_T,
                                        SpaceObjectEmbedBase_T,
                                        PObjEvalCtrl_T>::PSpaceBoolArray
  MatrixBasisVectorCoefsCurve<BasisMatrixGenerator_T, SpaceObjectEmbedBase_T,
                               PObjEvalCtrl_T>::isClosed() const
  {
    return {{false}};
  }

  template <typename BasisMatrixGenerator_T, typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename MatrixBasisVectorCoefsCurve<
    BasisMatrixGenerator_T, SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  MatrixBasisVectorCoefsCurve<BasisMatrixGenerator_T, SpaceObjectEmbedBase_T,
                               PObjEvalCtrl_T>::startParameters() const
  {
    return PSpacePoint{0};
  }

  template <typename BasisMatrixGenerator_T, typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename MatrixBasisVectorCoefsCurve<
    BasisMatrixGenerator_T, SpaceObjectEmbedBase_T, PObjEvalCtrl_T>::PSpacePoint
  MatrixBasisVectorCoefsCurve<BasisMatrixGenerator_T, SpaceObjectEmbedBase_T,
                               PObjEvalCtrl_T>::endParameters() const
  {
    return PSpacePoint{1};
  }




  template <typename BasisMatrixGenerator_T, typename SpaceObjectEmbedBase_T,
            template <typename> typename PObjEvalCtrl_T>
  typename MatrixBasisVectorCoefsCurve<BasisMatrixGenerator_T,
                                        SpaceObjectEmbedBase_T,
                                        PObjEvalCtrl_T>::EvaluationResult
  MatrixBasisVectorCoefsCurve<
    BasisMatrixGenerator_T, SpaceObjectEmbedBase_T,
    PObjEvalCtrl_T>::evaluate(const PSpacePoint&     par,
                              const PSpaceSizeArray& no_der,
                              const PSpaceBoolArray& /*from_left*/) const
  {
    const auto& t              = par[0];
    const auto& no_derivatives = no_der[0];

    EvaluationResult p(no_derivatives + 1);

    auto p_H = m_M(t) * m_c;

    p[0] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(p_H[0],
                                                                         1.0);
    if (no_derivatives)
      p[1] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
        p_H[1], 0.0);
    if (no_derivatives > 1)
      p[2] = utils::extendStaticContainer<Vector, VectorH, VectorDim, 1UL>(
        p_H[2], 0.0);

    return p;
  }

};   // namespace gmlib2::parametric


#endif // GM2_PARAMETRIC_HELPER_CONSTRUCTIONS_MATRIXBASISVECTORCOEFSCURVE_H
