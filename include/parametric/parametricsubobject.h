#ifndef GM_PSUBSPACEOBJECT_H
#define GM_PSUBSPACEOBJECT_H



#include "curve.h"
#include "parametricobject.h"
#include "../core/coreutils.h"

// stl
#include <type_traits>
#include <iostream>


#if 1
#define GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES           \
  /*********************************************/                              \
  /* Presumes the following                   **/                              \
  /* template parameter:                      **/                              \
  /*                                          **/                              \
  /* ParametricSubObject_T                    **/                              \
  /*********************************************/                              \
                                                                               \
  /*******************/                                                        \
  /* SubObject Types */                                                        \
  using SubParametricObject = SubParametricObject_T;                           \
  using PSubObjEvalCtrl     = typename SubParametricObject::PObjEvalCtrl;      \
  using EvaluationResult    = typename PSubObjEvalCtrl::EvaluationResult;      \
                                                                               \
  /***********************/ /* PSpace Object Types */                          \
  using PSpaceObject   = typename SubParametricObject::PSpaceObject;           \
  using PSpaceEvalCtrl = typename PSpaceObject::PObjEvalCtrl;                  \
                                                                               \
  /***************************/ /* Parametric Object Types */                  \
  using PObject = typename SubParametricObject::PObject;     \
  using PObjEvalCtrl     = typename PObject::PObjEvalCtrl;


#else
#define GM2_DEFINE_DEFAULT_PARAMETRIC_SUBOBJECT_EVALUATIONCTRL_TYPES
#endif



namespace gmlib2::parametric
{


  template <
    template <typename, template <typename> typename> typename PSpaceObject_T,
    typename PObject_T, template <typename> typename PSubObjEvalCtrl_T,
    template <typename> typename PSpaceObject_PObjEvalCtrl_T,
    template <typename> typename PSubObj_PObjEvalCtrl_T,
    typename BaseSpaceObjectBase_T =
      typename PObject_T::BaseSpaceObjectBase,
    template <typename> typename PSpaceObject_SpaceObject_T
    = ProjectiveSpaceObject>
  class ParametricSubObject
    : public ParametricObject<
        typename PSpaceObject_T<
          PSpaceObject_SpaceObject_T<typename PObject_T::PSpace>,
          PSubObj_PObjEvalCtrl_T>::PSpaceInfo,
        typename PSpaceObject_T<
          PSpaceObject_SpaceObject_T<typename PObject_T::PSpace>,
          PSubObj_PObjEvalCtrl_T>::ParametricObjectType_Tag,
        BaseSpaceObjectBase_T, PSubObj_PObjEvalCtrl_T> {

  public:
    using Base = ParametricObject<
      typename PSpaceObject_T<
        PSpaceObject_SpaceObject_T<typename PObject_T::PSpace>,
        PSubObj_PObjEvalCtrl_T>::PSpaceInfo,
      typename PSpaceObject_T<
        PSpaceObject_SpaceObject_T<typename PObject_T::PSpace>,
        PSubObj_PObjEvalCtrl_T>::ParametricObjectType_Tag,
      BaseSpaceObjectBase_T, PSubObj_PObjEvalCtrl_T>;










    /////////////////////////////
    ///                       ///
    ///   Parametric Object   ///
    ///                       ///
    /////////////////////////////


    /////////////////
    // Embedded space
    using PObject              = PObject_T;
    using PObject_EmbedSpaceObject = PObject;
    using PObject_EmbedSpaceInfo =
      typename PObject_EmbedSpaceObject::EmbedSpaceInfo;
    using PObject_EmbedSpace =
      typename PObject_EmbedSpaceObject::EmbedSpace;

    // Dimensions
    static constexpr auto PObject_FrameDim
      = PObject_EmbedSpace::FrameDim;
    static constexpr auto PObject_VectorDim
      = PObject_EmbedSpace::VectorDim;
    static constexpr auto PObject_ASFrameDim
      = PObject_EmbedSpace::ASFrameDim;
    static constexpr auto PObject_VectorHDim
      = PObject_EmbedSpace::VectorHDim;

    // Embedded space types
    using PObject_Unit = typename PObject_EmbedSpace::Unit;

    // Embedded space types: vector space
    using PObject_Vector =
      typename PObject_EmbedSpace::Vector;
    using PObject_Frame = typename PObject_EmbedSpace::Frame;

    // Embedded space types: affine space
    using PObject_Point = typename PObject_EmbedSpace::Point;
    using PObject_ASFrame =
      typename PObject_EmbedSpace::ASFrame;

    // Embedded space types: projective space
    using PObject_PointH =
      typename PObject_EmbedSpace::PointH;
    using PObject_VectorH =
      typename PObject_EmbedSpace::VectorH;
    using PObject_ASFrameH =
      typename PObject_EmbedSpace::ASFrameH;


    ///////////////////
    // Parametric space
    using PObject_PSpaceInfo =
      typename PObject_EmbedSpaceObject::PSpaceInfo;
    using PObject_PSpace =
      typename PObject_EmbedSpaceObject::PSpace;

    // Dimensions
    static constexpr auto PObject_PSpaceFrameDim
      = PObject_PSpace::FrameDim;
    static constexpr auto PObject_PSpaceVectorDim
      = PObject_PSpace::VectorDim;
    static constexpr auto PObject_PSpaceASFrameDim
      = PObject_PSpace::ASFrameDim;
    static constexpr auto PObject_PSpaceVectorHDim
      = PObject_PSpace::VectorHDim;

    // Embedded space types
    using PObject_PSpaceUnit = typename PObject_PSpace::Unit;

    // Embedded space types: vector space
    using PObject_PSpaceVector =
      typename PObject_PSpace::Vector;
    using PObject_PSpaceFrame =
      typename PObject_PSpace::Frame;

    // Embedded space types: affine space
    using PObject_PSpacePoint =
      typename PObject_PSpace::Point;
    using PObject_PSpaceASFrame =
      typename PObject_PSpace::ASFrame;

    // Embedded space types: projective space
    using PObject_PSpacePointH =
      typename PObject_PSpace::PointH;
    using PObject_PSpaceVectorH =
      typename PObject_PSpace::VectorH;
    using PObject_PSpaceASFrameH =
      typename PObject_PSpace::ASFrameH;


    ///////////////////////
    // Paramter space types
    using PObject_PSpaceSizeArray =
      typename PObject_EmbedSpaceObject::PSpaceSizeArray;
    using PObject_PSpaceBoolArray =
      typename PObject_EmbedSpaceObject::PSpaceBoolArray;


    //////////
    // PObjEvalCtrl
    using PObject_PObjEvalCtrl =
      typename PObject_EmbedSpaceObject::PObjEvalCtrl;
    using PObject_EvaluationResult =
      typename PObject_EmbedSpaceObject::EvaluationResult;
    using PObject_SamplingResult =
      typename PObject_EmbedSpaceObject::SamplingResult;







    /////////////////////////
    ///                   ///
    ///   PSpace Object   ///
    ///                   ///
    /////////////////////////


    /////////////////
    // Embedded space
    using PSpaceObject_SpaceObject
      = PSpaceObject_SpaceObject_T<typename PObject::PSpace>;
    using PSpaceObject
      = PSpaceObject_T<PSpaceObject_SpaceObject, PSpaceObject_PObjEvalCtrl_T>;
    using PSpaceObject_EmbedSpaceObject = PSpaceObject;
    using PSpaceObject_EmbedSpaceInfo =
      typename PSpaceObject_EmbedSpaceObject::EmbedSpaceInfo;
    using PSpaceObject_EmbedSpace =
      typename PSpaceObject_EmbedSpaceObject::EmbedSpace;

    // Dimensions
    static constexpr auto PSpaceObject_FrameDim
      = PSpaceObject_EmbedSpace::FrameDim;
    static constexpr auto PSpaceObject_VectorDim
      = PSpaceObject_EmbedSpace::VectorDim;
    static constexpr auto PSpaceObject_ASFrameDim
      = PSpaceObject_EmbedSpace::ASFrameDim;
    static constexpr auto PSpaceObject_VectorHDim
      = PSpaceObject_EmbedSpace::VectorHDim;

    // Embedded space types
    using PSpaceObject_Unit = typename PSpaceObject_EmbedSpace::Unit;

    // Embedded space types: vector space
    using PSpaceObject_Vector = typename PSpaceObject_EmbedSpace::Vector;
    using PSpaceObject_Frame  = typename PSpaceObject_EmbedSpace::Frame;

    // Embedded space types: affine space
    using PSpaceObject_Point   = typename PSpaceObject_EmbedSpace::Point;
    using PSpaceObject_ASFrame = typename PSpaceObject_EmbedSpace::ASFrame;

    // Embedded space types: projective space
    using PSpaceObject_PointH   = typename PSpaceObject_EmbedSpace::PointH;
    using PSpaceObject_VectorH  = typename PSpaceObject_EmbedSpace::VectorH;
    using PSpaceObject_ASFrameH = typename PSpaceObject_EmbedSpace::ASFrameH;


    ///////////////////
    // Parametric space
    using PSpaceObject_PSpaceInfo =
      typename PSpaceObject_EmbedSpaceObject::PSpaceInfo;
    using PSpaceObject_PSpace = typename PSpaceObject_EmbedSpaceObject::PSpace;

    // Dimensions
    static constexpr auto PSpaceObject_PSpaceFrameDim
      = PSpaceObject_PSpace::FrameDim;
    static constexpr auto PSpaceObject_PSpaceVectorDim
      = PSpaceObject_PSpace::VectorDim;
    static constexpr auto PSpaceObject_PSpaceASFrameDim
      = PSpaceObject_PSpace::ASFrameDim;
    static constexpr auto PSpaceObject_PSpaceVectorHDim
      = PSpaceObject_PSpace::VectorHDim;

    // Embedded space types
    using PSpaceObject_PSpaceUnit = typename PSpaceObject_PSpace::Unit;

    // Embedded space types: vector space
    using PSpaceObject_PSpaceVector = typename PSpaceObject_PSpace::Vector;
    using PSpaceObject_PSpaceFrame  = typename PSpaceObject_PSpace::Frame;

    // Embedded space types: affine space
    using PSpaceObject_PSpacePoint   = typename PSpaceObject_PSpace::Point;
    using PSpaceObject_PSpaceASFrame = typename PSpaceObject_PSpace::ASFrame;

    // Embedded space types: projective space
    using PSpaceObject_PSpacePointH   = typename PSpaceObject_PSpace::PointH;
    using PSpaceObject_PSpaceVectorH  = typename PSpaceObject_PSpace::VectorH;
    using PSpaceObject_PSpaceASFrameH = typename PSpaceObject_PSpace::ASFrameH;


    ///////////////////////
    // Paramter space types
    using PSpaceObject_PSpaceSizeArray =
      typename PSpaceObject_EmbedSpaceObject::PSpaceSizeArray;
    using PSpaceObject_PSpaceBoolArray =
      typename PSpaceObject_EmbedSpaceObject::PSpaceBoolArray;


    //////////
    // PObjEvalCtrl
    using PSpaceObject_PObjEvalCtrl =
      typename PSpaceObject_EmbedSpaceObject::PObjEvalCtrl;
    using PSpaceObject_EvaluationResult =
      typename PSpaceObject_EmbedSpaceObject::EvaluationResult;
    using PSpaceObject_SamplingResult =
      typename PSpaceObject_EmbedSpaceObject::SamplingResult;







    /////////////////////////////
    ///                       ///
    ///   PSpace Object       ///
    ///   on                  ///
    ///   Parametric Object   ///
    ///                       ///
    /////////////////////////////

    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    using PSubObjEvalCtrl = PSubObjEvalCtrl_T<ParametricSubObject>;



    // Constructor
    template <typename... Ts>
    ParametricSubObject(PObject* parametric_object, Ts&&... ts)
      : Base(), m_pspace_object(std::forward<Ts>(ts)...), m_parametric_object{
                                                            parametric_object}
    {
    }

    // Members
    PSpaceObject      m_pspace_object;
    PObject* m_parametric_object;

    //   Curve interface
  public:
    PSpaceBoolArray isClosed() const override
    {
      return m_pspace_object.isClosed();
    }
    PSpacePoint startParameters() const override
    {
      return m_pspace_object.startParameters();
    }
    PSpacePoint endParameters() const override
    {
      return m_pspace_object.endParameters();
    }

  protected:
    EvaluationResult
    evaluate(const PSpacePoint& par, const PSpaceSizeArray& no_derivatives,
             const PSpaceBoolArray& from_left
             = utils::initStaticContainer<PSpaceBoolArray, PSpaceVectorDim>(
               true)) const override
    {
      return PSubObjEvalCtrl::evaluate(m_pspace_object, m_parametric_object,
                                       par, no_derivatives, from_left);
    }
  };   // namespace gmlib2::parametric









}   // namespace gmlib2::parametric



#endif   // GM_PSUBSPACEOBJECT_H
